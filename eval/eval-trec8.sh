#!/bin/bash

# $1 is a valid trec run file
./trec_eval -m all_trec qrels.2004.robust $1 | grep -e "^map " -e "^P_10 " -e "^ndcg " -e "ndcg_cut_10 " -e "ndcg_cut_100 " -e "^P_100 "
