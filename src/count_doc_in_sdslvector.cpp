/*
 * Transforms a sequence of 64-bit integers into a bit-compressed integer vector.
 * The first command line parameter argv[1] specifies the file, which contains the sequence
 * of integers.
 * The bit-compressed integer vector is stored in a file called `argv[1].int_vector`.
 */
#include <sdsl/util.hpp>
#include <sdsl/int_vector.hpp>
#include <iostream>
#include <string>

using namespace sdsl;
using namespace std;

int main(int argc, char* argv[])
{
    if (argc < 2) {
        cout << "Usage: " << argv[0] << " sdsl_file" << endl;
        return EXIT_FAILURE;
    }
    std::string input_file = argv[1];
    std::ifstream ifs(input_file);
    if(!ifs.is_open()) {
        std::cerr << "Could not open file " << input_file << std::endl;
        return EXIT_FAILURE;
    }
    ifs.close();

    sdsl::int_vector<> T;
    sdsl::load_from_file(T,input_file);

    size_t ndoc = 0;
    for(size_t i=0;i<T.size();i++) {
        if(T[i] == 1) ndoc++;
    }

    std::cout << "ndoc = " << ndoc << std::endl;

    return EXIT_SUCCESS;
}
