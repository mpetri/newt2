
#include <unistd.h>
#include <stdlib.h>
#include <iostream>

#include "sdsl/config.hpp"
#include "newt_index.hpp"
#include "term_map.hpp"
#include "query_parser.hpp"
#include "query_strategies.hpp"
#include "query_processor.hpp"
#include "settings.hpp"

typedef struct cmdargs {
    std::string index_file;
} cmdargs_t;

void
print_usage(char* program)
{
    fprintf(stdout,"%s -i <newt index> -q \n",program);
    fprintf(stdout,"where\n");
    fprintf(stdout,"  -i <newt index>                 : is the index file\n");
};

cmdargs_t
parse_args(int argc,char* const argv[])
{
    cmdargs_t args;
    int op;
    args.index_file = "";

    while ((op=getopt(argc,argv,"i:")) != -1) {
        std::string strarg = optarg;
        switch (op) {
            case 'i':
                args.index_file = optarg;
                break;
            case '?':
            default:
                print_usage(argv[0]);
        }
    }

    if (args.index_file=="") {
        fprintf(stderr,"Missing command line parameters.\n");
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    return args;
}

bool
verify_plist_data(size_t sp,size_t ep,uint64_t term_id,const t_newtindex::plist_type& plist,t_newtindex& index)
{
    //std::cout << "verify_plist_data" << std::endl;
    bool valid = true;
    sdsl::int_vector<64> A(ep-sp+1);
    for (size_t i=sp; i<=ep; i++) A[i-sp] = index.document_array[i];
    std::sort(A.begin(),A.end());
    auto itr = plist.begin();
    auto end = plist.end();
    size_t freq_sum = 0;
    while (itr != end) {
        auto id = itr.docid();
        auto freq = itr.freq();
        freq_sum += freq;
        auto p = std::equal_range(A.begin(),A.end(),id);
        uint64_t calc_freq = std::distance(p.first,p.second);
        if (freq != calc_freq) {
            std::cerr << "ERROR! FREQ NOT EQUAL FOR ID=" << id << " stored = " << freq << " calc = " << calc_freq << std::endl;
            valid = false;
        }
        ++itr;
    }
    size_t F_t = ep-sp+1;
    if (freq_sum != F_t) {
        std::cout << "freq_sum = " << freq_sum << " F_t = " << F_t << std::endl;
        valid = false;
    }

    return valid;
}

bool
verify_plist_skip(size_t sp,size_t ep,uint64_t term_id,const t_newtindex::plist_type& plist,t_newtindex& index)
{
    //std::cout << "verify_plist_skip" << std::endl;
    bool valid = true;
    sdsl::int_vector<64> A(ep-sp+1);
    for (size_t i=sp; i<=ep; i++) A[i-sp] = index.document_array[i];
    std::sort(A.begin(),A.end());
    auto itr = plist.begin();
    auto end = plist.end();


    //std::cout << "search ids" << std::endl;
    static std::mt19937 gen(4711);
    static std::uniform_int_distribution<> search_dist(1, 5000);
    size_t search_pos = search_dist(gen);
    size_t j=1;
    while (itr != end) {
        if (A.size() <= search_pos) break;
        auto id = A[search_pos];
        itr.skip_to_id(id);
        if (itr != end && id != itr.docid()) {
            std::cerr << "(" << search_pos << "/" << A.size() << ") ERROR! id NOT EQUAL FOR searchid = " << id << " stored = " << itr.docid() << " calc = " << itr.freq() << std::endl;
            valid = false;
        }
        search_pos += search_dist(gen);
        j++;
    }

    // test successor skips
    //std::cout << "search successor" << std::endl;
    std::vector<std::pair<uint64_t,uint64_t>> skips;
    for (size_t i=1; i<A.size(); i++) {
        if (A[i] != A[i-1] && A[i-1]+1 != A[i]) {
            skips.emplace_back(A[i-1]+1,A[i]);
        }
    }
    itr = plist.begin();
    j = 0;
    for (const auto& skip : skips) {
        auto search_id = skip.first;
        auto exp_res = skip.second;
        itr.skip_to_id(search_id);
        if (itr.docid() != exp_res) {
            std::cerr << "(" << j << ") " << "ERROR SEARCHING FOR SUCCESSOR...: " << search_id << " -> " << itr.docid() << std::endl;
            std::cerr << "itr.m_cur_pos = " << itr.m_cur_pos << std::endl;
            std::cerr << "itr.m_cur_block_id = " << itr.m_cur_block_id << std::endl;
            std::cerr << "searched for = " << search_id << std::endl;
            std::cerr << "res = " << itr.docid() << " expected = " << skip.second << std::endl;
            std::cerr << "block_reps = ";
            size_t k=0;
            for (const auto& br : plist.m_block_representatives) {
                std::cerr << "<" << k << "," << br << ">";
            }
            std::cerr << std::endl;
            valid = false;
        }
        j++;
    }

    // check searching past the end
    //std::cout << "search past the end" << std::endl;
    size_t last = A[A.size()-1];
    j = 0;
    while (j < 20) {
        itr = plist.begin();
        itr.docid();
        itr.skip_to_id(A[rand()%A.size()]);
        itr.skip_to_id(last+search_dist(gen));
        if (itr != end) {
            std::cerr << "(" << j << ") " << "ERROR SEARCHING FOR PAST END!!" << std::endl;
            valid = false;
        }
        j++;
    }
    itr = plist.begin();
    itr.docid();
    itr.skip_to_id(A[A.size()-1]);
    if (itr.docid() != A[A.size()-1]) {
        std::cerr << "(" << j << ") " << "ERROR SKIPPING TO END!!" << std::endl;
        valid = false;
    }
    ++itr;
    if (itr != end) {
        std::cerr << "(" << j << ") " << "ERROR MOVING PAST END!!" << std::endl;
        valid = false;
    }

    // verify block skip
    //std::cout << "verify block skip" << std::endl;
    size_t num_blocks = plist.num_blocks();
    std::vector<uint64_t> ids;
    itr = plist.begin();
    while (itr != end) {
        ids.push_back(itr.docid());
        ++itr;
    }
    itr = plist.begin();
    size_t pos = 0;
    while (itr != end) {
        // pick random id
        pos = pos + rand() % 512;
        if (pos >= ids.size()) break;
        auto did = ids[pos];
        itr.skip_to_block_with_id(did);
        if (itr.m_cur_block_id != (pos/plist.block_size)) {
            std::cout << "error skipping to block" << std::endl;
            valid = false;
        }
    }

    // verify skip to end of list
    //std::cout << "verify block skip to end of list" << std::endl;
    itr = plist.begin();
    itr.skip_to_id(ids[ids.size()/2]);
    itr.skip_to_block_with_id(ids[ids.size()-1]+10);
    if (itr.m_cur_block_id != num_blocks) {
        std::cout << "error block skipping past end" << std::endl;
    }
    if (itr.m_cur_pos != plist.size()) {
        std::cout << "error block skipping past end (pos)" << std::endl;
    }

    itr = plist.begin();
    itr.skip_to_id(ids[ids.size()/2]);
    itr.skip_to_block_with_id(ids[ids.size()-1]);
    if (itr.m_cur_block_id != num_blocks-1) {
        std::cout << "error block skipping to last elem" << std::endl;
        std::cout << "num_blocks = " << num_blocks << std::endl;
        std::cout << "num_blocks-1 = " << num_blocks-1 << std::endl;
        std::cout << "m_cur_block_id = " << itr.m_cur_block_id << std::endl;
        std::cout << "ids.size() = " << ids.size() << std::endl;
        std::cout << "ids[ids.size()-1] = " << ids[ids.size()-1] << std::endl;
    }

    return valid;
}

int main(int argc,char* const argv[])
{
    using clock = std::chrono::high_resolution_clock;
    /* parse command line */
    cmdargs_t args = parse_args(argc,argv);

    // load the index
    newt_log::info() << "Loading index from file " << args.index_file << std::endl;
    auto load_start = clock::now();
    t_newtindex index;
    sdsl::load_from_file(index,args.index_file);
    auto load_stop = clock::now();
    auto load_time_sec = std::chrono::duration_cast<std::chrono::seconds>(load_stop-load_start);
    newt_log::info() << "Index loaded from disk in " << load_time_sec.count() << " seconds." << std::endl;

    std::cout << "Verifying postings lists.";
    std::cout.flush();

    auto itr = index.termmap.begin();
    std::vector<uint64_t> term(1);
    size_t sp,ep;
    size_t num_terms = index.termmap.num_terms();
    size_t j=1;
    while (itr != index.termmap.end()) {
        term[0] = itr->second;
        sdsl::backward_search(index.csa,0ULL,index.csa.size()-1,
                              term.begin(),term.end(),sp,ep);
        auto& plist = index.lookup_or_create( {sp,ep});
        if (! verify_plist_data(sp,ep,term[0],plist,index)) {
            std::cout << "VERIFY-DATA('" << itr->first << "'," << itr->second << ")  -> FAILED!!" << std::endl;
        }
        if (! verify_plist_skip(sp,ep,term[0],plist,index)) {
            std::cout << "VERIFY-SKIP('" << itr->first << "'," << itr->second << ")  -> FAILED!!" << std::endl;
        }
        if (j % (num_terms/100) == 0) {
            std::cout << ".";
            std::cout.flush();
        }
        j++;
        ++itr;
    }
    std::cout << std::endl;

    newt_log::info() << "Everything done. Exiting." << std::endl;
    return EXIT_SUCCESS;
}
