/*
 * Transforms a sequence of 64-bit integers into a bit-compressed integer vector.
 * The first command line parameter argv[1] specifies the file, which contains the sequence
 * of integers.
 * The bit-compressed integer vector is stored in a file called `argv[1].int_vector`.
 */
#include <sdsl/util.hpp>
#include <sdsl/int_vector.hpp>
#include <iostream>
#include <string>

using namespace sdsl;
using namespace std;

template<class T>
int convert_to_sdsl_vector(std::string input_file,std::string output_file)
{
    size_t x = util::file_size(input_file);
    const int BPI=sizeof(T);
    cout<<"file size in bytes = "<<x<<endl;
    if (x % BPI != 0) {
        cout << "Error: x%"<<BPI<<" != 0"<<endl;
        return EXIT_FAILURE;
    }

//  (1) scan file and determine the largest value
    FILE* f = fopen(input_file.c_str(), "rb"); // open input file
    if (f == NULL) {
        cout<<"ERROR: could not open file "<< input_file <<endl;
        return EXIT_FAILURE;
    }
    const size_t BUF_SIZE = 6400000; // BUF_SIZE has to be a multiple of 64
    T* buf = (T*)malloc(BUF_SIZE*BPI);
    T max=0;
    size_t zeros = 0;
    for (size_t i=0, len=BUF_SIZE*BPI; i<x; i+=len) {
        len = BUF_SIZE*BPI;
        if (i+len > x) {
            len = x-i;
        }
        size_t read = fread((char*)buf, 1, len, f);
        for (size_t j=0; j<len/BPI and read; ++j) {
            if (buf[j] > max)
                max = buf[j];
            if (buf[j] == 0) zeros++;
        }
    }
    cout<<"max value: "<<(uint64_t)max<<endl;
    uint8_t width = bits::hi(max)+1;
    if (width<8) width = 8; // fix for weird sdsl behaviour
    cout<<"width="<<(int)width<<endl;

    if (zeros > 0) {
        cout<<"WARNING: source file contains " << zeros << " zeros!" <<endl;
    }


//  (2) scan file, bit-compress values and write to outfile
    rewind(f); // reset file pointer
    FILE* of = fopen(output_file.c_str(),"wb"); // open output file
    if (of == NULL) {
        cout<<"ERROR: could not open output file "<< output_file <<endl;
        return EXIT_FAILURE;
    }
    uint64_t bitlen=(x/BPI)*width;
    fwrite((char*)&bitlen, 8, 1, of);
    fwrite((char*)&width, 1, 1, of);
    int_vector<> v(BUF_SIZE, 0, width);
    for (size_t i=0, len=BUF_SIZE*BPI; i<x; i+=len) {
        len = BUF_SIZE*BPI;
        if (i+len > x) {
            len = x-i;
        }
        size_t read = fread((char*)buf, 1, len, f);
        for (size_t j=0; j<len/BPI and read; ++j) {
            v[j] = buf[j];
        }
        fwrite((char*)v.data(), 8, ((len/BPI*width+63)/64), of);
    }
    free(buf);
    fclose(f);
    fclose(of);
    return EXIT_SUCCESS;
}

int main(int argc, char* argv[])
{
    if (argc < 3) {
        cout << "Usage: " << argv[0] << " integerfile num_bytes sdsl_file" << endl;
        return EXIT_FAILURE;
    }
    std::string input_file = argv[1];
    std::string output_file = argv[3];
    uint8_t num_bytes = atoi(argv[2]);

    switch (num_bytes) {
        case 1:
            return convert_to_sdsl_vector<uint8_t>(input_file,output_file);
        case 2:
            return convert_to_sdsl_vector<uint16_t>(input_file,output_file);
        case 4:
            return convert_to_sdsl_vector<uint32_t>(input_file,output_file);
        case 8:
            return convert_to_sdsl_vector<uint64_t>(input_file,output_file);
        default:
            cerr << "Error: num_bytes not a valid value (1/2/4/8)" << endl;
            return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
