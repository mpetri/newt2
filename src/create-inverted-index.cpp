
#include <unistd.h>
#include <stdlib.h>
#include <iostream>

#include "sdsl/config.hpp"

#include "inverted_index.hpp"
#include "logger.hpp"
#include "settings.hpp"
#include "input_config.hpp"

typedef struct cmdargs {
    std::string outfile;
    std::string input_config;
} cmdargs_t;

void
print_usage(char* program)
{
    fprintf(stdout,"%s -i <input file description> -o <output>\n",program);
    fprintf(stdout,"where\n");
    fprintf(stdout,"  -i <input text>                  : is the input text (has to be sdsl int_vector\n");
    fprintf(stdout,"  -o <output index>                : the output index\n");
};

cmdargs_t
parse_args(int argc,char* const argv[])
{
    cmdargs_t args;
    int op;
    args.input_config = args.outfile = "";
    while ((op=getopt(argc,argv,"i:q:o:k:e")) != -1) {
        switch (op) {
            case 'i':
                args.input_config = optarg;
                break;
            case 'o':
                args.outfile = optarg;
                break;
            case '?':
            default:
                print_usage(argv[0]);
        }
    }
    if (args.input_config==""||args.outfile=="") {
        fprintf(stderr,"Missing command line parameters.\n");
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
    }
    return args;
}

int main(int argc,char* const argv[])
{
    using clock = std::chrono::high_resolution_clock;
    newt_log::info() << "Building inverted index" << std::endl;

    using t_invidx = inverted_index<CSATYPE, DATYPE, RANKTYPE, PLIST_BLOCK_SIZE>;

    /* parse command line */
    cmdargs_t args = parse_args(argc,argv);

    /* parse input config */
    newt_log::info() << "Parsing input specification file" << std::endl;
    sdsl::cache_config config = input_config::parse_input_cfg(args.input_config);

    // // build the index
    newt_log::info() << "Building inverted index" << std::endl;
    auto build_start = clock::now();
    t_invidx index(config);
    auto build_stop = clock::now();
    auto build_time_sec = std::chrono::duration_cast<std::chrono::seconds>(build_stop-build_start);
    newt_log::info() << "Index built in " << build_time_sec.count() << " seconds." << std::endl;

    // // write index
    newt_log::info() << "Writing index to file " << args.outfile << std::endl;
    auto write_start = clock::now();
    std::ofstream ofile(args.outfile);
    index.serialize(ofile);
    ofile.close();
    auto write_stop = clock::now();
    auto write_time_sec = std::chrono::duration_cast<std::chrono::seconds>(write_stop-write_start);
    newt_log::info() << "Index written to disk in " << write_time_sec.count() << " seconds." << std::endl;

    // // write json extra info
    std::string json_file = args.outfile + ".json";
    std::string html_file = json_file + ".html";
    newt_log::info() << "Writing index structure to file " << html_file << std::endl;
    std::ofstream html_fs(html_file);
    auto json_start = clock::now();
    write_structure<HTML_FORMAT>(index, html_fs);
    html_fs.close();
    auto json_stop = clock::now();
    auto json_time_sec = std::chrono::duration_cast<std::chrono::seconds>(json_stop-json_start);
    newt_log::info() << "Structure files written to disk in " << json_time_sec.count() << " seconds." << std::endl;

    return EXIT_SUCCESS;
}
