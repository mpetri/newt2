
#include <unistd.h>
#include <stdlib.h>
#include <iostream>

#include "sdsl/config.hpp"
#include "newt_index.hpp"
#include "term_map.hpp"
#include "query_parser.hpp"
#include "query_strategies.hpp"
#include "query_processor.hpp"
#include "settings.hpp"

typedef struct cmdargs {
    std::string index_file;
    std::string query_file;
    std::string results_file;
    uint64_t k = 1000;
    uint64_t runs = 3;
    bool query_expansion = false;
} cmdargs_t;

void
print_usage(char* program)
{
    fprintf(stdout,"%s -i <newt index> -q <query file> -o <output> -k <k> -r <runs> -e\n",program);
    fprintf(stdout,"where\n");
    fprintf(stdout,"  -i <newt index>                 : is the index file\n");
    fprintf(stdout,"  -q <query file>                 : query file\n");
    fprintf(stdout,"  -o <output file>                : results go here\n");
    fprintf(stdout,"  -k <num docs returned>          : top-k results are calculated\n");
    fprintf(stdout,"  -e                              : perform query expansion\n");
    fprintf(stdout,"  -r <runs>                       : number of times the queries are processed\n");
};

cmdargs_t
parse_args(int argc,char* const argv[])
{
    cmdargs_t args;
    int op;

    args.index_file = args.query_file = args.results_file = "";

    while ((op=getopt(argc,argv,"i:q:o:k:r:e")) != -1) {
        std::string strarg = optarg;
        switch (op) {
            case 'i':
                args.index_file = optarg;
                break;
            case 'o':
                args.results_file = optarg;
                break;
            case 'e':
                args.query_expansion = true;
                break;
            case 'q':
                args.query_file = optarg;
                break;
            case 'k':
                args.k = atoll(optarg);
                break;
            case 'r':
                args.runs = atoll(optarg);
                break;
            case '?':
            default:
                print_usage(argv[0]);
        }
    }

    if (args.index_file==""||args.query_file==""||args.results_file=="") {
        fprintf(stderr,"Missing command line parameters.\n");
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    return args;
}

output_benchmark<t_newtindex>
process_queries(const t_newtindex& index,std::vector<query>& qrys,uint64_t k,uint64_t runs,query_strat qs)
{
    using clock = std::chrono::high_resolution_clock;

    // process the queries
    newt_log::info() << "Processing queries using " << sg_query_strat_names[qs] << std::endl;
    auto process_start = clock::now();
    std::vector<output_benchmark<t_newtindex>> run_results;
    for (size_t i=0; i<runs; i++) {
        run_results.emplace_back(
            query_processor<output_benchmark<t_newtindex>, RANKTYPE, t_newtindex>::process_queries(index,qrys,k,qs)
        );
    }
    auto process_stop = clock::now();
    auto process_time_sec = std::chrono::duration_cast<std::chrono::seconds>(process_stop-process_start);
    newt_log::info() << qrys.size() << " queries processed " << runs << " times in " << process_time_sec.count() << " seconds." << std::endl;

    // calculate avg time
    for (size_t i=1; i<run_results.size(); i++) {
        for (size_t j=0; j<run_results[0].results.size(); j++) {
            run_results[0].results[j].total_time += run_results[i].results[j].total_time;
            run_results[0].results[j].csa_time += run_results[i].results[j].csa_time;
            run_results[0].results[j].candidate_time += run_results[i].results[j].candidate_time;
            run_results[0].results[j].process_time += run_results[i].results[j].process_time;
        }
    }
    for (size_t j=0; j<run_results[0].results.size(); j++) {
        run_results[0].results[j].total_time = run_results[0].results[j].total_time / run_results.size();
        run_results[0].results[j].csa_time = run_results[0].results[j].csa_time / run_results.size();
        run_results[0].results[j].candidate_time = run_results[0].results[j].candidate_time / run_results.size();
        run_results[0].results[j].process_time = run_results[0].results[j].process_time / run_results.size();
    }
    return run_results[0];
}

int main(int argc,char* const argv[])
{
    using clock = std::chrono::high_resolution_clock;
    /* parse command line */
    cmdargs_t args = parse_args(argc,argv);

    // load the index
    newt_log::info() << "Loading index from file " << args.index_file << std::endl;
    auto load_start = clock::now();
    t_newtindex index;
    sdsl::load_from_file(index,args.index_file);
    auto load_stop = clock::now();
    auto load_time_sec = std::chrono::duration_cast<std::chrono::seconds>(load_stop-load_start);
    newt_log::info() << "Index loaded from disk in " << load_time_sec.count() << " seconds." << std::endl;

    // parse the queries
    newt_log::info() << "Parsing queries from file " << args.query_file << std::endl;
    auto parse_start = clock::now();

    std::vector<query> qrys;
    if (args.query_expansion) {
        const uint64_t single_term_weight = newt_constants::expansion_single_weight;
        const uint64_t phrase_weight = newt_constants::expansion_phrase_weight;
        qrys = query_parser<t_newtindex,true,true,single_term_weight,phrase_weight>::parse_query_file(args.query_file,index);
    } else {
        const uint64_t single_term_weight = newt_constants::no_expansion_single_weight;
        qrys = query_parser<t_newtindex,false,true,single_term_weight,0>::parse_query_file(args.query_file,index);
    }

    auto parse_stop = clock::now();
    auto parse_time_sec = std::chrono::duration_cast<std::chrono::seconds>(parse_stop-parse_start);
    newt_log::info() << qrys.size() << " queries parsed in " << parse_time_sec.count() << " seconds." << std::endl;


    // run queries
    std::vector<output_benchmark<t_newtindex>> results;
    results.emplace_back(process_queries(index,qrys,args.k,args.runs,QUERY_DAAT));
    results.emplace_back(process_queries(index,qrys,args.k,args.runs,QUERY_WAND));
    results.emplace_back(process_queries(index,qrys,args.k,args.runs,QUERY_MAXSCORE));
    results.emplace_back(process_queries(index,qrys,args.k,args.runs,QUERY_BMW));
    //results.emplace_back(process_queries(index,qrys,args.k,args.runs,QUERY_BMM));

    // write output
    std::ofstream res_fs(args.results_file);
    if (res_fs.is_open()) {
        newt_log::info() << "Writing results to file " << args.results_file << std::endl;
        bool first = true;
        for (const auto& res : results) {
            res.output(res_fs,index,first);
            first = false;
        }
        res_fs.close();
    } else {
        newt_log::crit() << "can not write output to file '" << args.results_file << std::endl;
    }

    newt_log::info() << "Everything done. Exiting." << std::endl;
    return EXIT_SUCCESS;
}
