
#include <unistd.h>
#include <stdlib.h>
#include <iostream>

#include "sdsl/config.hpp"
#include "inverted_index.hpp"
#include "term_map.hpp"
#include "query_parser.hpp"
#include "query_strategies.hpp"
#include "query_processor.hpp"
#include "settings.hpp"
#include "output_formats.hpp"
#include "result.hpp"

using t_invidx = inverted_index<CSATYPE, DATYPE, RANKTYPE, PLIST_BLOCK_SIZE>;

typedef struct cmdargs {
    std::string index_file;
    std::string query_file;
    std::string results_file;
    query_strat query_strategy;
    uint64_t k = 1000;
    bool query_expansion = false;
} cmdargs_t;

void
print_usage(char* program)
{
    fprintf(stdout,"%s -i <newt index> -q <query file> -o <output> -p <ptype> -k <k> -e\n",program);
    fprintf(stdout,"where\n");
    fprintf(stdout,"  -i <inverted index>                 : is the index file\n");
    fprintf(stdout,"  -q <query file>                 : query file\n");
    fprintf(stdout,"  -o <output file>                : results go here\n");
    fprintf(stdout,"  -p <processing type>            : TAAT/DAAT/WAND/MAXSCORE/BMW/BMM \n");
    fprintf(stdout,"  -e                              : perform query expansion\n");
    fprintf(stdout,"  -k <num docs returned>          : top-k results are calculated\n");
};

cmdargs_t
parse_args(int argc,char* const argv[])
{
    cmdargs_t args;
    int op;
    args.index_file = args.query_file = args.results_file = "";
    args.query_strategy = QUERY_UNKNOWN;

    while ((op=getopt(argc,argv,"i:q:o:k:ep:")) != -1) {
        std::string strarg;
        if (optarg) {
            strarg = optarg;
        }
        switch (op) {
            case 'i':
                args.index_file = optarg;
                break;
            case 'e':
                args.query_expansion = true;
                break;
            case 'p':
                if (strarg == "TAAT") args.query_strategy = QUERY_TAAT;
                else if (strarg == "DAAT") args.query_strategy = QUERY_DAAT;
                else if (strarg == "WAND") args.query_strategy = QUERY_WAND;
                else if (strarg == "MAXSCORE") args.query_strategy = QUERY_MAXSCORE;
                else if (strarg == "BMW") args.query_strategy = QUERY_BMW;
                else if (strarg == "BMM") args.query_strategy = QUERY_BMM;
                break;
            case 'o':
                args.results_file = optarg;
                break;
            case 'q':
                args.query_file = optarg;
                break;
            case 'k':
                args.k = atoll(optarg);
                break;
            case '?':
            default:
                print_usage(argv[0]);
        }
    }

    if (args.index_file==""||args.query_file==""||args.results_file=="") {
        fprintf(stderr,"Missing command line parameters.\n");
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    return args;
}


template<class t_pl>
auto
retrieve_postings_lists(query& qry,const t_invidx& index) -> std::vector<plist_data<t_pl>> {
    std::vector<plist_data<t_pl>> lists;



    return index.lookup_and_create_postings(qry);
}

template<class t_ranker>
result
process_query(const t_invidx& index,query qry,uint64_t k,query_strat query_strategy)
{
    using clock = std::chrono::high_resolution_clock;

    result res;
    uint64_t num_terms = (index.docmap.average_length*index.docmap.num_documents()) - index.docmap.num_documents();
    t_ranker ranker(num_terms,index.docmap);

    // (1) retrieve all postings lists
    auto candidates_start = clock::now();
    auto postings_lists = retrieve_postings_lists<t_invidx::plist_type>(qry,index);
    auto candidates_stop = clock::now();

    // we work with pointers to the actual data instead to avoid copying
    std::vector<plist_data<t_invidx::plist_type>*> ptr_lists;
    for (auto& pl : postings_lists) {
        ptr_lists.push_back(&pl);
    }

    // (2) process the lists
    auto list_process_start = clock::now();

#ifdef NEWT_PROFILE_QUERY_PERFORMANCE
    query_profiler::set_cur_qry_and_strat(qry.number,query_strategy);
#endif

    switch (query_strategy) {
        case QUERY_TAAT:
            res.list = query_strategy_taat::process(ptr_lists,ranker,k);
            break;
        case QUERY_DAAT:
            res.list = query_strategy_daat::process(ptr_lists,ranker,k);
            break;
        case QUERY_WAND:
            res.list = query_strategy_wand::process(ptr_lists,ranker,k);
            break;
        case QUERY_BMW:
            res.list = query_strategy_bmw::process(ptr_lists,ranker,k);
            break;
        case QUERY_MAXSCORE:
            res.list = query_strategy_maxscore::process(ptr_lists,ranker,k);
            break;
            // case QUERY_BMM:
            //     res.list = query_strategy_bmm::process(ptr_lists,ranker,k);
            //     break;
        default:
            newt_log::crit() << "Unknown query strategy!"  << std::endl;
            exit(EXIT_FAILURE);
    }
    auto list_process_stop = clock::now();

    // timing stuff
    auto total_elapsed = (candidates_stop-candidates_start) + (list_process_stop-list_process_start);
    res.total_time = std::chrono::duration_cast<std::chrono::milliseconds>(total_elapsed);
    res.candidate_time = std::chrono::duration_cast<std::chrono::milliseconds>(candidates_stop-candidates_start);
    res.process_time = std::chrono::duration_cast<std::chrono::milliseconds>(list_process_stop-list_process_start);

    // clear the tmp content in the lists
    index.clear_onthefly();

    // ensure the same sorting order of the results
    struct {
        bool operator()(const doc_res_t& a,const doc_res_t& b) {
            if (a.second == b.second) {
                return a.first < b.first;
            }
            return a.second > b.second;
        }
    } doc_res_sorter;
    std::sort(res.list.begin() , res.list.end() , doc_res_sorter);
    res.qry = std::move(qry);
    res.query_strategy = query_strategy;
    return res;
};


template<class t_output>
t_output
process_queries(const t_invidx& index,std::vector<query>& queries,uint64_t k,query_strat query_strategy)
{
    t_output output;
    for (const auto& qry : queries) {
        output.results.emplace_back(process_query<t_invidx::rank_type>(index,qry,k,query_strategy));
        newt_log::info() << "QUERY [" << qry.number  << "] - "
                         <<  output.results.back().total_time.count()
                         << " ms" << std::endl;
    }
    return output;
};



int main(int argc,char* const argv[])
{
    using clock = std::chrono::high_resolution_clock;
    /* parse command line */
    cmdargs_t args = parse_args(argc,argv);

    // load the index
    newt_log::info() << "Loading inverted index from file " << args.index_file << std::endl;
    auto load_start = clock::now();
    t_invidx index;
    sdsl::load_from_file(index,args.index_file);
    auto load_stop = clock::now();
    auto load_time_sec = std::chrono::duration_cast<std::chrono::seconds>(load_stop-load_start);
    newt_log::info() << "Index loaded from disk in " << load_time_sec.count() << " seconds." << std::endl;

    // parse the queries
    newt_log::info() << "Parsing queries from file " << args.query_file << std::endl;
    auto parse_start = clock::now();

    std::vector<query> qrys;
    if (args.query_expansion) {
        const uint64_t single_term_weight = newt_constants::expansion_single_weight;
        const uint64_t phrase_weight = newt_constants::expansion_phrase_weight;
        qrys = query_parser<t_invidx,true,true,single_term_weight,phrase_weight>::parse_query_file(args.query_file,index);
    } else {
        const uint64_t single_term_weight = newt_constants::no_expansion_single_weight;
        qrys = query_parser<t_invidx,false,true,single_term_weight,0>::parse_query_file(args.query_file,index);
    }

    auto parse_stop = clock::now();
    auto parse_time_sec = std::chrono::duration_cast<std::chrono::seconds>(parse_stop-parse_start);
    newt_log::info() << qrys.size() << " queries parsed in " << parse_time_sec.count() << " seconds." << std::endl;

    // process the queries
    newt_log::info() << "Processing queries" << std::endl;
    auto process_start = clock::now();
    auto results = process_queries<output_trec<t_invidx,1000>>(index,qrys,args.k,args.query_strategy);
    auto process_stop = clock::now();
    auto process_time_sec = std::chrono::duration_cast<std::chrono::seconds>(process_stop-process_start);
    newt_log::info() << qrys.size() << " queries processed in " << process_time_sec.count() << " seconds." << std::endl;

    // write output
    std::ofstream res_fs(args.results_file);
    if (res_fs.is_open()) {
        newt_log::info() << "Writing results to file " << args.results_file << std::endl;
        results.output(res_fs,index);
        res_fs.close();
    } else {
        newt_log::crit() << "can not write output to file '"
                         << args.results_file << "'\n";
    }

    newt_log::info() << "Everything done. Exiting." << std::endl;
    return EXIT_SUCCESS;
}
