
#include <unistd.h>
#include <stdlib.h>
#include <iostream>

#include "sdsl/config.hpp"
#include "newt_index.hpp"
#include "term_map.hpp"
#include "query_parser.hpp"
#include "query_strategies.hpp"
#include "query_processor.hpp"
#include "settings.hpp"

typedef struct cmdargs {
    std::string index_file;
    std::string query_file;
    std::string results_file;
    query_strat query_strategy;
    uint64_t k = 1000;
    bool query_expansion = false;
} cmdargs_t;

void
print_usage(char* program)
{
    fprintf(stdout,"%s -i <newt index> -q <query file> -o <output> -p <ptype> -k <k> -e\n",program);
    fprintf(stdout,"where\n");
    fprintf(stdout,"  -i <newt index>                 : is the index file\n");
    fprintf(stdout,"  -q <query file>                 : query file\n");
    fprintf(stdout,"  -o <output file>                : results go here\n");
    fprintf(stdout,"  -p <processing type>            : TAAT/DAAT/WAND/MAXSCORE/BMW/BMM \n");
    fprintf(stdout,"  -e                              : perform query expansion\n");
    fprintf(stdout,"  -k <num docs returned>          : top-k results are calculated\n");
};

cmdargs_t
parse_args(int argc,char* const argv[])
{
    cmdargs_t args;
    int op;
    args.index_file = args.query_file = args.results_file = "";
    args.query_strategy = QUERY_UNKNOWN;

    while ((op=getopt(argc,argv,"i:q:o:k:ep:")) != -1) {
        std::string strarg;
        if (optarg) {
            strarg = optarg;
        }
        switch (op) {
            case 'i':
                args.index_file = optarg;
                break;
            case 'e':
                args.query_expansion = true;
                break;
            case 'p':
                if (strarg == "TAAT") args.query_strategy = QUERY_TAAT;
                else if (strarg == "DAAT") args.query_strategy = QUERY_DAAT;
                else if (strarg == "WAND") args.query_strategy = QUERY_WAND;
                else if (strarg == "MAXSCORE") args.query_strategy = QUERY_MAXSCORE;
                else if (strarg == "BMW") args.query_strategy = QUERY_BMW;
                else if (strarg == "BMM") args.query_strategy = QUERY_BMM;
                break;
            case 'o':
                args.results_file = optarg;
                break;
            case 'q':
                args.query_file = optarg;
                break;
            case 'k':
                args.k = atoll(optarg);
                break;
            case '?':
            default:
                print_usage(argv[0]);
        }
    }

    if (args.index_file==""||args.query_file==""||args.results_file=="") {
        fprintf(stderr,"Missing command line parameters.\n");
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    return args;
}

int main(int argc,char* const argv[])
{
    using clock = std::chrono::high_resolution_clock;
    /* parse command line */
    cmdargs_t args = parse_args(argc,argv);

    // load the index
    newt_log::info() << "Loading index from file " << args.index_file << std::endl;
    auto load_start = clock::now();
    t_newtindex index;
    sdsl::load_from_file(index,args.index_file);
    auto load_stop = clock::now();
    auto load_time_sec = std::chrono::duration_cast<std::chrono::seconds>(load_stop-load_start);
    newt_log::info() << "Index loaded from disk in " << load_time_sec.count() << " seconds." << std::endl;

    // parse the queries
    newt_log::info() << "Parsing queries from file " << args.query_file << std::endl;
    auto parse_start = clock::now();

    std::vector<query> qrys;
    if (args.query_expansion) {
        const uint64_t single_term_weight = newt_constants::expansion_single_weight;
        const uint64_t phrase_weight = newt_constants::expansion_phrase_weight;
        qrys = query_parser<t_newtindex,true,true,single_term_weight,phrase_weight>::parse_query_file(args.query_file,index);
    } else {
        const uint64_t single_term_weight = newt_constants::no_expansion_single_weight;
        qrys = query_parser<t_newtindex,false,true,single_term_weight,0>::parse_query_file(args.query_file,index);
    }

    auto parse_stop = clock::now();
    auto parse_time_sec = std::chrono::duration_cast<std::chrono::seconds>(parse_stop-parse_start);
    newt_log::info() << qrys.size() << " queries parsed in " << parse_time_sec.count() << " seconds." << std::endl;

    // process the queries
    newt_log::info() << "Processing queries" << std::endl;
    auto process_start = clock::now();
    auto results = query_processor<output_trec<t_newtindex,1000>, RANKTYPE, t_newtindex>::process_queries(index,qrys,args.k,args.query_strategy);
    auto process_stop = clock::now();
    auto process_time_sec = std::chrono::duration_cast<std::chrono::seconds>(process_stop-process_start);
    newt_log::info() << qrys.size() << " queries processed in " << process_time_sec.count() << " seconds." << std::endl;

    // write output
    std::ofstream res_fs(args.results_file);
    if (res_fs.is_open()) {
        newt_log::info() << "Writing results to file " << args.results_file << std::endl;
        results.output(res_fs,index);
        res_fs.close();
    } else {
        newt_log::crit() << "can not write output to file '"
                         << args.results_file << "'\n";
    }

    newt_log::info() << "Everything done. Exiting." << std::endl;
    return EXIT_SUCCESS;
}
