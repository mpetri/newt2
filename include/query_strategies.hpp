#ifndef QUERY_STRATEGIES_HPP
#define QUERY_STRATEGIES_HPP

#include "query.hpp"

template<class t_pl>
struct plist_data {
    const t_pl* data;
    const qterm* qt;
    typename t_pl::const_iterator cur;
    typename t_pl::const_iterator end;
    double max_doc_score;
    double wand_cum_score;
    plist_data(const t_pl& pl,const qterm& q) : data(&pl) , qt(&q) , max_doc_score(0), wand_cum_score(0) {
        cur = pl.begin();
        end = pl.end();
    }
    plist_data(const plist_data& pd) : data(pd.data) , qt(pd.qt) , cur(pd.cur) ,
        end(pd.end) , max_doc_score(pd.max_doc_score) ,
        wand_cum_score(pd.wand_cum_score) {
        /*std::cout << "plist_data copy constructur" << std::endl;*/
    }
    plist_data(plist_data&& pd) : data(std::move(pd.data)) , qt(std::move(pd.qt)) , cur(std::move(pd.cur)) , end(std::move(pd.end)) , max_doc_score(pd.max_doc_score) , wand_cum_score(pd.wand_cum_score) {}
    plist_data& operator=(const plist_data& pd) {
        //std::cout << "plist_data copy assignment" << std::endl;
        data = pd.data;
        qt = pd.qt;
        cur = pd.cur;
        end = pd.end;
        max_doc_score = pd.max_doc_score;
        wand_cum_score = pd.wand_cum_score;
        return (*this);
    }
    plist_data& operator=(plist_data&& pd) {
        data = std::move(pd.data);
        qt = std::move(pd.qt);
        cur = std::move(pd.cur);
        end = std::move(pd.end);
        max_doc_score = pd.max_doc_score;
        wand_cum_score = pd.wand_cum_score;
        return (*this);
    }
    bool operator < (const plist_data& o) const {
        return cur.docid() < o.cur.docid();
    }
    void reset() {
        cur = data->begin();
    }
};

#include "query_strat_taat.hpp"
#include "query_strat_daat.hpp"
#include "query_strat_wand.hpp"
#include "query_strat_bmw.hpp"
#include "query_strat_maxscore.hpp"
//#include "query_strat_bmm.hpp"

#endif
