#ifndef POSTINGS_LIST
#define POSTINGS_LIST

#include <limits>
#include <stdexcept>

#include "util.h"
#include "memutil.h"
#include "codecs.h"
#include "codecfactory.h"
#include "bitpacking.h"
#include "simdfastpfor.h"
#include "deltautil.h"

#include "query_profiler.hpp"
#include "sdsl/int_vector.hpp"

using namespace sdsl;

template<uint64_t t_block_size>
class postings_list;

template<uint64_t t_block_size>
class plist_iterator
{
    public:
        typedef postings_list<t_block_size>                               list_type;
        typedef typename list_type::size_type                             size_type;
        typedef uint64_t                                                 value_type;
    public:
        plist_iterator();
        plist_iterator(const list_type& l,size_t pos);
        plist_iterator(const plist_iterator& pi);
        plist_iterator(plist_iterator&& pi);
        plist_iterator& operator=(const plist_iterator& pi);
        plist_iterator& operator=(plist_iterator&& pi);
        plist_iterator& operator++();
        bool operator ==(const plist_iterator& b) const;
        bool operator !=(const plist_iterator& b) const;
        uint64_t docid() const;
        uint64_t freq() const;
        void skip_to_id(uint64_t id);
        void skip_to_block_with_id(uint64_t id);
        void skip_to_block_with_id_prv(uint64_t id);
        double block_max() const;
        double block_max_doc_weight() const;
        uint64_t block_rep() const;
        size_t size() const;
        size_t remaining() const {
            return size() - m_cur_pos;
        }
        size_t offset() const {
            return m_cur_pos;
        }
    private:
        void access_and_decode_cur_pos() const;
    private:
        size_type                                                         m_cur_pos;
        mutable size_type                                            m_cur_block_id;
        mutable size_type                                     m_last_accessed_block;
        mutable size_type                                        m_last_accessed_id;
        mutable value_type                                              m_cur_docid;
        mutable value_type                                               m_cur_freq;
        const list_type*                                                m_plist_ptr;
        mutable std::vector<uint32_t>                                 m_decoded_ids;
        mutable std::vector<uint32_t>                               m_decoded_freqs;
};


template<uint64_t t_block_size>
class postings_list
{
    public:
        typedef sdsl::int_vector<>::size_type                             size_type;
        typedef std::pair<uint64_t,uint64_t>                             value_type;
        typedef plist_iterator<t_block_size>                         const_iterator;
        typedef std::vector<uint32_t, cacheallocator>                pfor_data_type;
        const uint64_t                                    block_size = t_block_size;
    public:
        postings_list();
        postings_list(std::istream& in);
        postings_list(const postings_list<t_block_size>& pl);
        postings_list(postings_list<t_block_size>&& pl);
        template<class t_rank> postings_list(const t_rank& ranker,sdsl::int_vector<64>& data,size_t n = 0);
        template<class t_rank> postings_list(const t_rank& ranker,std::vector<std::pair<uint64_t,uint64_t>>& pre_sorted_data);
        postings_list<t_block_size>& operator=(postings_list<t_block_size>&& pl);
        size_t size() const;
        const_iterator begin() const;
        const_iterator end() const;
        size_t num_blocks() const;
        size_t serialize(std::ostream& out, sdsl::structure_tree_node* v=nullptr, std::string name="") const;
        void load(std::istream& in);
        double max_doc_weight() const {
            return m_max_doc_weight;
        }
        double block_max(uint64_t bid) const {
            return m_block_maximums[bid];
        }
        double block_max_doc_weight(uint64_t bid) const {
            return m_block_max_doc_weights[bid];
        }
        uint64_t block_rep(uint64_t bid) const {
            return m_block_representatives[bid];
        }
        double list_max_score() const {
            return m_list_maximuim;
        }
    private:
        friend class plist_iterator<t_block_size>;
        size_t                                                               m_size;
        pfor_data_type                                                 m_docid_data;
        pfor_data_type                                                  m_freq_data;
        sdsl::int_vector<>                                           m_id_block_ptr;
        sdsl::int_vector<>                                         m_freq_block_ptr;
        sdsl::int_vector<>                                  m_block_representatives;
    private: // rank function dependent
        std::vector<double>                                        m_block_maximums;
        std::vector<double>                                 m_block_max_doc_weights;
        double                                                      m_list_maximuim;
        double                                                     m_max_doc_weight;
    private:
        void decompress_block(size_t bid,std::vector<uint32_t>& id_data,std::vector<uint32_t>& freq_data) const;
        void create_block_support(const sdsl::int_vector<32>& ids,const sdsl::int_vector<32>& freqs);
        template<class t_rank> void create_rank_support(const sdsl::int_vector<32>& ids,const sdsl::int_vector<32>& freqs,double F_t,const t_rank& ranker);
        void compress_postings_data(const sdsl::int_vector<32>& ids,const sdsl::int_vector<32>& freqs);
        size_t find_block_with_id(uint64_t id,size_t start_block = 0) const;
};

template<uint64_t t_bs>
postings_list<t_bs>::postings_list() : m_size(0) ,
    m_list_maximuim(std::numeric_limits<double>::lowest()) ,
    m_max_doc_weight(std::numeric_limits<double>::lowest()) {}

template<uint64_t t_bs>
postings_list<t_bs>::postings_list(postings_list<t_bs>&& pl) : m_size(pl.m_size),
    m_docid_data(std::move(pl.m_docid_data)) , m_freq_data(std::move(pl.m_freq_data)) ,
    m_id_block_ptr(std::move(pl.m_id_block_ptr)) , m_freq_block_ptr(std::move(pl.m_freq_block_ptr)) ,
    m_block_representatives(std::move(pl.m_block_representatives)) , m_block_maximums(std::move(pl.m_block_maximums)) ,
    m_block_max_doc_weights(std::move(pl.m_block_max_doc_weights)) ,
    m_list_maximuim(pl.m_list_maximuim) , m_max_doc_weight(pl.m_max_doc_weight)
{
}

template<uint64_t t_bs>
postings_list<t_bs>::postings_list(const postings_list<t_bs>& pl) : m_size(pl.m_size),
    m_docid_data(pl.m_docid_data) , m_freq_data(pl.m_freq_data) ,
    m_id_block_ptr(pl.m_id_block_ptr) , m_freq_block_ptr(pl.m_freq_block_ptr) ,
    m_block_representatives(pl.m_block_representatives) , m_block_maximums(pl.m_block_maximums) ,
    m_block_max_doc_weights(pl.m_block_max_doc_weights) ,
    m_list_maximuim(pl.m_list_maximuim) , m_max_doc_weight(pl.m_max_doc_weight)
{
    //std::cout << "copy constructor called." << std::endl;
}


template<uint64_t t_bs>
postings_list<t_bs>::postings_list(std::istream& in) : postings_list()
{
    load(in);
}

template<uint64_t t_bs>
postings_list<t_bs>& postings_list<t_bs>::operator=(postings_list<t_bs>&& pl)
{
    m_size = pl.m_size;
    m_docid_data = std::move(pl.m_docid_data);
    m_freq_data = std::move(pl.m_freq_data);
    m_id_block_ptr = std::move(pl.m_id_block_ptr);
    m_freq_block_ptr = std::move(pl.m_freq_block_ptr);
    m_block_representatives = std::move(pl.m_block_representatives);
    m_block_maximums = std::move(pl.m_block_maximums);
    m_block_max_doc_weights = std::move(pl.m_block_max_doc_weights);
    m_list_maximuim = pl.m_list_maximuim;
    m_max_doc_weight = pl.m_max_doc_weight;
    return (*this);
}

template<uint64_t t_bs>
size_t postings_list<t_bs>::size() const
{
    return m_size;
}

template<uint64_t t_bs>
size_t postings_list<t_bs>::num_blocks() const
{
    return m_block_representatives.size();
}

template<uint64_t t_bs>
typename postings_list<t_bs>::const_iterator postings_list<t_bs>::begin() const
{
    return const_iterator(*this,0);
}

template<uint64_t t_bs>
typename postings_list<t_bs>::const_iterator postings_list<t_bs>::end() const
{
    return const_iterator(*this,m_size);
}

template<uint64_t t_bs>
template<class t_rank>
postings_list<t_bs>::postings_list(const t_rank& ranker,std::vector<std::pair<uint64_t,uint64_t>>& pre_sorted_data)
{
    // extract doc_ids and freqs
    sdsl::int_vector<32> tmp_data(pre_sorted_data.size());
    sdsl::int_vector<32> tmp_freq(pre_sorted_data.size());
    for (size_type i=0; i<pre_sorted_data.size(); i++) {
        tmp_data[i] = pre_sorted_data[i].first;
        tmp_freq[i] = pre_sorted_data[i].second;
    }

    // create block max structure first
    create_block_support(tmp_data,tmp_freq);

    // create rank support structure
    create_rank_support(tmp_data,tmp_freq,pre_sorted_data.size(),ranker);

    // compress postings
    compress_postings_data(tmp_data,tmp_freq);
}

template<uint64_t t_bs>
template<class t_rank>
postings_list<t_bs>::postings_list(const t_rank& ranker,sdsl::int_vector<64>& data,size_t n)
{
    if (data.size() == 0) {
        std::cerr << "ERROR: trying to create empty postings list.\n";
        throw std::logic_error("trying to create empty postings list.");
    }
    std::sort(data.begin(),data.begin()+n);

    // count uniq docs
    size_t unique = 1;
    for (size_t i=1; i<n; i++) {
        if (data[i-1] != data[i]) unique++;
    }

    // extract doc_ids and freqs
    sdsl::int_vector<32> tmp_data(unique);
    sdsl::int_vector<32> tmp_freq(unique);
    size_type j = 0; size_type freq = 1;
    for (size_type i=1; i<n; i++) {
        if (data[i] != data[i-1]) {
            tmp_data[j] = data[i-1];
            tmp_freq[j] = freq;
            j++;
            freq = 0;
        }
        freq++;
    }
    tmp_data[j] = data[n-1];
    tmp_freq[j] = freq;


    // create block max structure first
    create_block_support(tmp_data,tmp_freq);

    // create rank support structure
    create_rank_support(tmp_data,tmp_freq,n,ranker);

    // compress postings
    compress_postings_data(tmp_data,tmp_freq);
}

template<uint64_t t_bs>
size_t postings_list<t_bs>::serialize(std::ostream& out, sdsl::structure_tree_node* v, std::string name) const
{
    sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
    size_type written_bytes = 0;
    written_bytes += sdsl::write_member(m_size,out,child,"size");
    written_bytes += m_id_block_ptr.serialize(out,child,"id ptrs");
    written_bytes += m_freq_block_ptr.serialize(out,child,"freq ptrs");
    written_bytes += sdsl::write_member(m_docid_data.size(),out,child,"docid u32s");
    written_bytes += sdsl::write_member(m_freq_data.size(),out,child,"freq u32s");
    sdsl::structure_tree_node* idchild = sdsl::structure_tree::add_child(child, "id data", "uint32_t");
    out.write((const char*)m_docid_data.data(), m_docid_data.size()*sizeof(uint32_t));
    sdsl::structure_tree::add_size(idchild, m_docid_data.size()*sizeof(uint32_t));
    written_bytes +=  m_docid_data.size()*sizeof(uint32_t);
    out.write((const char*)m_freq_data.data(), m_freq_data.size()*sizeof(uint32_t));
    written_bytes +=  m_freq_data.size()*sizeof(uint32_t);
    sdsl::structure_tree_node* fchild = sdsl::structure_tree::add_child(child, "freq data", "uint32_t");
    sdsl::structure_tree::add_size(fchild, m_freq_data.size()*sizeof(uint32_t));

    sdsl::structure_tree_node* bmchild = sdsl::structure_tree::add_child(child, "blockmax", "blockmax");
    size_type bm_written_bytes = m_block_representatives.serialize(out,bmchild,"block reps");
    sdsl::structure_tree_node* bmmchild = sdsl::structure_tree::add_child(bmchild, "block maximums", "double");
    bm_written_bytes += sdsl::write_member(m_block_maximums.size(),out,bmchild,"num max_scores");
    out.write((const char*)m_block_maximums.data(), m_block_maximums.size()*sizeof(double));
    out.write((const char*)m_block_max_doc_weights.data(), m_block_max_doc_weights.size()*sizeof(double));
    bm_written_bytes += 2*m_block_maximums.size()*sizeof(double);
    sdsl::structure_tree::add_size(bmmchild,2*m_block_maximums.size()*sizeof(double));
    sdsl::structure_tree::add_size(bmchild,bm_written_bytes);
    written_bytes += bm_written_bytes;

    written_bytes += sdsl::write_member(m_list_maximuim,out,child,"list max score");
    written_bytes += sdsl::write_member(m_max_doc_weight,out,child,"max doc weight");

    sdsl::structure_tree::add_size(child, written_bytes);
    return written_bytes;
}

template<uint64_t t_bs>
void postings_list<t_bs>::load(std::istream& in)
{
    read_member(m_size,in);
    m_id_block_ptr.load(in);
    m_freq_block_ptr.load(in);
    size_t docidu32;
    size_t frequ32;
    read_member(docidu32,in);
    read_member(frequ32,in);
    m_docid_data.resize(docidu32);
    m_freq_data.resize(frequ32);
    in.read((char*)m_docid_data.data(),docidu32*sizeof(uint32_t));
    in.read((char*)m_freq_data.data(),frequ32*sizeof(uint32_t));
    m_block_representatives.load(in);
    size_t num_block_max_scores;
    read_member(num_block_max_scores,in);
    m_block_maximums.resize(num_block_max_scores);
    in.read((char*)m_block_maximums.data(),num_block_max_scores*sizeof(double));
    m_block_max_doc_weights.resize(num_block_max_scores);
    in.read((char*)m_block_max_doc_weights.data(),num_block_max_scores*sizeof(double));
    read_member(m_list_maximuim,in);
    read_member(m_max_doc_weight,in);
}


template<uint64_t t_bs>
template<class t_rank>
void postings_list<t_bs>::create_rank_support(const sdsl::int_vector<32>& ids,const sdsl::int_vector<32>& freqs,double F_t,const t_rank& ranker)
{
    size_t num_blocks = ids.size() / t_bs;
    if (ids.size() % t_bs != 0) num_blocks++;

    m_block_maximums.resize(num_blocks);
    m_block_max_doc_weights.resize(num_blocks);
    double max_score = 0.0f;
    double max_doc_weight = std::numeric_limits<double>::lowest();
    double f_t = ids.size();
    size_t i = 1;
    size_t j = 0;
    m_max_doc_weight = std::numeric_limits<double>::lowest();
    m_list_maximuim = std::numeric_limits<double>::lowest();
    for (size_t l=0; l<ids.size(); l++) {
        auto id = ids[l];
        auto f_dt = freqs[l];
        double W_d = ranker.doc_length(id);
        double doc_weight = ranker.calc_doc_weight(W_d);
        double score = ranker.calculate_docscore(1.0f,f_dt,f_t,F_t,W_d);
        max_score = std::max(max_score,score);
        max_doc_weight = std::max(max_doc_weight,doc_weight);
        if (i % block_size == 0) {
            m_block_maximums[j] = max_score;
            m_block_max_doc_weights[j] = max_doc_weight;
            m_list_maximuim = std::max(m_list_maximuim,max_score);
            m_max_doc_weight = std::max(m_max_doc_weight,max_doc_weight);
            i = 0;
            max_score = 0.0f;
            max_doc_weight = std::numeric_limits<double>::lowest();
            j++;
        }
        i++;
    }
    if (ids.size() % t_bs != 0) {
        m_block_maximums[num_blocks-1] = max_score;
        m_block_max_doc_weights[num_blocks-1] = max_doc_weight;
    }
    m_list_maximuim = std::max(m_list_maximuim,max_score);
    m_max_doc_weight = std::max(m_max_doc_weight,max_doc_weight);
}


template<uint64_t t_bs>
void postings_list<t_bs>::create_block_support(const sdsl::int_vector<32>& ids,const sdsl::int_vector<32>& freqs)
{
    size_t num_blocks = ids.size() / t_bs;
    if (ids.size() % t_bs != 0) num_blocks++;

    m_block_representatives.resize(num_blocks);
    size_t j = 0;
    for (size_t i=t_bs-1; i<ids.size(); i+=t_bs) {
        m_block_representatives[j++] = ids[i];
    }
    m_block_representatives[j] = ids[ids.size()-1];
    sdsl::util::bit_compress(m_block_representatives);
}

template<uint64_t t_bs>
void postings_list<t_bs>::compress_postings_data(const sdsl::int_vector<32>& ids,const sdsl::int_vector<32>& freqs)
{
    // encode in blocks
    size_t num_blocks = ids.size() / t_bs;
    if (ids.size() % t_bs != 0) num_blocks++;
    m_id_block_ptr.resize(num_blocks+1);
    m_freq_block_ptr.resize(num_blocks+1);
    m_size = ids.size();

    // encode using pfor
    static shared_ptr<IntegerCODEC> codec =
        CODECFactory::getFromName("simdbinarypacking");
    IntegerCODEC& c = *codec;

    // encode ids
    size_t id_start = 0;
    m_docid_data.resize(2 * ids.size() + 1024);
    uint32_t* id_data = m_docid_data.data();
    uint32_t* input = (uint32_t*) ids.data();
    size_t j = 0;
    for (size_t i=0; i<ids.size(); i+=t_bs) {
        m_id_block_ptr[j] = id_start;
        size_t n = t_bs;
        if (i+t_bs >= ids.size()) n = ids.size()-i;
        size_t encoded_size = 0;
        Delta::encode(c,true,&input[i],n,&id_data[id_start],encoded_size);
        id_start += encoded_size;
        j++;
    }
    m_id_block_ptr[j] = id_start;
    m_docid_data.resize(id_start);

    // encode freqs
    size_t freq_start = 0;
    m_freq_data.resize(2 * freqs.size() + 1024);
    uint32_t* freq_data = m_freq_data.data();
    uint32_t* finput = (uint32_t*) freqs.data();
    j = 0;
    for (size_t i=0; i<freqs.size(); i+=t_bs) {
        m_freq_block_ptr[j] = freq_start;
        size_t n = t_bs;
        if (i+t_bs >= freqs.size()) n = freqs.size()-i;
        size_t encoded_size = 0;
        c.encodeArray(&finput[i],n,&freq_data[freq_start],encoded_size);
        freq_start += encoded_size;
        j++;
    }
    m_freq_block_ptr[j] = freq_start;
    m_freq_data.resize(freq_start);

    sdsl::util::bit_compress(m_id_block_ptr);
    sdsl::util::bit_compress(m_freq_block_ptr);
}

template<uint64_t t_bs>
void postings_list<t_bs>::decompress_block(size_t bid,std::vector<uint32_t>& id_data,std::vector<uint32_t>& freq_data) const
{
#ifdef NEWT_PROFILE_QUERY_PERFORMANCE
    query_profiler::record_block_decode();
#endif
    if (id_data.size() != t_bs) { // did we allocate space already?
        id_data.resize(t_bs);
        freq_data.resize(t_bs);
    }
    static shared_ptr<IntegerCODEC> codec =
        CODECFactory::getFromName("simdbinarypacking");
    IntegerCODEC& c = *codec;
    size_t block_start = m_id_block_ptr[bid];
    size_t block_stop = m_id_block_ptr[bid+1];
    size_t compressed_block_size = block_stop-block_start;

    // decode doc ids
    uint32_t* aligned_docids = (uint32_t*)(m_docid_data.data()+block_start);
    size_t num_recovered_ids = 0;
    uint32_t* result_ids = id_data.data();
    Delta::decode(c,true,aligned_docids,compressed_block_size,result_ids,num_recovered_ids);

    // decode freqs (no d-gap as non-increasing)
    block_start = m_freq_block_ptr[bid];
    block_stop = m_freq_block_ptr[bid+1];
    compressed_block_size = block_stop-block_start;
    size_t num_recovered_freqs = 0;
    uint32_t* aligned_freqs = (uint32_t*)(m_freq_data.data()+block_start);
    uint32_t* result_freqs = freq_data.data();
    c.decodeArray(aligned_freqs , compressed_block_size ,result_freqs,num_recovered_freqs);

    if (num_recovered_ids != num_recovered_freqs) {
        std::cerr << "ERROR: number of decoded ids and freqs is not equal. "
                  << num_recovered_ids << " != " << num_recovered_freqs << "\n";
        throw std::logic_error("number of decoded ids and freqs is not equal.");
    }

    if (num_recovered_ids != t_bs) {
        //std::cout << "resize to " << num_recovered_ids << std::endl;
        freq_data.resize(num_recovered_ids);
        id_data.resize(num_recovered_ids);
    }
}

template<uint64_t t_bs>
size_t postings_list<t_bs>::find_block_with_id(uint64_t id,size_t start_block) const
{
    size_t block_id = start_block;
    size_t nblocks = m_block_representatives.size();
    while (block_id < nblocks && m_block_representatives[block_id] < id) {
        block_id++;
    }
    return block_id;
}


template<uint64_t t_bs>
plist_iterator<t_bs>::plist_iterator() : m_cur_pos(std::numeric_limits<uint64_t>::max()),
    m_cur_block_id(std::numeric_limits<uint64_t>::max()),
    m_last_accessed_block(std::numeric_limits<uint64_t>::max()-1),
    m_last_accessed_id(std::numeric_limits<uint64_t>::max()-1),
    m_cur_docid(0),
    m_cur_freq(0),
    m_plist_ptr(nullptr) {}

template<uint64_t t_bs>
plist_iterator<t_bs>::plist_iterator(const list_type& l,size_t pos) : m_cur_pos(pos),
    m_cur_block_id(0),
    m_last_accessed_block(std::numeric_limits<uint64_t>::max()-1),
    m_last_accessed_id(std::numeric_limits<uint64_t>::max()-1),
    m_cur_docid(0),
    m_cur_freq(0),
    m_plist_ptr(&l) {}

template<uint64_t t_bs>
plist_iterator<t_bs>::plist_iterator(const plist_iterator& pi) : m_cur_pos(pi.m_cur_pos),
    m_cur_block_id(pi.m_cur_block_id),
    m_last_accessed_block(pi.m_last_accessed_block),
    m_last_accessed_id(pi.m_last_accessed_id),
    m_cur_docid(pi.m_cur_docid),
    m_cur_freq(pi.m_cur_freq),
    m_plist_ptr(pi.m_plist_ptr),
    m_decoded_ids(pi.m_decoded_ids),
    m_decoded_freqs(pi.m_decoded_freqs)
{
    /*std::cout << "plist itr copy constructor" << std::endl;*/
}

template<uint64_t t_bs>
plist_iterator<t_bs>::plist_iterator(plist_iterator&& pi) : m_cur_pos(pi.m_cur_pos),
    m_cur_block_id(pi.m_cur_block_id),
    m_last_accessed_block(pi.m_last_accessed_block),
    m_last_accessed_id(pi.m_last_accessed_id),
    m_cur_docid(pi.m_cur_docid),
    m_cur_freq(pi.m_cur_freq),
    m_plist_ptr(pi.m_plist_ptr),
    m_decoded_ids(std::move(pi.m_decoded_ids)),
    m_decoded_freqs(std::move(pi.m_decoded_freqs)) { }

template<uint64_t t_bs>
plist_iterator<t_bs>& plist_iterator<t_bs>::operator=(const plist_iterator& pi)
{
    //std::cout << "plist itr copy assignment" << std::endl;
    m_cur_pos = pi.m_cur_pos;
    m_cur_block_id = pi.m_cur_block_id;
    m_last_accessed_block = pi.m_last_accessed_block;
    m_last_accessed_id = pi.m_last_accessed_id;
    m_cur_docid = pi.m_cur_docid;
    m_cur_freq = pi.m_cur_freq;
    m_plist_ptr = pi.m_plist_ptr;
    m_decoded_ids = pi.m_decoded_ids;
    m_decoded_freqs = pi.m_decoded_freqs;
    return *this;
}

template<uint64_t t_bs>
plist_iterator<t_bs>& plist_iterator<t_bs>::operator=(plist_iterator&& pi)
{
    // size_t s = 0;
    // if(m_plist_ptr) s = m_plist_ptr->size();
    // std::cout << "plist itr move assignment " << pi.m_cur_pos << " -> "
    //          << m_cur_pos << " - " << s
    //          << " S: " << m_decoded_ids.size() << " OS: " << pi.m_decoded_ids.size() << std::endl;
    m_cur_pos = pi.m_cur_pos;
    m_cur_block_id = pi.m_cur_block_id;
    m_last_accessed_block = pi.m_last_accessed_block;
    m_last_accessed_id = pi.m_last_accessed_id;
    m_cur_docid = pi.m_cur_docid;
    m_cur_freq = pi.m_cur_freq;
    m_plist_ptr = pi.m_plist_ptr;
    m_decoded_ids = std::move(pi.m_decoded_ids);
    m_decoded_freqs = std::move(pi.m_decoded_freqs);
    return *this;
}

template<uint64_t t_bs>
plist_iterator<t_bs>& plist_iterator<t_bs>::operator++()
{
    if (m_cur_pos != size()) { // end?
        (*this).m_cur_pos++;
    } else {
        std::cerr << "ERROR: trying to advance plist iterator beyond list end.\n";
        throw std::out_of_range("trying to advance plist iterator beyond list end");
    }
    return (*this);
}

template<uint64_t t_bs>
bool plist_iterator<t_bs>::operator ==(const plist_iterator& b) const
{
    return ((*this).m_cur_pos == b.m_cur_pos) && ((*this).m_plist_ptr == b.m_plist_ptr);
}

template<uint64_t t_bs>
bool plist_iterator<t_bs>::operator !=(const plist_iterator& b) const
{
    return !((*this)==b);
}

template<uint64_t t_bs>
size_t plist_iterator<t_bs>::size() const
{
    return m_plist_ptr->size();
}

template<uint64_t t_bs>
double plist_iterator<t_bs>::block_max() const
{
    return m_plist_ptr->block_max(m_cur_block_id);
}

template<uint64_t t_bs>
double plist_iterator<t_bs>::block_max_doc_weight() const
{
    return m_plist_ptr->block_max_doc_weight(m_cur_block_id);
}

template<uint64_t t_bs>
uint64_t plist_iterator<t_bs>::block_rep() const
{
    return m_plist_ptr->block_rep(m_cur_block_id);
}

template<uint64_t t_bs>
typename plist_iterator<t_bs>::value_type plist_iterator<t_bs>::docid() const
{
    if (m_cur_pos == m_plist_ptr->size()) { // end?
        std::cerr << "ERROR: plist iterator dereferenced at list end.\n";
        throw std::out_of_range("plist iterator dereferenced at list end");
    }
    if (m_cur_pos == m_last_accessed_id) {
        return m_cur_docid;
    }
    access_and_decode_cur_pos();
    return m_cur_docid;
}

template<uint64_t t_bs>
typename plist_iterator<t_bs>::value_type plist_iterator<t_bs>::freq() const
{
    if (m_cur_pos == m_plist_ptr->size()) { // end?
        std::cerr << "ERROR: plist iterator dereferenced at list end.\n";
        throw std::out_of_range("plist iterator dereferenced at list end");
    }
    if (m_cur_pos == m_last_accessed_id) {
        return m_cur_freq;
    }
    access_and_decode_cur_pos();
    return m_cur_freq;
}

template<uint64_t t_bs>
void plist_iterator<t_bs>::access_and_decode_cur_pos() const
{
    m_cur_block_id = m_cur_pos / t_bs;
    if (m_cur_block_id != m_last_accessed_block) {  // decompress block
        m_last_accessed_block = m_cur_block_id;
        m_plist_ptr->decompress_block(m_cur_block_id,m_decoded_ids,m_decoded_freqs);
    }
    size_t in_block_offset = m_cur_pos % t_bs;
    m_cur_docid = m_decoded_ids[in_block_offset];
    m_cur_freq = m_decoded_freqs[in_block_offset];
    m_last_accessed_id = m_cur_pos;
}

template<uint64_t t_bs>
void plist_iterator<t_bs>::skip_to_block_with_id(uint64_t id)
{
#ifdef NEWT_PROFILE_QUERY_PERFORMANCE
    query_profiler::record_skip_block();
#endif
    skip_to_block_with_id_prv(id);
}

template<uint64_t t_bs>
void plist_iterator<t_bs>::skip_to_block_with_id_prv(uint64_t id)
{
    size_t old_block = m_cur_block_id;
    m_cur_block_id = m_plist_ptr->find_block_with_id(id,m_cur_block_id);

    // we now go to the first id in the new block!
    if (old_block != m_cur_block_id) {
        m_cur_pos = m_cur_block_id*t_bs;
        if (m_cur_pos > m_plist_ptr->size()) { // don't go past the end!
            m_cur_pos = m_plist_ptr->size();
        }
    }
}

template<uint64_t t_bs>
void plist_iterator<t_bs>::skip_to_id(uint64_t id)
{
#ifdef NEWT_PROFILE_QUERY_PERFORMANCE
    query_profiler::record_skip_id();
#endif
    skip_to_block_with_id(id);
    // check if we reached list end!
    if (m_cur_block_id >= m_plist_ptr->num_blocks()) {
        m_cur_pos = m_plist_ptr->size();
        return;
    }
    if (m_last_accessed_block != m_cur_block_id) {
        m_last_accessed_block = m_cur_block_id;
        //std::cout << "skip decompress" << std::endl;
        m_plist_ptr->decompress_block(m_cur_block_id,m_decoded_ids,m_decoded_freqs);
        //std::cout << "size = " << m_decoded_ids.size() << std::endl;
        // new block -> find from the beginning
        auto block_itr = std::lower_bound(m_decoded_ids.begin(),m_decoded_ids.end(),id);
        m_cur_pos = (t_bs*m_cur_block_id) + std::distance(m_decoded_ids.begin(),block_itr);
    } else {
        size_t in_block_offset = m_cur_pos % t_bs;
        auto block_itr = std::lower_bound(m_decoded_ids.begin()+in_block_offset,m_decoded_ids.end(),id);
        m_cur_pos = (t_bs*m_cur_block_id) + std::distance(m_decoded_ids.begin(),block_itr);
    }
    size_t inblock_offset = m_cur_pos % t_bs;
    m_cur_docid = m_decoded_ids[inblock_offset];
    m_cur_freq = m_decoded_freqs[inblock_offset];
    m_last_accessed_id = m_cur_pos;
}


#endif
