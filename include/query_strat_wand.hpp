#ifndef QUERY_STRAT_WAND_HPP
#define QUERY_STRAT_WAND_HPP

#include "settings.hpp"
#include "result.hpp"
#include "query.hpp"
#include "score_heap.hpp"
#include "query_profiler.hpp"

#include <limits>
#include <random>
#include <unordered_set>

//#define DEBUG_WAND 1

class query_strategy_wand
{
    public:
        template<class t_pl,class rank_func>
        static ranked_result_list_t
        process(std::vector<plist_data<t_pl>*>& postings_lists,const rank_func& ranker,size_t k) {
            score_heap heap(k,0.0);

            double query_factor = 0;
            for (const auto& pl : postings_lists) {
                query_factor += pl->qt->term_weight;
            }

            sort_list_by_id(postings_lists);
            auto list_and_score = determine_next_candidate(postings_lists,heap.threshold(),query_factor);
            auto pivot_list = std::get<0>(list_and_score);
            auto potential_score = std::get<1>(list_and_score);

            // perform wand
            while (pivot_list != postings_lists.end()) {
#ifdef DEBUG_WAND
                std::cout << "pivot = " << (*pivot_list)->cur.docid() << std::endl;
#endif

                if (postings_lists[0]->cur.docid() == (*pivot_list)->cur.docid()) {
                    // all lists above us are on the pivot. evaluate
                    evaluate_pivot(postings_lists,ranker,heap,query_factor,potential_score);
                } else {
                    // there are lists pointing to doc_ids smaller than the pivot.
                    // move them ahead
                    forward_lists(postings_lists,pivot_list-1,(*pivot_list)->cur.docid());
                }

                auto list_and_score = determine_next_candidate(postings_lists,heap.threshold(),query_factor);
                pivot_list = std::get<0>(list_and_score);
                potential_score = std::get<1>(list_and_score);
            }

            // create the result from the heap and return
            return heap.elems();
        }
    public:
        template<class t_pl>
        static typename std::vector<plist_data<t_pl>*>::iterator
        find_shortest_list(std::vector<plist_data<t_pl>*>& postings_lists,const typename std::vector<plist_data<t_pl>*>::iterator& end,uint64_t id) {
            auto itr = postings_lists.begin();
            if (itr != end) {
                size_t smallest = std::numeric_limits<size_t>::max();
                auto smallest_itr = itr;
                while (itr != end) {
                    if ((*itr)->cur.remaining() < smallest && (*itr)->cur.docid() != id) {
                        smallest = (*itr)->cur.remaining();
                        smallest_itr = itr;
                    }
                    ++itr;
                }
                return smallest_itr;
            }
            return end;
        }

        template<class t_pl>
        static void
        sort_list_by_id(std::vector<plist_data<t_pl>*>& postings_lists) {
            // check if we can delete stuff
            auto ditr = postings_lists.begin();
            while (ditr != postings_lists.end()) {
                if ((*ditr)->cur == (*ditr)->end) {
                    ditr = postings_lists.erase(ditr);
                } else {
                    ditr++;
                }
            }
            // sort
            auto ptr_sort = [](const plist_data<t_pl>* a,const plist_data<t_pl>* b) {
                return *a < *b;
            };
            std::sort(postings_lists.begin(),postings_lists.end(),ptr_sort);
        }

        template<class t_pl>
        static void
        forward_lists(std::vector<plist_data<t_pl>*>& postings_lists,const typename std::vector<plist_data<t_pl>*>::iterator& pivot_list,uint64_t id) {
            auto smallest_itr = find_shortest_list(postings_lists,pivot_list+1,id);

#ifdef DEBUG_WAND
            std::cout << "forward_lists = " << id << std::endl;
#endif

            // advance the smallest list to the new id
            (*smallest_itr)->cur.skip_to_id(id);

            if ((*smallest_itr)->cur == (*smallest_itr)->end) {
                // list is finished! reorder list by id
                sort_list_by_id(postings_lists);
            }

            // bubble it down!
            auto next = smallest_itr + 1;
            auto list_end = postings_lists.end();
            while (next != list_end && (*smallest_itr)->cur.docid() > (*next)->cur.docid()) {
                std::swap(*smallest_itr,*next);
                smallest_itr = next;
                next++;
            }
        }

        template<class t_pl,class rank_func>
        static void
        evaluate_pivot(std::vector<plist_data<t_pl>*>& postings_lists,const rank_func& ranker,score_heap& heap,double query_factor,double potential_score) {
#ifdef NEWT_PROFILE_QUERY_PERFORMANCE
            query_profiler::record_evaluation();
#endif
            auto doc_id = postings_lists[0]->cur.docid();
            double W_d = ranker.doc_length(doc_id);
            double doc_score = query_factor * ranker.calc_doc_weight(W_d);
            potential_score -= doc_score;

#ifdef DEBUG_WAND
            std::cout << "evaluate_pivot = " << doc_id << std::endl;
#endif

            auto itr = postings_lists.begin();
            auto end = postings_lists.end();
            while (itr != end) {
                if ((*itr)->cur.docid() == doc_id) {
                    double contrib = ranker.calculate_impact((*itr)->qt->f_qt,
                                     (*itr)->cur.freq(),(double)(*itr)->qt->f_t,
                                     (*itr)->qt->F_t,W_d,(*itr)->qt->term_weight);
                    doc_score += contrib;
                    potential_score += contrib;
                    potential_score -= ((*itr)->qt->term_weight * (*itr)->data->list_max_score());
                    ++((*itr)->cur); // move to next larger!
                    if (potential_score < heap.threshold()) {
                        break;
                    }
                } else {
                    break;
                }
                itr++;
            }
            // add the doc score
            heap.insert_if_higher_score( {doc_id,doc_score});

            // resort
            sort_list_by_id(postings_lists);
        }


        template<class t_pl>
        static std::pair<typename std::vector<plist_data<t_pl>*>::iterator,double>
        determine_next_candidate(std::vector<plist_data<t_pl>*>& postings_lists,double threshold,double query_factor) {
            double score = 0;
            double max_doc_score = std::numeric_limits<double>::lowest();
            double total_score = 0;
            auto itr = postings_lists.begin();
            auto end = postings_lists.end();
            while (itr != end) {
                score += ((*itr)->qt->term_weight * (*itr)->data->list_max_score());
                max_doc_score = std::max(max_doc_score,(*itr)->data->max_doc_weight());
                total_score = score + (max_doc_score*query_factor);
#ifdef DEBUG_WAND
                std::cout << "[" << score << "] -> ";
#endif
                if (total_score > threshold) {
                    // forward to last list with id equal to pivot!
                    uint64_t pivot_id = (*itr)->cur.docid();
#ifdef DEBUG_WAND
                    std::cout << "FOUND PIVOT ID = " << pivot_id;
#endif
                    auto next = itr+1;
                    while (next != end && (*next)->cur.docid() == pivot_id) {
                        itr = next;
                        score += (*itr)->data->list_max_score();
                        max_doc_score = std::max(max_doc_score,(*itr)->data->max_doc_weight());
                        total_score = score + (max_doc_score*query_factor);
#ifdef DEBUG_WAND
                        std::cout << " ADVANCE [" << score << "]";
#endif
                        next++;
                    }
#ifdef DEBUG_WAND
                    std::cout << std::endl;
#endif
                    return {itr,score};
                }
                itr++;
            }
#ifdef DEBUG_WAND
            std::cout << std::endl;
#endif
            return {end,score};
        }
};


#endif
