#ifndef DOCUMENT_ARRAY_BITCOMPRESSED_HPP
#define DOCUMENT_ARRAY_BITCOMPRESSED_HPP

#include <sdsl/io.hpp>
#include <sdsl/suffix_trees.hpp>

#include <utility> // for std::pair
#include <map>     // for std::map

#include "logger.hpp"
#include "settings.hpp"

using namespace sdsl;

template<class t_csa>
class _document_array_bitcompressed
{
    public:
        typedef uint64_t                 value_type;
        typedef int_vector<>::size_type  size_type;
    private:
        t_csa*              mp_csa = nullptr;
        int_vector<>        m_array;
        size_type           m_num_documents = 1;
        int_vector<>        m_doc_start_row;
    public:
        const size_type&    num_documents = m_num_documents;

        _document_array_bitcompressed() {};

        _document_array_bitcompressed(_document_array_bitcompressed&& other) :
            m_array(std::move(other.m_array)),
            m_num_documents(other.m_num_documents) {
        }

        _document_array_bitcompressed& operator=(_document_array_bitcompressed&& other) {
            m_array = std::move(other.m_array);
            return *this;
        }

        //! Constructor
        _document_array_bitcompressed(cache_config& config) {

            // iterate over text and determine document bounds
            newt_log::info() << "\t- Marking document boundaries (" << newt_constants::document_delimiter << ")" << std::endl;
            int_vector_buffer<> text_buf(config.file_map[newt_constants::KEY_INPUT].c_str(),std::ios_base::in);
            size_type n = text_buf.size();
            bit_vector doc_indicators(n+1); // to account for the appended \0
            for (size_type i=0; i < n; ++i) {
                if (text_buf[i] == newt_constants::document_delimiter) {
                    m_num_documents++;
                    doc_indicators[i] = 1;
                }
            }
            newt_log::info() << "\t- Found " << m_num_documents << " documents (" << bits::hi(m_num_documents)+1 << " bps)" << std::endl;

            // create the document array by iterating over the suffix array and
            // performing a rank for each suffix position
            newt_log::info() << "\t- Mapping suffix positions" << std::endl;
            int_vector_buffer<> sa_buf(config.file_map[conf::KEY_SA].c_str(),std::ios_base::in);
            map_suffix_positions(doc_indicators,sa_buf);
        }

        void map_suffix_positions(const bit_vector& doc_indicators,int_vector_buffer<>& sa) {
            rank_support_v<1> docrank(&doc_indicators);
            size_type n = sa.size();
            m_array.width(bits::hi(m_num_documents)+1);
            m_array.resize(n+1); // to account for the appended \0
            for (size_type i=0; i < n; ++i) {
                value_type suffix_pos = sa[i];
                m_array[i] = docrank.rank(suffix_pos);
            }
            m_doc_start_row.resize(m_num_documents);
            for (size_type i=1; i <= m_num_documents; i++) {
                m_doc_start_row[m_array[i]] = i;
            }
            util::bit_compress(m_doc_start_row);
        }

        void set_csa(t_csa* csa) {
            mp_csa = csa;
        }

        size_t size() const {
            return m_array.size();
        };

        void load(std::istream& in) {
            m_array.load(in);
            read_member(m_num_documents,in);
            m_doc_start_row.load(in);
        }

        size_t serialize(std::ostream& out, structure_tree_node* v=NULL, std::string name="") const {
            structure_tree_node* child = structure_tree::add_child(v, name, util::class_name(*this));
            size_t written_bytes = 0;
            written_bytes += m_array.serialize(out,child,"data");
            written_bytes += write_member(m_num_documents,out,child,"num_documents");
            written_bytes += m_doc_start_row.serialize(out,child,"start row data");
            structure_tree::add_size(child, written_bytes);
            return written_bytes;
        }

        inline value_type operator[](size_type i) const {
            return m_array[i];
        }

        size_type doc_start_decode(size_type i) const {
            return m_doc_start_row[i];
        }

};


struct document_array_bitcompressed {
    template<class t_csa>
    using type = _document_array_bitcompressed<t_csa>;
};

#endif
