#ifndef INVERTED_INDEX_HPP
#define INVERTED_INDEX_HPP

#include "sdsl/suffix_arrays.hpp"

#include "postings_list.hpp"
#include "document_arrays.hpp"
#include "settings.hpp"
#include "logger.hpp"
#include "document_map.hpp"
#include "term_map.hpp"
#include "rank_functions.hpp"
#include "plist_position_support.hpp"
#include "query.hpp"
#include "query_strategies.hpp"

#include <utility>  // for std::pair
#include <map>      // for std::map
#include <future>

using namespace sdsl;

/*!
 *  simple inverted index
 */
template<class t_csa,class t_da,class t_rank,uint64_t t_plist_block_size>
class inverted_index
{
    public:
        // types
        typedef std::pair<size_t, size_t>                                                 tPII;
        typedef t_rank                                                                    rank_type;
        typedef t_csa                                                                     csa_type;
        typedef postings_list<t_plist_block_size>                                         plist_type;
        typedef plist_position_support                                                    pos_list_type;
        typedef typename t_da::template type<t_csa>                                       da_type;
        typedef typename csa_type::size_type                                              size_type;
        typedef typename std::unordered_map<tPII , plist_type,pair_hash>::const_iterator  const_iterator;
    private:
        size_t                                                                            m_N;
        std::unordered_map<tPII,plist_type,pair_hash>                                     m_plist_store;
        std::unordered_map<tPII,pos_list_type,pair_hash>                                  m_poslist_store;
        document_map		                                                              m_document_map;
        term_map					                                                      m_term_map;
        std::unordered_map<uint64_t,tPII>                                                 m_term_range_map;
    private:
        mutable std::unordered_map<uint64_t , plist_type>                                 m_onthefly_lists;
        mutable std::unordered_map<uint64_t , pos_list_type>                              m_onthefly_poslist;
    private:
        void compute_positions(const tPII& range,int_vector_buffer<>& sa,da_type& D,std::vector<uint64_t>& starts);
        std::vector<tPII> determine_ranges(cache_config& config);
        void compute_plists_from_ranges(cache_config& config,std::vector<tPII>& ranges_to_store);
        auto store_plists(std::ostream& out, structure_tree_node* v=NULL, std::string name="") const -> size_type;
        void load_plists(std::istream& in,std::vector<std::pair<size_t,size_t>>& ranges);
        auto store_ranges(std::ostream& out, structure_tree_node* v=NULL, std::string name="") const -> size_type;
        std::vector<tPII> load_ranges(std::istream& in);
    public:
        auto serialize(std::ostream& out, structure_tree_node* v=NULL, std::string name="") const -> size_type;
        inverted_index(cache_config& config);
        inverted_index() = default;
        void load(std::istream& in);
        std::string type_info() const;
        void clear_onthefly() const {
            m_onthefly_lists.clear();
            m_onthefly_poslist.clear();
        }
        std::vector<plist_data<plist_type>> lookup_and_create_postings(query& qry) const;
        void compute_phrase_list(qterm& qt,std::vector<plist_data<plist_type>>& postings) const;
    public:
        const term_map&                                                                     termmap = m_term_map;
        const document_map&                                                                 docmap = m_document_map;
};

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size>
void
inverted_index<t_csa,t_da,t_rank,t_plist_block_size>::compute_phrase_list(qterm& qt,std::vector<plist_data<plist_type>>& postings) const
{
    std::cout << "compute_phrase_list for [";
    for (const auto& t : qt.tokens) std::cout << t << ",";
    std::cout << "]" << std::endl;

    // (0) make sure it doesn't exist already!
    std::hash<qterm> qt_fn;
    std::size_t qt_hash = qt_fn(qt);
    auto list_itr = m_onthefly_lists.find(qt_hash);
    if (list_itr != m_onthefly_lists.end()) {
        postings.emplace_back(plist_data<plist_type>(list_itr->second,qt));
        return;
    }

    // (1) find two lists to intersect
    using pl_type = plist_data<postings_list<t_plist_block_size>>;
    using pos_type = plist_position_support;
    //
    std::vector<std::tuple<bool,const pos_type*,pl_type*>> intersect_lists;
    size_t phrase_len = qt.tokens.size();
    size_t part_list_len = phrase_len - 1;
    size_t found = 0;
    for (auto& pl : postings) {
        auto prev_found = found;
        bool first = false;
        if (pl.qt->tokens.size() == part_list_len) {
            if (std::equal(pl.qt->tokens.begin(),pl.qt->tokens.end(),qt.tokens.begin())) {
                std::cout << "found first part of phrase :[";
                for (const auto& t : pl.qt->tokens) std::cout << t << ",";
                std::cout << "]" << std::endl;

                first = true;
                found++;
            } else {
                if (std::equal(pl.qt->tokens.begin(),pl.qt->tokens.end(),qt.tokens.begin()+1)) {
                    std::cout << "found second part of phrase :[";
                    for (const auto& t : pl.qt->tokens) std::cout << t << ",";
                    std::cout << "]" << std::endl;
                    found++;
                }
            }
        }
        if (found != prev_found) {
            tPII range(pl.qt->sp,pl.qt->ep);
            auto pos_itr = m_poslist_store.find(range);
            if (pos_itr == m_poslist_store.end()) {
                std::size_t q_hash = qt_fn(*(pl.qt));
                auto opos_itr = m_onthefly_poslist.find(q_hash);
                if (opos_itr == m_onthefly_poslist.end()) {
                    std::cerr << "need positions to perform intersection" << std::endl;
                    return;
                }
                intersect_lists.emplace_back(first,&(opos_itr->second),&pl);
            } else {
                intersect_lists.emplace_back(first,&(pos_itr->second),&pl);
            }
        }
        if (found == 2) {
            break;
        }
    }
    if (found != 2) {
        // nothing to be done if one of the parts already doesn't exist!
        return;
    }

    // (2) intersect lists
    // std::cout << "intersecting lists" << std::endl;
    // std::cout << "size first = " << std::get<2>(intersect_lists[0])->data->size() << std::endl;
    // std::cout << "size second = " << std::get<2>(intersect_lists[1])->data->size() << std::endl;

    size_t new_F_t = 0;
    std::vector< std::pair<uint64_t,uint64_t> > new_posting_entries;
    std::vector< std::vector<uint32_t> > positions_for_new_posting;
    std::vector<uint32_t> pos_first;
    std::vector<uint32_t> pos_second;
    while (std::get<2>(intersect_lists[0])->cur != std::get<2>(intersect_lists[0])->end) {
        auto id = std::get<2>(intersect_lists[0])->cur.docid();
        std::get<2>(intersect_lists[1])->cur.skip_to_id(id);
        if (std::get<2>(intersect_lists[1])->cur == std::get<2>(intersect_lists[1])->end) {
            break;
        }

        if (std::get<2>(intersect_lists[1])->cur.docid() == id) {
            // found! now perform position based intersection
            //std::cout << "FOUND id = " << id << std::endl;

            // (a) retrieve lists
            auto first_offset = std::get<2>(intersect_lists[0])->cur.offset();
            auto second_offset = std::get<2>(intersect_lists[1])->cur.offset();

            // std::cout << "first offset = " << first_offset << std::endl;
            // std::cout << "second offset = " << second_offset << std::endl;

            // std::cout << "first ptr = " << std::get<1>(intersect_lists[0])->m_list_starts.size() << std::endl;
            // std::cout << "second ptr = " << std::get<1>(intersect_lists[1])->m_list_starts.size() << std::endl;


            auto num_first = std::get<1>(intersect_lists[0])->access_positions(pos_first,first_offset);
            // std::cout << "pos first = [";
            // for(size_t i=0;i<num_first;i++) std::cout << pos_first[i] << ",";
            // std::cout << "]" << std::endl;

            auto num_second = std::get<1>(intersect_lists[1])->access_positions(pos_second,second_offset);
            // std::cout << "pos second = [";
            // for(size_t i=0;i<num_second;i++) std::cout << pos_second[i] << ",";
            // std::cout << "]" << std::endl;

            // (a1) make pos equal so intersection is easier
            if (std::get<0>(intersect_lists[0]) == false) {
                for (size_t i=0; i<num_first; i++) {
                    if (pos_first[i] != 0) {
                        pos_first[i]--;
                    }
                }
            } else {
                for (size_t i=0; i<num_second; i++) {
                    if (pos_second[i] != 0) {
                        pos_second[i]--;
                    }
                }
            }

            // std::cout << "fixed pos first = [";
            // for(size_t i=0;i<num_first;i++) std::cout << pos_first[i] << ",";
            // std::cout << "]" << std::endl;
            // std::cout << "fixed pos second = [";
            // for(size_t i=0;i<num_second;i++) std::cout << pos_second[i] << ",";
            // std::cout << "]" << std::endl;

            // (b) pos intersect
            std::vector<uint32_t> intersected_pos;
            std::set_intersection(pos_first.begin(),pos_first.begin()+num_first,
                                  pos_second.begin(),pos_second.begin()+num_second,
                                  std::back_inserter(intersected_pos));

            // std::cout << "intersection = [";
            // for(size_t i=0;i<intersected_pos.size();i++) std::cout << intersected_pos[i] << ",";
            // std::cout << "]" << std::endl;

            // add doc_id pair
            if (intersected_pos.size() > 0) {
                //
                // std::cout << "new postings entry = <" << id << "," << intersected_pos.size() << std::endl;
                new_posting_entries.emplace_back(id,intersected_pos.size());
                positions_for_new_posting.emplace_back(intersected_pos);
                new_F_t += intersected_pos.size();
            }


            ++(std::get<2>(intersect_lists[1])->cur);
        }
        ++(std::get<2>(intersect_lists[0])->cur);


        if (std::get<2>(intersect_lists[0])->cur.remaining() > std::get<2>(intersect_lists[1])->cur.remaining()) {
            std::swap(intersect_lists.front(),intersect_lists.back());
        }
    }

    // (2a) reset the iterators of the involved lists
    std::get<2>(intersect_lists[0])->reset();
    std::get<2>(intersect_lists[1])->reset();

    // (3) create postings list from results if there was a result!
    if (new_posting_entries.size() > 0) {
        t_rank ranker(m_N,m_document_map);
        qt.F_t = new_F_t;
        auto oinsert_listitr = m_onthefly_lists.emplace(qt_hash,plist_type(ranker,new_posting_entries));
        /*auto oinsert_positr = */
        auto oinsert_positr = m_onthefly_poslist.emplace(qt_hash,plist_position_support(positions_for_new_posting));
        // (4) add to the postings of the query
        auto& pl = oinsert_listitr.first->second;
        auto& pos = oinsert_positr.first->second;
        std::cout << "new pl = ";
        auto itr = pl.begin();
        auto end = pl.end();
        std::vector<uint32_t> pos_decoded;
        while (itr != end) {
            std::cout << "<" << itr.docid() << "," << itr.freq() << ">|";
            auto num_decoded = pos.access_positions(pos_decoded,itr.offset());
            for (size_t o=0; o<num_decoded; o++) {
                std::cout << "[" << pos_decoded[o] << "]";
            }
            std::cout << "|";
            ++itr;
        }
        std::cout << std::endl;

        postings.emplace_back(plist_data<plist_type>(pl,qt));
    }
}

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size>
std::vector<plist_data<postings_list<t_plist_block_size>>>
inverted_index<t_csa,t_da,t_rank,t_plist_block_size>::lookup_and_create_postings(query& qry) const
{
    std::vector<plist_data<postings_list<t_plist_block_size>>> lists;
    // (a) find all the singleton lists first
    bool contains_uncomputed_phrases = false;
    for (auto& qt : qry.terms) {
        std::cout << "searching for postings list for [";
        for (const auto& t : qt.tokens) std::cout << t << ",";
        std::cout << "]" << std::endl;

        if (qt.tokens.size() == 1) {
            // singleton -> just look it up
            // (1) map id to range
            auto range_itr = m_term_range_map.find(qt.tokens[0]);
            if (range_itr != m_term_range_map.end()) {
                auto range = range_itr->second;
                std::cout << "token id = " << qt.tokens[0] << " range = <" << range.first << "," << range.second << ">" << std::endl;
                // (2) compute F_t
                qt.sp = range.first;
                qt.ep = range.second;
                qt.F_t = qt.ep - qt.sp + 1;
                // (3) retrieve the list
                auto list_itr=m_plist_store.find(range);
                if (list_itr != m_plist_store.end()) {
                    std::cout << "found precomputed postings list of size " << list_itr->second.size() << std::endl;
                    lists.emplace_back(list_itr->second,qt);
                }
            }
        } else {
            contains_uncomputed_phrases = true;
        }
    }
    // (b) if we have phrases we need to perform list intersection
    size_t next_phrase_len_to_compute = 2;
    while (contains_uncomputed_phrases) {
        std::cout << "computing lists for phrases of length " << next_phrase_len_to_compute << std::endl;
        contains_uncomputed_phrases = false;
        for (auto& qt : qry.terms) {
            if (qt.tokens.size() == next_phrase_len_to_compute) {
                compute_phrase_list(qt,lists);
            }
            if (qt.tokens.size() > next_phrase_len_to_compute) {
                contains_uncomputed_phrases = true;
            }
        }
        next_phrase_len_to_compute++; // move on to larger phrases
    }

    return lists;
}

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size>
std::string inverted_index<t_csa,t_da,t_rank,t_plist_block_size>::type_info() const
{
    std::stringstream type_name;
    type_name << "inverted_index-"
              << typeid(t_rank).name() << "-"
              << t_plist_block_size;
    return type_name.str();
}

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size>
inverted_index<t_csa,t_da,t_rank,t_plist_block_size>::inverted_index(cache_config& config)
{
    // make sure we don't delete stuff
    config.delete_files = false;

    // (1) check, if the compressed suffix array exists
    //     ->  also creates the bwt and the suffix array
    if (!cache_file_exists(newt_constants::KEY_CSA+"_"+util::class_to_hash(csa_type()), config)) {
        newt_log::info() << "[CONSTRUCT] compressed suffix array" << std::endl;
        csa_type csa;
        construct(csa, config.file_map[newt_constants::KEY_INPUT], config, 0);
        store_to_cache(csa,newt_constants::KEY_CSA+"_"+util::class_to_hash(csa), config);
        newt_log::info() << "[SIZE] compressed suffix array " << size_in_mega_bytes(csa) << " MB" << std::endl;
        register_cache_file(newt_constants::KEY_CSA+"_"+util::class_to_hash(csa), config);
    } else {
        newt_log::info() << "[SKIP] compressed suffix array construction" << std::endl;
        register_cache_file(conf::KEY_SA, config);
        csa_type csa;
        register_cache_file(newt_constants::KEY_CSA+"_"+util::class_to_hash(csa), config);
    }

    // (2) create the D array
    //     -> uses the suffix array created in the csa step
    if (!cache_file_exists(newt_constants::KEY_DARRAY+"_"+util::class_to_hash(da_type()), config)) {
        newt_log::info() << "[CONSTRUCT] document array" << std::endl;
        da_type tmp_darray(config);
        store_to_cache(tmp_darray, newt_constants::KEY_DARRAY+"_"+util::class_to_hash(tmp_darray), config);
        newt_log::info() << "[SIZE] document array " << size_in_mega_bytes(tmp_darray) << " MB" << std::endl;
    } else {
        newt_log::info() << "[SKIP] document array construction" << std::endl;
    }
    register_cache_file(newt_constants::KEY_DARRAY+"_"+util::class_to_hash(da_type()), config);


    // (3) create the document map
    //     -> uses the document array
    if (!cache_file_exists(newt_constants::KEY_DOCMAP, config)) {
        newt_log::info() << "[CONSTRUCT] document map" << std::endl;
        m_document_map = document_map(config);
        store_to_cache(m_document_map,newt_constants::KEY_DOCMAP, config);
        newt_log::info() << "[SIZE] document map " << size_in_mega_bytes(m_document_map) << " MB" << std::endl;
    } else {
        load_from_cache(m_document_map,newt_constants::KEY_DOCMAP, config);
        newt_log::info() << "[LOAD] document map" << std::endl;
    }
    register_cache_file(newt_constants::KEY_DOCMAP, config);


    // (4) create the term map
    //
    if (!cache_file_exists(newt_constants::KEY_TERMMAP, config)) {
        newt_log::info() << "[CONSTRUCT] term map" << std::endl;
        m_term_map = term_map(config);
        store_to_cache(m_term_map, newt_constants::KEY_TERMMAP, config);
        newt_log::info() << "[SIZE] term map " << size_in_mega_bytes(m_term_map) << " MB" << std::endl;
    } else {
        newt_log::info() << "[LOAD] term map" << std::endl;
        load_from_cache(m_term_map, newt_constants::KEY_TERMMAP, config);
    }
    register_cache_file(newt_constants::KEY_TERMMAP, config);


    // (5) compute the ranges for which we will store stuff
    std::vector<tPII> ranges_to_store;
    if (!cache_file_exists(std::string("inverted-index")+"_"+std::string(newt_constants::KEY_RANGES), config)) {
        newt_log::info() << "[CONSTRUCT] term ranges" << std::endl;
        ranges_to_store = determine_ranges(config);
        std::ofstream rofs(cache_file_name(std::string("inverted-index")+"_"+std::string(newt_constants::KEY_RANGES),config));
        store_ranges(rofs);
    } else {
        // we computed the ranges before. load them instead!
        newt_log::info() << "[LOAD] term ranges" << std::endl;
        std::ifstream rifs(cache_file_name(std::string("inverted-index")+"_"+std::string(newt_constants::KEY_RANGES),config));
        ranges_to_store = load_ranges(rifs);
    }

    // (6) compute the things we store
    uint64_t num_terms = m_document_map.num_documents();
    t_rank ranker(num_terms,m_document_map);
    if (!cache_file_exists(std::string("inverted-index")+"_"+
                           std::string(newt_constants::KEY_PLISTS)+"_"+
                           util::class_to_hash(ranker)+"_"+
                           std::to_string(t_plist_block_size), config)) {
        newt_log::info() << "[COMPUTE] postings lists from ranges" << std::endl;
        compute_plists_from_ranges(config,ranges_to_store);
        std::ofstream rofs(cache_file_name(std::string("inverted-index")+"_"+
                                           std::string(newt_constants::KEY_PLISTS)+"_"+
                                           util::class_to_hash(ranker)+"_"+
                                           std::to_string(t_plist_block_size),config));
        newt_log::info() << "[STORE] postings lists and position lists" << std::endl;
        store_plists(rofs);
    } else {
        newt_log::info() << "[LOAD] postings lists" << std::endl;
        std::ifstream rifs(cache_file_name(std::string("inverted-index")+"_"+
                                           std::string(newt_constants::KEY_PLISTS)+"_"+
                                           util::class_to_hash(ranker)+"_"+
                                           std::to_string(t_plist_block_size),config));
        load_plists(rifs,ranges_to_store);
    }

    // (7) determine N for ranking purposes
    int_vector_buffer<> sa(config.file_map[conf::KEY_SA].c_str(),std::ios_base::in);
    m_N = sa.size() - m_document_map.num_documents();


    newt_log::info() << "[TOTAL INDEX SIZE] " << size_in_mega_bytes(*this) << " MB" << std::endl;
}

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size>
std::vector<std::pair<size_t,size_t>>
                                   inverted_index<t_csa,t_da,t_rank,t_plist_block_size>::determine_ranges(cache_config& config)
{
    std::vector<tPII> ranges_to_store;
    csa_type csa;
    newt_log::info() << "[LOAD] compressed suffix array" << std::endl;
    load_from_cache(csa,newt_constants::KEY_CSA+"_"+util::class_to_hash(csa), config);
    auto itr = termmap.begin();
    std::vector<uint64_t> term(1);
    size_t sp,ep;
    while (itr != termmap.end()) {
        term[0] = itr->second;
        sdsl::backward_search(csa,0ULL,csa.size()-1,term.begin(),term.end(),sp,ep);
        //if ((ep-sp+1) <= m_document_map.num_documents()) {
        m_term_range_map.emplace(itr->second,make_pair(sp,ep));
        ranges_to_store.emplace_back(sp,ep);
        //}
        ++itr;
    }

    std::mt19937 g(4711);
    std::shuffle(ranges_to_store.begin(),ranges_to_store.end(), g);

    return ranges_to_store;
}


template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size>
void inverted_index<t_csa,t_da,t_rank,t_plist_block_size>::compute_plists_from_ranges(cache_config& config,std::vector<tPII>& ranges_to_store)
{
    using clock = std::chrono::high_resolution_clock;
    da_type D;
    auto precompute_start = clock::now();
    newt_log::info() << "[LOAD] document array" << std::endl;
    load_from_cache(D, newt_constants::KEY_DARRAY+"_"+util::class_to_hash(D), config);

    // find max range for tmp storage reqs
    auto range_size_cmp = [](const tPII& a,const tPII& b) {
        return (a.second-a.first+1) < (b.second-b.first+1);
    };
    auto max_elem = std::max_element(ranges_to_store.begin(),ranges_to_store.end(),range_size_cmp);
    size_t max_range_size = max_elem->second - max_elem->first + 1;

    newt_log::info() << "[COMPUTE] postings lists from ranges in parallel" << std::endl;
    size_t num_threads = std::thread::hardware_concurrency()*2;
    uint64_t num_terms = D.size() - m_document_map.num_documents();
    t_rank ranker(num_terms,m_document_map);
    //size_t num_threads = 1;
    std::vector<std::future<std::vector<std::pair<tPII,plist_type>>>> v;
    size_t n = ranges_to_store.size();
    off_t block_size = n / num_threads + 1;
    for (size_t i=0; i<n; i+=block_size) {
        v.push_back(std::async(std::launch::async,[&,i] {
            size_t cnt = 0;
            std::vector<std::pair<tPII,plist_type>> lists;
            int_vector<64> tmp(max_range_size);
            for (size_t j=i; j<=std::min((size_type)n-1,(size_type)(i+block_size-1)); j++) {
                auto p = ranges_to_store[j];
                auto sp = p.first; auto ep = p.second;
                for (size_t itr=sp; itr<=ep; itr++) tmp[itr-sp] = D[itr];
                lists.emplace_back(p, plist_type(ranker,tmp,(ep-sp+1)));
                cnt += ep-sp+1;
            }
            return lists;
        }));
    }

    // combine results and add to map!
    for (auto& f : v) {
        auto computed_lists = f.get();
        for (const auto& list : computed_lists) {
            const auto& range = list.first;
            const auto& plist = list.second;
            bool output = false;
            if (range.first == 263172699 && range.second == 263178142) output = true;
            if (range.first == 280794539 && range.second == 280796090) output = true;
            if (output) {
                auto itr = plist.begin();
                auto end = plist.end();
                std::cout << "size = " << plist.size() << " [";
                while (itr != end) {
                    std::cout << "<" << itr.docid() << "," << itr.freq() << ">,";
                    ++itr;
                }
                std::cout << "]" << std::endl;
            }
            m_plist_store.emplace(list);
        }
    }
    auto precompute_end = clock::now();
    auto precompute_time_sec = std::chrono::duration_cast<std::chrono::seconds>(precompute_end-precompute_start);
    newt_log::info() << "\t- Precomputed all ranges. (" << precompute_time_sec.count() << " sec)" << std::endl;

    newt_log::info() << "[COMPUTE] position information for each postings list" << std::endl;
    int_vector_buffer<> sa(config.file_map[conf::KEY_SA].c_str(),std::ios_base::in);

    // compute the doc starts
    auto pos_start = clock::now();
    std::vector<uint64_t> doc_starts(m_document_map.num_documents()+1);
    doc_starts[0] = 0;
    for (size_t i=1; i<=doc_starts.size(); i++) {
        size_t suffix_pos = sa[i];
        doc_starts[D[i]+1] = suffix_pos+1;
    }
    for (const auto& r : ranges_to_store) {
        compute_positions(r,sa,D,doc_starts);
    }
    auto pos_stop = clock::now();
    auto pos_time_sec = std::chrono::duration_cast<std::chrono::seconds>(pos_stop-pos_start);
    newt_log::info() << "\t- Precomputed all positions. (" << pos_time_sec.count() << " sec)" << std::endl;
}

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size>
void inverted_index<t_csa,t_da,t_rank,t_plist_block_size>::compute_positions(const tPII& range,int_vector_buffer<>& sa,da_type& D,std::vector<uint64_t>& starts)
{
    static auto range_first = m_term_range_map[11054];
    static auto range_second = m_term_range_map[4864];
    bool debug = false;
    if (range_first == range || range_second == range) {
        debug = true;
    }

    if (debug) {
        std::cout << "compute pos for range <" << range.first << "," << range.second << ">" << std::endl;
    }
    std::vector<tPII> doc_pos;
    for (size_t i=range.first; i<=range.second; i++) {
        size_t doc = D[i];
        size_t suffix = sa[i];
        doc_pos.emplace_back(doc,suffix-starts[doc]);

        if (debug && doc == 61) {
            std::cout << "doc = " << doc << std::endl;
            std::cout << "starts[doc] = " << starts[doc] << std::endl;
            std::cout << "suffix = " << suffix << std::endl;
            std::cout << "indocoffset = " << suffix-starts[doc] << std::endl;
        }
    }
    auto doc_sorter = [](const tPII& a,const tPII& b) {
        if (a.first == b.first) {
            return a.second < b.second;
        }
        return a.first < b.first;
    };
    std::sort(doc_pos.begin(),doc_pos.end(),doc_sorter);

    // create position lists
    std::vector<std::vector<uint32_t>> position_lists;
    uint64_t last_doc = doc_pos[0].first;
    std::vector<uint32_t> positions;
    positions.push_back(doc_pos[0].second);
    for (size_t i=1; i<doc_pos.size(); i++) {
        if (doc_pos[i].first != last_doc) {
            position_lists.push_back(positions);
            positions.clear();
            last_doc = doc_pos[i].first;
        }
        positions.push_back(doc_pos[i].second);
    }
    position_lists.push_back(positions);

    // encode and add to index
    m_poslist_store.emplace(range,pos_list_type(position_lists));
}

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size>
void inverted_index<t_csa,t_da,t_rank,t_plist_block_size>::load(std::istream& in)
{
    // compute hash of the hash of the index and compare it to the one we are
    // trying to load
    std::hash<std::string> hash_fn;
    std::string type_str = type_info();
    size_t type_hash = hash_fn(type_str);
    size_t load_hash;
    read_member(load_hash,in);
    if (load_hash != type_hash) {
        newt_log::crit() << "Type of index stored in file is not equal to the type of index data structure" << std::endl;
        exit(EXIT_FAILURE);
    }

    read_member(m_N,in);

    newt_log::info() << "[LOAD] newt-index" << std::endl;
    m_document_map.load(in);
    m_term_map.load(in);
    auto ranges = load_ranges(in);
    load_plists(in,ranges);
}

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size>
auto inverted_index<t_csa,t_da,t_rank,t_plist_block_size>::serialize(std::ostream& out, structure_tree_node* v, std::string name) const -> size_type
{
    structure_tree_node* child = structure_tree::add_child(v, name, util::class_name(*this));
    size_type written_bytes = 0;

    // write a hash to identify the type of the index on load time
    std::hash<std::string> hash_fn;
    std::string type_str = type_info();
    size_t type_hash = hash_fn(type_str);
    written_bytes += write_member(type_hash,out,child,"type hash");

    written_bytes += write_member(m_N,out,child,"N");

    written_bytes += m_document_map.serialize(out, child, "document_map");
    written_bytes += m_term_map.serialize(out, child, "term_map");
    written_bytes += store_ranges(out, child, "stored ranges");
    written_bytes += store_plists(out, child, "postings lists");

    structure_tree::add_size(child, written_bytes);
    return written_bytes;
}

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size>
auto inverted_index<t_csa,t_da,t_rank,t_plist_block_size>::store_ranges(std::ostream& out, structure_tree_node* v, std::string name) const -> size_type
{
    structure_tree_node* child = structure_tree::add_child(v, name, util::class_name(*this));
    size_type written_bytes = 0;
    sdsl::int_vector<> sp(m_term_range_map.size());
    sdsl::int_vector<> ep(m_term_range_map.size());
    sdsl::int_vector<> tid(m_term_range_map.size());
    // extract sp,ep
    size_t i=0;
    for (const auto& r : m_term_range_map) {
        tid[i] = r.first;
        sp[i] = r.second.first;
        ep[i] = r.second.second;
        i++;
    }
    util::bit_compress(tid);
    util::bit_compress(sp);
    util::bit_compress(ep);
    written_bytes += tid.serialize(out,child,"tid");
    written_bytes += sp.serialize(out,child,"sp");
    written_bytes += ep.serialize(out,child,"ep");
    structure_tree::add_size(child, written_bytes);
    return written_bytes;
}

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size>
auto inverted_index<t_csa,t_da,t_rank,t_plist_block_size>::store_plists(std::ostream& out, structure_tree_node* v, std::string name) const -> size_type
{
    structure_tree_node* child = structure_tree::add_child(v, name, util::class_name(*this));
    size_type written_bytes = 0;
    // store lists
    for (const auto& r : m_term_range_map) {
        written_bytes += m_plist_store.find(r.second)->second.serialize(out,child,"plist data");
        written_bytes += m_poslist_store.find(r.second)->second.serialize(out,child,"position data");
    }
    structure_tree::add_size(child, written_bytes);
    return written_bytes;
}

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size>
std::vector<std::pair<size_t,size_t>>
                                   inverted_index<t_csa,t_da,t_rank,t_plist_block_size>::load_ranges(std::istream& in)
{
    std::vector<tPII> ranges;
    sdsl::int_vector<> tid;
    sdsl::int_vector<> sp;
    sdsl::int_vector<> ep;
    tid.load(in);
    sp.load(in);
    ep.load(in);
    for (size_t i=0; i<sp.size(); i++) {
        m_term_range_map.emplace(tid[i],make_pair(sp[i],ep[i]));
        ranges.emplace_back(sp[i],ep[i]);
    }
    return ranges;
}

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size>
void inverted_index<t_csa,t_da,t_rank,t_plist_block_size>::load_plists(std::istream& in,std::vector<std::pair<size_t,size_t>>& ranges)
{
    for (const auto& r : ranges) {
        m_plist_store.emplace(r,plist_type(in));
        m_poslist_store.emplace(r,pos_list_type(in));
    }
}

#endif
