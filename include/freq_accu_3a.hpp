/*
 *  Authors: Andrew Turpin, Simon Gog
 */

#ifndef FREQ_ACCU_3A_HPP
#define FREQ_ACCU_3A_HPP

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <limits>
#include <cassert>

//! An object of type freq_accu_3u maintains an ordered set of uint64_t ordered by frequency 
/*! \tparam t_key Type for the keys
 *  \tparam t_idx Type for the frequencies. Must be able to hold [0, n == number of processed keys]
 */
template<class t_key=uint64_t, class t_idx=uint64_t>
class freq_accu_3a
{

    public:
        typedef std::vector<t_key> t_key_vec;
        typedef std::vector<t_idx> t_idx_vec;
        using key_type = t_key;
        using idx_type = t_idx;

    private:
        t_key ABSENT = std::numeric_limits<t_key>::max();

        t_key_vec sorted;   // sorted-by-freq array of indexes to items (max freq is element 0)
        t_idx_vec freq;     // freq[i] contains frequency of key sorted[i]
        t_idx_vec boundary; // boundary[i] is index of leftmost element in sorted[] with frequency <= i
        t_idx_vec index;    // index[0..maxKey] contains location of key in `sorted`. ABSENT if not in sorted

        t_idx n = 0;        // number of elments in sorted[] and freq[]
        t_idx max_freq = 0; // maximum freq in freq[]   (which should be at position freq[0])

        //! Add non existing key to set
        /*! \param key  Key to add.
         *  \pre Key is not already in the set.
         */
        void add(t_key key) {
            // add item to end of sorted[] and record freqency of 1.
            if (n >= sorted.size())
                sorted.push_back(key);
            else
                sorted[n] = key;
            // update index to strore position in freq
            if (n >= freq.size())
                freq.push_back(1);
            else
                freq[n] = 1;
            n++;
            // update index to store position of key in sorted[]
            if (key >= index.size())
                index.insert(index.end(), key+1 - index.size(), ABSENT);
            index[key] = n - 1;
            // make sure boundary[0] is updated
            boundary[0] = n;
            if (max_freq < 1)
                max_freq = 1;
        }

    public:

        freq_accu_3a():boundary(t_idx_vec(2, 0)) { }  // boundary[1] must be 0

        //! Increment frequency of key.
        /*! \param key Key which frequency should be incremented or set to
         *             1, if it was not yet present in the set.
         */
        void inc(t_key key) {
            if (key >= index.size() or index[key] == ABSENT) {
                add(key);
                return;
            }
            if (freq[index[key]] == max_freq) { // make more room in boundary[]
                max_freq++;
                if (boundary.size() <= max_freq) {
                    boundary.push_back(0);
                } else {
                    boundary[max_freq]=0;
                }
            }
            // swap key to head (left) of its freq group (no need to change freq[])
            t_idx i = index[key];
            t_idx j = boundary[freq[i]];
            t_key   k = sorted[j];
            index[key] = j;
            index[k]   = i;
            sorted[i]  = k;
            sorted[j]  = key;

            // push boundary for key's freq to right, and inc freq
            boundary[freq[index[key]]]++;
            freq[index[key]]++;
        }

        //! Return the frequency of a key
        t_idx key_freq(t_key key) {
            if (key >= index.size() or ABSENT == index[key])
                return 0;
            else
                return freq[index[key]];
        }

        /*
        ** Returns key at rank 'rank' or ABSENT if rank higher than number of keys.
        ** Ranks start at 0.
        */
        t_key key_at_rank(t_idx rank) {
            if (rank < n)
                return sorted[rank];
            else
                return ABSENT;
        }

        //! Reset the accumulator to the inital state.
        void reset() {
            for (size_t i = 0 ; i < n ; i++)
                index[sorted[i]] = ABSENT;
            n = 0;
            max_freq = 0;
            boundary[0] = 0;
            boundary[1] = 0;
        }

        //! Return the number of keys in the set.
        t_key size() {
            return n;
        };

        //! Print arrays for debugging
        void dump() {
            using std::cout;
            using std::endl;

            printf("sorted[] = ");
            for (size_t i = 0 ; i < sorted.size() ; i++)
                cout << sorted[i]<< " ";
            cout << endl;

            printf("freq[] = ");
            for (size_t i = 0 ; i < freq.size() ; i++)
                cout << freq[i] << " ";
            cout << endl;

            printf("boundary[] = ");
            for (size_t i = 0 ; i < boundary.size() ; i++)
                cout << boundary[i] << " ";
            cout << endl;

            printf("index[] = ");
            for (size_t i = 0 ; i < index.size() ; i++)
                if (ABSENT != index[i])
                    cout << index[i] << ":" << i << " ";
            cout << endl;
        }

};


#endif
