#ifndef QUERY_STRAT_MAXSCORE_HPP
#define QUERY_STRAT_MAXSCORE_HPP

#include "settings.hpp"
#include "result.hpp"
#include "query.hpp"
#include "score_heap.hpp"
#include "query_profiler.hpp"

//#define DEBUG_MAXSCORE 1

class query_strategy_maxscore
{
    public:
        template<class t_pl,class rank_func>
        static ranked_result_list_t
        process(std::vector<plist_data<t_pl>*>& postings_lists,const rank_func& ranker,size_t k) {
            score_heap heap(k,0.0);

            double query_factor = 0;
            for (const auto& pl : postings_lists) {
                query_factor += pl->qt->term_weight;
            }

            // sort lists by score instead of id and create cum_sum
            //std::cout << "sort_lists_by_max_score" << std::endl;
            sort_lists_by_max_score(postings_lists,query_factor);
#ifdef DEBUG_MAXSCORE
            std::cout << "LISTS: ";
            for (const auto& pl : postings_lists) {
                std::cout << "[" << pl->data->list_max_score() << "]";
            }
            std::cout << std::endl;
#endif

            // split into non-essential and essential
            //std::cout << "find_essential_lists" << std::endl;
            auto essential_itr = find_essential_lists<t_pl>(postings_lists.begin(),postings_lists.end(),heap.threshold());
            auto end = postings_lists.end();
            while (essential_itr != end) {
                auto candidate_id = determine_smallest_id<t_pl>(essential_itr,end);
                //std::cout << "determine_smallest_id = " << candidate_id << std::endl;
                auto num_plists = postings_lists.size();
                if (evaluate_candidate(candidate_id,postings_lists,ranker,heap,query_factor)) {
                    // heap changed. determine new split
                    if (num_plists != postings_lists.size()) {
                        // one of the lists was removed. reset the iterators
                        end = postings_lists.end();
                        essential_itr = postings_lists.begin();
                    }
                    //std::cout << "find_essential_lists" << std::endl;
                    essential_itr = find_essential_lists<t_pl>(essential_itr,end,heap.threshold());
                }
            }
            // create the result from the heap and return
            return heap.elems();
        }
    public:
        template<class t_pl,class rank_func>
        static bool
        evaluate_candidate(uint64_t candidate_id,std::vector<plist_data<t_pl>*>& postings_lists,const rank_func& ranker,score_heap& heap,double query_factor) {
#ifdef NEWT_PROFILE_QUERY_PERFORMANCE
            query_profiler::record_evaluation();
#endif
#ifdef DEBUG_MAXSCORE
            std::cout << "evaluate_candidate = " << candidate_id << std::endl;
#endif
            // strategy: start from the back and partially evaluate till we know we can't get into the heap
            //std::cout << "evaluate_candidate = " << candidate_id << std::endl;
            double W_d = ranker.doc_length(candidate_id);
            double doc_score = query_factor * ranker.calc_doc_weight(W_d);
            auto itr = postings_lists.rbegin();
            auto end = postings_lists.rend();

#ifdef DEBUG_MAXSCORE
            std::cout << "WAND CUM SUMS: ";
            while (itr != end) {
                std::cout << "[" << (*itr)->wand_cum_score << "]";
                itr++;
            }
            std::cout << std::endl;
            itr = postings_lists.rbegin();
#endif

            bool list_finished = false;
            while (itr != end) {
#ifdef DEBUG_MAXSCORE
                std::cout << "doc_score = " << doc_score << std::endl;
                std::cout << "itr->wand_cum_score = " << (*itr)->wand_cum_score << std::endl;
#endif
                //std::cout << "doc_score = " << doc_score << std::endl;
                //std::cout << "itr->wand_cum_score = " << itr->wand_cum_score << std::endl;
                if (doc_score + (*itr)->wand_cum_score < heap.threshold()) {
#ifdef DEBUG_MAXSCORE
                    std::cout << "doc_score + (*itr)->wand_cum_score < heap.threshold() = "
                              <<  doc_score + (*itr)->wand_cum_score << " < "
                              << heap.threshold() << std::endl;
                    // we can't get into the heap. stop
                    if ((*itr)->cur.docid() == candidate_id) {
                        // add to score
                        double contrib = ranker.calculate_impact((*itr)->qt->f_qt,
                                         (*itr)->cur.freq(),(double)(*itr)->qt->f_t,
                                         (*itr)->qt->F_t,W_d,(*itr)->qt->term_weight);
                        std::cout << "contrib = " << contrib << std::endl;
                    }
#endif
                    break;
                } else {
                    // might go into the top-k. evaluate this list
                    if ((*itr)->cur.docid() < candidate_id) {
                        (*itr)->cur.skip_to_id(candidate_id);
                        // check if we are at the end?
                        if ((*itr)->cur == (*itr)->end) {
                            list_finished = true;
                            itr++;
                            continue;
                        }
                    }
                    if ((*itr)->cur.docid() == candidate_id) {
                        // add to score
                        double contrib = ranker.calculate_impact((*itr)->qt->f_qt,
                                         (*itr)->cur.freq(),(double)(*itr)->qt->f_t,
                                         (*itr)->qt->F_t,W_d,(*itr)->qt->term_weight);
                        doc_score += contrib;
                    }
                }
                itr++;
            }

            // forward lists!
            itr = postings_lists.rbegin();
            while (itr != end) {
                if ((*itr)->cur == (*itr)->end) {
                    ++itr;
                    continue;
                }
                if ((*itr)->cur.docid() == candidate_id) {
                    ++((*itr)->cur);
                } else {
                    if ((*itr)->cur.docid() < candidate_id) {
                        (*itr)->cur.skip_to_id(candidate_id+1);
                    }
                }
                if ((*itr)->cur == (*itr)->end) {
                    list_finished = true;
                }
                ++itr;
            }

#ifdef DEBUG_MAXSCORE
            std::cout << "final doc_score = " << doc_score << std::endl;
#endif
            //std::cout << "final doc_score = " << doc_score << std::endl;
            if (list_finished) {
#ifdef DEBUG_MAXSCORE
                std::cout << "LIST FINISHED!!" << std::endl;
#endif
                // if there is a finished list we clean things up and recompute the cum_score.
                auto del_itr = postings_lists.begin();

                while (del_itr != postings_lists.end()) {
                    if ((*del_itr)->cur == (*del_itr)->end) {
                        //std::cout << "delete list" << std::endl;
                        del_itr = postings_lists.erase(del_itr);
                    } else {
                        del_itr++;
                    }
                }
                update_list_cum_scores(postings_lists,query_factor);
            }
            double old_threshold = heap.threshold();
            heap.insert_if_higher_score( {candidate_id,doc_score});
            if (heap.threshold() != old_threshold) {
                return true;
            }
            return list_finished;
        }
        template<class t_pl>
        static uint64_t
        determine_smallest_id(typename std::vector<plist_data<t_pl>*>::iterator itr,const typename std::vector<plist_data<t_pl>*>::iterator& end) {
            uint64_t smallest_id = (*itr)->cur.docid();
            while (itr!= end) {
                smallest_id = std::min(smallest_id, (*itr)->cur.docid());
                ++itr;
            }
#ifdef DEBUG_MAXSCORE
            std::cout << "smallest_id = " << smallest_id << std::endl;
#endif
            return smallest_id;
        }
        template<class t_pl>
        static void
        sort_lists_by_max_score(std::vector<plist_data<t_pl>*>& postings_lists,double query_factor) {
            auto score_cmp = [](const plist_data<t_pl>* a,const plist_data<t_pl>* b) {
                return (a->data->list_max_score() * (a->qt->term_weight)) <
                       (b->data->list_max_score() * (b->qt->term_weight));
            };
            std::sort(postings_lists.begin(),postings_lists.end(),score_cmp);
            update_list_cum_scores(postings_lists,query_factor);
        }
        template<class t_pl>
        static void
        update_list_cum_scores(std::vector<plist_data<t_pl>*>& postings_lists,double query_factor) {
            double cum_sum = 0;
            double max_doc_score = std::numeric_limits<double>::lowest();
            for (size_t i=0; i<postings_lists.size(); i++) {
                cum_sum += (postings_lists[i]->data->list_max_score() * postings_lists[i]->qt->term_weight);
                max_doc_score = std::max(max_doc_score,postings_lists[i]->data->max_doc_weight());
                postings_lists[i]->wand_cum_score = cum_sum;
                postings_lists[i]->max_doc_score = max_doc_score * query_factor;
            }
        }
        template<class t_pl>
        static typename std::vector<plist_data<t_pl>*>::iterator
        find_essential_lists(typename std::vector<plist_data<t_pl>*>::iterator itr,
                             const typename std::vector<plist_data<t_pl>*>::iterator& end,double threshold) {
            while (itr!= end) {
                if (threshold < ((*itr)->wand_cum_score +(*itr)->max_doc_score)) break;
                ++itr;
            }
            return itr;
        }
};

#endif
