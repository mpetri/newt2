#ifndef PLIST_POSITION_SUPPORT_HPP
#define PLIST_POSITION_SUPPORT_HPP

#include "sdsl/int_vector.hpp"
#include "sdsl/sd_vector.hpp"
#include "sdsl/select_support.hpp"

class plist_position_support
{
    public:
        typedef sdsl::int_vector<>::size_type                                                                   size_type;
        typedef sdsl::sd_vector<sdsl::bit_vector,sdsl::select_support_mcl<1>,sdsl::select_support_scan<0>>      pos_data_type;
    public:
        pos_data_type                                                 m_list_starts;
        pos_data_type::select_1_type                           m_list_starts_select;
        pos_data_type                                                    m_pos_data;
        pos_data_type::select_1_type                              m_pos_data_select;
    public:
        plist_position_support() {}
        plist_position_support(const plist_position_support& ps) {
            std::cout << "copy constructor called.";
            m_list_starts = ps.m_list_starts;
            m_list_starts_select = ps.m_list_starts_select;
            m_list_starts_select.set_vector(&m_list_starts);
            m_pos_data = ps.m_pos_data;
            m_pos_data_select = ps.m_pos_data_select;
            m_pos_data_select.set_vector(&m_pos_data);
        }
        plist_position_support(plist_position_support&& ps) {
            m_list_starts = std::move(ps.m_list_starts);
            m_list_starts_select = std::move(ps.m_list_starts_select);
            m_list_starts_select.set_vector(&m_list_starts);
            m_pos_data = std::move(ps.m_pos_data);
            m_pos_data_select = std::move(ps.m_pos_data_select);
            m_pos_data_select.set_vector(&m_pos_data);
        }
        plist_position_support& operator=(const plist_position_support& ps) {
            std::cout << "copy assignment called.";
            m_list_starts = ps.m_list_starts;
            m_list_starts_select = ps.m_list_starts_select;
            m_list_starts_select.set_vector(&m_list_starts);
            m_pos_data = ps.m_pos_data;
            m_pos_data_select = ps.m_pos_data_select;
            m_pos_data_select.set_vector(&m_pos_data);
            return (*this);
        }
        plist_position_support& operator=(plist_position_support& ps) {
            std::cout << "move assignment called.";
            m_list_starts = std::move(ps.m_list_starts);
            m_list_starts_select = std::move(ps.m_list_starts_select);
            m_list_starts_select.set_vector(&m_list_starts);
            m_pos_data = std::move(ps.m_pos_data);
            m_pos_data_select = std::move(ps.m_pos_data_select);
            m_pos_data_select.set_vector(&m_pos_data);
            return (*this);
        }
        plist_position_support(std::istream& in) {
            load(in);
        }
        plist_position_support(std::vector<std::vector<uint32_t>>& position_lists) {
            size_t total_number_of_pos = 0;
            size_t prefix_sum = 0;
            for (size_t i=0; i<position_lists.size(); i++) {
                auto& positions = position_lists[i];
                std::for_each(positions.begin(), positions.end(), [](uint32_t& n) {
                    n++;
                });
                total_number_of_pos += positions.size();
                prefix_sum += positions[0];
                for (size_t j=1; j<positions.size(); j++) {
                    prefix_sum += positions[j] - positions[j-1];
                }
            }
            //std::cout << "prefix_sum = " << prefix_sum << std::endl;
            //std::cout << "total_number_of_pos = " << total_number_of_pos << std::endl;
            sdsl::bit_vector tmp_list_starts(total_number_of_pos+1);
            sdsl::bit_vector tmp_pos_data(prefix_sum+1);
            prefix_sum = 0;
            tmp_pos_data[prefix_sum] = 1;
            //std::cout << "tmp_pos_data[" << prefix_sum << "] = 1" << std::endl;
            size_t list_start_cum_sum = 0;
            for (size_t i=0; i<position_lists.size(); i++) {
                const auto& positions = position_lists[i];
                tmp_list_starts[list_start_cum_sum] = 1;
                //std::cout << "list_start[" << list_start_cum_sum << "] = 1" << std::endl;
                //std::cout << "positions.size() = " << positions.size() << std::endl;
                list_start_cum_sum += positions.size();

                prefix_sum += positions[0];
                tmp_pos_data[prefix_sum] = 1;
                //std::cout << "tmp_pos_data[" << prefix_sum+1 << "] = 1" << std::endl;
                for (size_t j=1; j<positions.size(); j++) {
                    prefix_sum += (positions[j] - positions[j-1]);
                    tmp_pos_data[prefix_sum] = 1;
                    //std::cout << "tmp_pos_data[" << prefix_sum << "] = 1" << std::endl;
                }
            }
            m_pos_data = pos_data_type(tmp_pos_data);
            m_pos_data_select = pos_data_type::select_1_type(&m_pos_data);
            tmp_list_starts[total_number_of_pos] = 1;
            m_list_starts = pos_data_type(tmp_list_starts);
            m_list_starts_select = pos_data_type::select_1_type(&m_list_starts);
        }
        size_t serialize(std::ostream& out, sdsl::structure_tree_node* v=nullptr, std::string name="") const {
            sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
            size_type written_bytes = 0;
            written_bytes += m_pos_data.serialize(out,child,"position data");
            written_bytes += m_pos_data_select.serialize(out,child,"position data select");
            written_bytes += m_list_starts.serialize(out,child,"list starts");
            written_bytes += m_list_starts_select.serialize(out,child,"list starts select");
            sdsl::structure_tree::add_size(child, written_bytes);
            return written_bytes;
        }
        void load(std::istream& in) {
            m_pos_data.load(in);
            m_pos_data_select.load(in,&m_pos_data);
            m_list_starts.load(in);
            m_list_starts_select.load(in,&m_list_starts);
        }
        size_t access_positions(std::vector<uint32_t>& buf,size_t list_nr) const {
            // std::cout << "m_list_starts.size() = " << m_list_starts.size() << std::endl;
            // pos_data_type::select_1_type sels(&m_list_starts);
            // std::cout << "sels.size() = " << sels.size() << std::endl;
            // std::cout << "m_list_starts_select.size() = " << m_list_starts_select.size() << std::endl;
            // pos_data_type::select_1_type selsp(&m_pos_data);
            // std::cout << "m_pos_data_select.size() = " << m_pos_data_select.size() << std::endl;
            // std::cout << "selsp.size() = " << selsp.size() << std::endl;
            // std::cout << "list_nr = " << list_nr << std::endl;
            size_t list_start = m_list_starts_select(list_nr+1);
            //std::cout << "list_start = " << list_start << std::endl;
            size_t list_stop = m_list_starts_select(list_nr+2);
            //std::cout << "list_stop = " << list_stop << std::endl;
            size_t num_elems = list_stop - list_start;
            //std::cout << "num_elems = " << num_elems << std::endl;

            if (buf.size() < num_elems) {
                buf.resize(num_elems);
            }

            //std::cout << "m_pos_data = " << m_pos_data << std::endl;
            //std::cout << "m_pos_data_select(list_start+2) = " << m_pos_data_select(list_start+2) << std::endl;
            //std::cout << "m_pos_data_select(list_start+1) = " << m_pos_data_select(list_start+1) << std::endl;
            size_t before = m_pos_data_select(list_start+1);
            buf[0] = m_pos_data_select(list_start+2) - before - 1;
            //std::cout << "buf[0] = " << buf[0] << std::endl;
            for (size_t i=1; i<num_elems; i++) {
                //std::cout << "m_pos_data_select(list_start+i+1) = " << m_pos_data_select(list_start+i+2) << std::endl;
                buf[i] = m_pos_data_select(list_start+i+2) - before - 1;
                //std::cout << "buf[" << i << "] = " << buf[i] << std::endl;
            }
            return num_elems;
        }
};

#endif
