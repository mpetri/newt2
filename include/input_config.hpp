#ifndef INPUT_CONFIG_HPP
#define INPUT_CONFIG_HPP

#include "jsonxx.h"
#include <sys/stat.h>

using namespace std;
using namespace jsonxx;

class input_config
{
    private:
        input_config() = delete;

    public:
        static sdsl::cache_config parse_input_cfg(const std::string& input_file) {
            sdsl::cache_config cfg;
            jsonxx::Object jsoncfg;
            std::ifstream ifs(input_file);
            if (ifs.is_open()) {
                if (! jsoncfg.parse(ifs)) {
                    newt_log::crit() << "Cannot read json input configuration: " << input_file  << std::endl;
                    exit(EXIT_FAILURE);
                } else {
                    // /* create cache config */
                    cfg.delete_files = false;
                    if (jsoncfg.has<String>("input_file")) {
                        std::string file = jsoncfg.get<String>("input_file");
                        ifstream in(file);
                        if (!in) {
                            newt_log::crit() << "Input file not accessible : " << file  << std::endl;
                            exit(EXIT_FAILURE);
                        }
                        cfg.file_map[newt_constants::KEY_INPUT] = file;
                        newt_log::info() << "[CFG] INPUT = " << file << std::endl;
                    } else {
                        newt_log::err() << "Cannot find input config property: " << "input_file"  << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    if (jsoncfg.has<String>("termmap_file")) {
                        std::string file = jsoncfg.get<String>("termmap_file");
                        ifstream in(file);
                        if (!in) {
                            newt_log::crit() << "Term map file not accessible : " << file  << std::endl;
                            exit(EXIT_FAILURE);
                        }
                        cfg.file_map[newt_constants::KEY_INPUTTERMMAP] = file;
                        newt_log::info() << "[CFG] TERMMAP = " << file << std::endl;
                    } else {
                        newt_log::err() << "Cannot find input config property: " << "termmap_file"  << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    if (jsoncfg.has<String>("docmap_file")) {
                        std::string file = jsoncfg.get<String>("docmap_file");
                        ifstream in(file);
                        if (!in) {
                            newt_log::crit() << "Docmap file not accessible : " << file  << std::endl;
                            exit(EXIT_FAILURE);
                        }
                        cfg.file_map[newt_constants::KEY_INPUTDOCMAP] = file;
                        newt_log::info() << "[CFG] DOCMAP = " << file << std::endl;
                    } else {
                        newt_log::err() << "Cannot find input config property: " << "docmap_file"  << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    if (jsoncfg.has<String>("uid")) {
                        cfg.id = jsoncfg.get<String>("uid");
                        newt_log::info() << "[CFG] UID = " << cfg.id << std::endl;
                    } else {
                        newt_log::err() << "Cannot find input config property: " << "uid"  << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    if (jsoncfg.has<String>("temp_dir")) {
                        struct stat sb;
                        const char* pathname = jsoncfg.get<String>("temp_dir").c_str();
                        if (stat(pathname, &sb) == 0 && S_ISDIR(sb.st_mode)) {
                            cfg.dir = jsoncfg.get<String>("temp_dir");
                            newt_log::info() << "[CFG] TEMPDIR = " << pathname << std::endl;
                        } else {
                            newt_log::crit() << "Specified temp directory is not valid : " << jsoncfg.get<String>("temp_dir")  << std::endl;
                            exit(EXIT_FAILURE);
                        }
                    } else {
                        newt_log::err() << "Cannot find input config property: " << "temp_dir"  << std::endl;
                        exit(EXIT_FAILURE);
                    }
                }
            } else {
                newt_log::crit() << "Cannot parse json input configuration :" << input_file  << std::endl;
                exit(EXIT_FAILURE);
            }
            return cfg;
        }
};

#endif
