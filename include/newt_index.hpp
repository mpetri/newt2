#ifndef NEWT_INDEX_HPP
#define NEWT_INDEX_HPP

#include "sdsl/suffix_arrays.hpp"

#include "pruned_suffix_tree.hpp"
#include "postings_list.hpp"
#include "document_arrays.hpp"
#include "settings.hpp"
#include "logger.hpp"
#include "document_map.hpp"
#include "term_map.hpp"
#include "rank_functions.hpp"

using namespace sdsl;

/*!
 * \tparam t_csa  Compressed suffix array
 * \tparam t_da   Document array
 * \tparam t_pl   Posting list
 * \poaram t_rank Ranking function
 */
template<class t_csa,
         class t_da,
         class t_rank,
         uint64_t t_plist_block_size,
         uint64_t t_pst_min_stored
         >
class newt_index
{
    public:
        // types
        typedef std::pair<size_t, size_t>                                                 tPII;
        typedef t_rank                                                                    rank_type;
        typedef t_csa												                      csa_type;
        typedef postings_list<t_plist_block_size>                                         plist_type;
        typedef typename t_da::template type<t_csa>				      	                  da_type;
        typedef typename csa_type::size_type						                      size_type;
        typedef typename std::unordered_map<tPII , plist_type,pair_hash>::const_iterator  const_iterator;
    private:
        da_type			 			                                                      m_document_array;
        csa_type 	 				                                                      m_csa;
        std::unordered_map<tPII,plist_type,pair_hash>                                     m_plist_store;
        std::vector<tPII>                                                                 m_stored_ranges;
        document_map		                                                              m_document_map;
        term_map					                                                      m_term_map;
    private:
        mutable std::unordered_map<tPII , plist_type,pair_hash>                           m_onthefly_lists;
    private:
        void compute_plists_from_ranges(cache_config& config,std::vector<tPII>& ranges_to_store);
        auto store_plists(std::ostream& out, structure_tree_node* v=NULL, std::string name="") const -> size_type;
        void load_plists(std::istream& in);
        auto store_ranges(std::ostream& out, structure_tree_node* v=NULL, std::string name="") const -> size_type;
        void load_ranges(std::istream& in);
    public:
        auto serialize(std::ostream& out, structure_tree_node* v=NULL, std::string name="") const -> size_type;
        newt_index(cache_config& config);
        newt_index() = default;
        void load(std::istream& in);
        std::string type_info() const;
        std::vector<typename t_csa::value_type> extract_document(size_type id) const;
        const plist_type& lookup_or_create(tPII range) const {
            /*  check permanent store */
            auto list_itr=m_plist_store.find(range);
            if (list_itr != m_plist_store.end()) {
                return list_itr->second;
            }
            /* check on the fly lists too... */
            auto olist_itr=m_onthefly_lists.find(range);
            if (olist_itr != m_onthefly_lists.end()) {
                return olist_itr->second;
            }
            /* create list on the fly */
            static uint64_t num_terms = m_document_array.size() - m_document_map.num_documents();
            static t_rank ranker(num_terms,m_document_map);
            static int_vector<64> tmp(m_document_map.num_documents());
            if (tmp.size() < range.second-range.first+1) {
                tmp.resize(range.second-range.first+1);
            }
            for (size_t itr=range.first; itr<=range.second; itr++) tmp[itr-range.first] = m_document_array[itr];
            m_onthefly_lists.emplace(range,plist_type(ranker,tmp,(range.second-range.first+1)));
            auto otflist_itr=m_onthefly_lists.find(range);
            return otflist_itr->second;
        }
        void clear_onthefly() const {
            m_onthefly_lists.clear();
        }
        const_iterator plist_begin() const {
            return m_plist_store.begin();
        };
        const_iterator plist_end() const {
            return m_plist_store.end();
        };
    public:
        const term_map&                                                             termmap = m_term_map;
        const document_map&                                                         docmap = m_document_map;
        const csa_type&                                                             csa = m_csa;
        const da_type&                                                              document_array = m_document_array;
};

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size, uint64_t t_pst_min_stored>
std::string newt_index<t_csa,t_da,t_rank,t_plist_block_size,t_pst_min_stored>::type_info() const
{
    std::stringstream type_name;
    type_name << "newt-index-"
              << typeid(t_csa).name() << "-"
              << typeid(t_da).name() << "-"
              << typeid(t_rank).name() << "-"
              << t_plist_block_size << "-"
              << t_pst_min_stored;
    return type_name.str();
}

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size, uint64_t t_pst_min_stored>
newt_index<t_csa,t_da,t_rank,t_plist_block_size,t_pst_min_stored>::newt_index(cache_config& config)
{
    // make sure we don't delete stuff
    config.delete_files = false;

    // (1) check, if the compressed suffix array exists
    //     ->  also creates the bwt and the suffix array
    if (!cache_file_exists(newt_constants::KEY_CSA+"_"+util::class_to_hash(m_csa), config)) {
        newt_log::info() << "[CONSTRUCT] compressed suffix array" << std::endl;
        construct(m_csa, config.file_map[newt_constants::KEY_INPUT], config, 0);
        store_to_cache(m_csa,newt_constants::KEY_CSA+"_"+util::class_to_hash(m_csa), config);
        newt_log::info() << "[SIZE] compressed suffix array " << size_in_mega_bytes(m_csa) << " MB" << std::endl;
    } else {
        newt_log::info() << "[SKIP] compressed suffix array construction" << std::endl;
        register_cache_file(conf::KEY_SA, config);
    }
    register_cache_file(newt_constants::KEY_CSA+"_"+util::class_to_hash(m_csa), config);
    // for now we don't need the csa so we reload it later
    util::clear(m_csa);

    // (2) create the D array
    //     -> uses the suffix array created in the csa step
    if (!cache_file_exists(newt_constants::KEY_DARRAY+"_"+util::class_to_hash(m_document_array), config)) {
        newt_log::info() << "[CONSTRUCT] document array" << std::endl;
        da_type tmp_darray(config);
        store_to_cache(tmp_darray, newt_constants::KEY_DARRAY+"_"+util::class_to_hash(tmp_darray), config);
        newt_log::info() << "[SIZE] document array " << size_in_mega_bytes(tmp_darray) << " MB" << std::endl;
    } else {
        newt_log::info() << "[SKIP] document array construction" << std::endl;
    }
    register_cache_file(newt_constants::KEY_DARRAY+"_"+util::class_to_hash(m_document_array), config);


    // (3) create the document map
    //     -> uses the document array
    if (!cache_file_exists(newt_constants::KEY_DOCMAP, config)) {
        newt_log::info() << "[CONSTRUCT] document map" << std::endl;
        m_document_map = document_map(config);
        store_to_cache(m_document_map,newt_constants::KEY_DOCMAP, config);
        newt_log::info() << "[SIZE] document map " << size_in_mega_bytes(m_document_map) << " MB" << std::endl;
    } else {
        load_from_cache(m_document_map,newt_constants::KEY_DOCMAP, config);
        newt_log::info() << "[LOAD] document map" << std::endl;
    }
    register_cache_file(newt_constants::KEY_DOCMAP, config);


    // (4) create the term map
    //
    if (!cache_file_exists(newt_constants::KEY_TERMMAP, config)) {
        newt_log::info() << "[CONSTRUCT] term map" << std::endl;
        term_map tmp_term_map(config);
        store_to_cache(tmp_term_map, newt_constants::KEY_TERMMAP, config);
        newt_log::info() << "[SIZE] term map " << size_in_mega_bytes(tmp_term_map) << " MB" << std::endl;
    } else {
        newt_log::info() << "[SKIP] term map construction" << std::endl;
    }
    register_cache_file(newt_constants::KEY_TERMMAP, config);


    // (5) compute the ranges for which we will store stuff
    if (!cache_file_exists(std::string(newt_constants::KEY_RANGES)+"_"+std::to_string(t_pst_min_stored), config)) {
        newt_log::info() << "[CONSTRUCT] pruned suffix tree ranges" << std::endl;
        m_stored_ranges = pruned_suffix_tree<t_csa>::determine_ranges(config,t_pst_min_stored);
        std::ofstream rofs(cache_file_name(std::string(newt_constants::KEY_RANGES)+"_"+std::to_string(t_pst_min_stored),config));
        store_ranges(rofs);
    } else {
        // we computed the ranges before. load them instead!
        newt_log::info() << "[LOAD] pruned suffix tree ranges" << std::endl;
        std::ifstream rifs(cache_file_name(std::string(newt_constants::KEY_RANGES)+"_"+std::to_string(t_pst_min_stored),config));
        load_ranges(rifs);
    }

    // (6) compute the things we store
    uint64_t num_terms = m_document_map.num_documents();
    t_rank ranker(num_terms,m_document_map); // only temp to get the type!
    if (!cache_file_exists(std::string(newt_constants::KEY_PLISTS)+"_"+
                           std::to_string(t_pst_min_stored)+"_"+
                           std::to_string(t_plist_block_size)+"_"
                           +util::class_to_hash(ranker), config)) {
        newt_log::info() << "[COMPUTE] postings lists from ranges" << std::endl;
        compute_plists_from_ranges(config,m_stored_ranges);
        std::ofstream rofs(cache_file_name(std::string(newt_constants::KEY_PLISTS)+"_"+
                                           std::to_string(t_pst_min_stored)+"_"+
                                           std::to_string(t_plist_block_size)+"_"
                                           +util::class_to_hash(ranker),config));
        store_plists(rofs);
    } else {
        newt_log::info() << "[LOAD] postings lists" << std::endl;
        std::ifstream rifs(cache_file_name(std::string(newt_constants::KEY_PLISTS)+"_"+
                                           std::to_string(t_pst_min_stored)+"_"+
                                           std::to_string(t_plist_block_size)+"_"
                                           +util::class_to_hash(ranker),config));
        load_plists(rifs);
        newt_log::info() << "[LOAD] document array" << std::endl;
        load_from_cache(m_document_array, newt_constants::KEY_DARRAY+"_"+util::class_to_hash(m_document_array), config);
    }

    // (7) load the remaining data structures back into memory
    newt_log::info() << "[LOAD] compressed suffix array" << std::endl;
    load_from_cache(m_csa,newt_constants::KEY_CSA+"_"+util::class_to_hash(m_csa), config);
    newt_log::info() << "[LOAD] term map" << std::endl;
    load_from_cache(m_term_map, newt_constants::KEY_TERMMAP, config);
    newt_log::info() << "[TOTAL INDEX SIZE] " << size_in_mega_bytes(*this) << " MB" << std::endl;
}


template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size, uint64_t t_pst_min_stored>
void newt_index<t_csa,t_da,t_rank,t_plist_block_size,t_pst_min_stored>::compute_plists_from_ranges(cache_config& config,std::vector<tPII>& ranges_to_store)
{
    using clock = std::chrono::high_resolution_clock;
    auto precompute_start = clock::now();
    newt_log::info() << "[LOAD] document array" << std::endl;
    load_from_cache(m_document_array, newt_constants::KEY_DARRAY+"_"+util::class_to_hash(m_document_array), config);

    // find max range for tmp storage reqs
    auto range_size_cmp = [](const tPII& a,const tPII& b) {
        return (a.second-a.first+1) < (b.second-b.first+1);
    };
    auto max_elem = std::max_element(ranges_to_store.begin(),ranges_to_store.end(),range_size_cmp);
    size_t max_range_size = max_elem->second - max_elem->first + 1;

    newt_log::info() << "[COMPUTE] postings lists from ranges in parallel" << std::endl;
    size_t num_threads = std::thread::hardware_concurrency()*2;
    uint64_t num_terms = m_document_array.size() - m_document_map.num_documents();
    t_rank ranker(num_terms,m_document_map);
    //size_t num_threads = 1;
    std::vector<std::future<std::vector<std::pair<tPII,plist_type>>>> v;
    size_t n = ranges_to_store.size();
    off_t block_size = n / num_threads + 1;
    for (size_t i=0; i<n; i+=block_size) {
        v.push_back(std::async(std::launch::async,[&,i] {
            size_t cnt = 0;
            std::vector<std::pair<tPII,plist_type>> lists;
            int_vector<64> tmp(max_range_size);
            for (size_t j=i; j<=std::min((size_type)n-1,(size_type)(i+block_size-1)); j++) {
                auto p = ranges_to_store[j];
                auto sp = p.first; auto ep = p.second;
                for (size_t itr=sp; itr<=ep; itr++) tmp[itr-sp] = m_document_array[itr];
                lists.emplace_back(p, plist_type(ranker,tmp,(ep-sp+1)));
                cnt += ep-sp+1;
            }
            return lists;
        }));
    }

    // combine results and add to map!
    for (auto& f : v) {
        auto computed_lists = f.get();
        for (const auto& list : computed_lists) {
            m_plist_store.emplace(list);
        }
    }
    auto precompute_end = clock::now();
    auto precompute_time_sec = std::chrono::duration_cast<std::chrono::seconds>(precompute_end-precompute_start);
    newt_log::info() << "\t- Precomputed all ranges. (" << precompute_time_sec.count() << " sec)" << std::endl;
}


template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size, uint64_t t_pst_min_stored>
void newt_index<t_csa,t_da,t_rank,t_plist_block_size,t_pst_min_stored>::load(std::istream& in)
{
    // compute hash of the hash of the index and compare it to the one we are
    // trying to load
    std::hash<std::string> hash_fn;
    std::string type_str = type_info();
    size_t type_hash = hash_fn(type_str);

    size_t load_hash;
    read_member(load_hash,in);
    if (load_hash != type_hash) {
        newt_log::crit() << "Type of index stored in file is not equal to the type of index data structure" << std::endl;
        exit(EXIT_FAILURE);
    }

    newt_log::info() << "[LOAD] newt-index" << std::endl;
    m_csa.load(in);
    m_document_array.load(in);
    m_document_array.set_csa(&m_csa);
    m_document_map.load(in);
    m_term_map.load(in);
    load_ranges(in);
    load_plists(in);
}

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size, uint64_t t_pst_min_stored>
auto newt_index<t_csa,t_da,t_rank,t_plist_block_size,t_pst_min_stored>::serialize(std::ostream& out, structure_tree_node* v, std::string name) const -> size_type
{
    structure_tree_node* child = structure_tree::add_child(v, name, util::class_name(*this));
    size_type written_bytes = 0;

    // write a hash to identify the type of the index on load time
    std::hash<std::string> hash_fn;
    std::string type_str = type_info();
    size_t type_hash = hash_fn(type_str);
    written_bytes += write_member(type_hash,out,child,"type hash");

    written_bytes += m_csa.serialize(out, child, "compressed_suffix_array");
    written_bytes += m_document_array.serialize(out, child, "document_array");
    written_bytes += m_document_map.serialize(out, child, "document_map");
    written_bytes += m_term_map.serialize(out, child, "term_map");
    written_bytes += store_ranges(out, child, "stored ranges");
    written_bytes += store_plists(out, child, "postings lists");

    structure_tree::add_size(child, written_bytes);
    return written_bytes;
}

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size, uint64_t t_pst_min_stored>
auto newt_index<t_csa,t_da,t_rank,t_plist_block_size,t_pst_min_stored>::store_ranges(std::ostream& out, structure_tree_node* v, std::string name) const -> size_type
{
    structure_tree_node* child = structure_tree::add_child(v, name, util::class_name(*this));
    size_type written_bytes = 0;
    sdsl::int_vector<> sp(m_stored_ranges.size());
    sdsl::int_vector<> ep(m_stored_ranges.size());
    // extract sp,ep
    size_t i=0;
    for (const auto& r : m_stored_ranges) {
        sp[i] = r.first;
        ep[i] = r.second;
        i++;
    }
    util::bit_compress(sp);
    util::bit_compress(ep);
    written_bytes += sp.serialize(out,child,"sp");
    written_bytes += ep.serialize(out,child,"ep");
    structure_tree::add_size(child, written_bytes);
    return written_bytes;
}

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size, uint64_t t_pst_min_stored>
auto newt_index<t_csa,t_da,t_rank,t_plist_block_size,t_pst_min_stored>::store_plists(std::ostream& out, structure_tree_node* v, std::string name) const -> size_type
{
    structure_tree_node* child = structure_tree::add_child(v, name, util::class_name(*this));
    size_type written_bytes = 0;
    // store lists
    for (const auto& r : m_stored_ranges) {
        written_bytes += m_plist_store.find(r)->second.serialize(out,child,"plist data");
    }
    structure_tree::add_size(child, written_bytes);
    return written_bytes;
}

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size, uint64_t t_pst_min_stored>
void newt_index<t_csa,t_da,t_rank,t_plist_block_size,t_pst_min_stored>::load_ranges(std::istream& in)
{
    sdsl::int_vector<> sp;
    sdsl::int_vector<> ep;
    sp.load(in);
    ep.load(in);
    for (size_t i=0; i<sp.size(); i++) {
        m_stored_ranges.emplace_back(sp[i],ep[i]);
    }
}

template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size, uint64_t t_pst_min_stored>
void newt_index<t_csa,t_da,t_rank,t_plist_block_size,t_pst_min_stored>::load_plists(std::istream& in)
{
    for (size_t i=0; i<m_stored_ranges.size(); i++) {
        m_plist_store.emplace(m_stored_ranges[i],plist_type(in));
    }
}


template<class t_csa, class t_da, class t_rank, uint64_t t_plist_block_size, uint64_t t_pst_min_stored>
std::vector<typename t_csa::value_type>
newt_index<t_csa,t_da,t_rank,t_plist_block_size,t_pst_min_stored>::extract_document(size_type id) const
{
    size_t start_row = m_document_array.doc_start_decode(id);
    size_t size = m_document_map.length(id);

    std::vector<typename t_csa::value_type> doc(size);
    size_t j = m_csa.lf[start_row];
    for (size_t i=0; i<size; i++) {
        doc[size-i-1] = m_csa.F[j];
        j = m_csa.lf[j];
    }

    return doc;
}

// types
using t_newtindex = newt_index<CSATYPE, DATYPE, RANKTYPE, PLIST_BLOCK_SIZE, PSTMINSTORED>;


#endif
