
#ifndef RESULT_HPP
#define RESULT_HPP

#include <chrono>
#include <vector>
#include "query.hpp"
#include "sdsl/vectors.hpp"

//                         <doc_id, score>
using doc_res_t = std::pair<uint64_t,double>;
using ranked_result_list_t = std::vector<doc_res_t>;

struct result {
    query_strat					query_strategy;
    query                       qry;
    ranked_result_list_t        list;
    // times for benchmarking
    std::chrono::milliseconds   total_time;
    std::chrono::milliseconds   csa_time;
    std::chrono::milliseconds   candidate_time;
    std::chrono::milliseconds   process_time;
};

#endif
