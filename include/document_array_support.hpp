#ifndef DOCUMENT_ARRAY_SUPPORT_HPP
#define DOCUMENT_ARRAY_SUPPORT_HPP

#include <sdsl/io.hpp>
#include <sdsl/suffix_trees.hpp>

#include <utility> // for std::pair
#include <map>     // for std::map

#include "settings.hpp"
#include "logger.hpp"

using namespace sdsl;

template<class t_csa,
         class t_bitvector,
         class t_rank
         >
class _document_array_support
{
    public:
        static const bool requires_csa = true;
        typedef uint64_t                value_type;
        typedef int_vector<>::size_type size_type;
    private:
        t_bitvector  m_doc_bounds;
        t_rank       m_doc_rank;
        size_type    m_num_documents = 1;
        t_csa*       mp_csa = nullptr;
    public:
        const size_type&    num_documents = m_num_documents;

        _document_array_support() = default;//() {};

        _document_array_support(_document_array_support&& other) :
            m_doc_bounds(std::move(other.m_doc_bounds)),
            m_doc_rank(std::move(other.m_doc_rank)),
            m_num_documents(other.m_num_documents) {
            m_doc_rank.set_vector(&m_doc_bounds);
        }

        _document_array_support& operator=(_document_array_support&& other) {
            m_doc_bounds = std::move(other.m_doc_bounds);
            m_doc_rank = std::move(other.m_doc_rank);
            m_doc_rank.set_vector(&m_doc_bounds);
            return *this;
        }

        void set_csa(t_csa* p_csa) {
            mp_csa = p_csa;
        }

        size_t size() const {
            return m_doc_bounds.size();
        };

        //! Constructor
        _document_array_support(cache_config& config) {
            // iterate over text and determine document bounds
            newt_log::info() << "\t- Marking document boundaries (" << newt_constants::document_delimiter << ")" << std::endl;
            int_vector_buffer<> text_buf(config.file_map[newt_constants::KEY_INPUT].c_str(),std::ios_base::in);
            size_type n = text_buf.size();
            bit_vector doc_bounds(n+1,0); // to account for the appended \0
            for (size_type i=0; i < n;i++) {
                if (text_buf[i] == newt_constants::document_delimiter) {
                    m_num_documents++;
                    doc_bounds[i] = 1;
                }
            }
            m_doc_bounds = t_bitvector(doc_bounds);
            newt_log::info() << "\t- Found " << m_num_documents << " documents (" << bits::hi(m_num_documents)+1 << " bps)" << std::endl;

            // create the document array by iterating over the suffix array and
            // performing a rank for each suffix position
            m_doc_rank = t_rank(&m_doc_bounds);

        }

        void load(std::istream& in) {
            m_doc_bounds.load(in);
            m_doc_rank.load(in);
            m_doc_rank.set_vector(&m_doc_bounds);
            read_member(m_num_documents,in);
        }

        size_t serialize(std::ostream& out, structure_tree_node* v=nullptr, std::string name="") const {
            structure_tree_node* child = structure_tree::add_child(v, name, util::class_name(*this));
            size_t written_bytes = 0;
            written_bytes += m_doc_bounds.serialize(out,child,"doc_bounds");
            written_bytes += m_doc_rank.serialize(out,child,"doc_rank");
            written_bytes += write_member(m_num_documents,out,child,"num_documents");
            structure_tree::add_size(child, written_bytes);
            return written_bytes;
        }

        inline value_type operator[](size_type i) const {
            assert(mp_csa != nullptr);
            size_type suffix_pos = (*mp_csa)[i];
            return m_doc_rank.rank(suffix_pos);
        }

};

template<class t_bitvector = sd_vector<>,
         class t_rank  = typename t_bitvector::rank_1_type
         >
struct document_array_support
{
    template<class t_csa>
    using type = _document_array_support<t_csa, t_bitvector, t_rank>;
};

#endif
