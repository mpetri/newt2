
#ifndef OUTPUT_FORMATS_HPP
#define OUTPUT_FORMATS_HPP

#include "result.hpp"

template<class t_index,uint64_t t_kmax = 1000>
class output_trec
{
    public:
        std::vector<result>   results;
        void output(std::ostream& of,const t_index& index,bool write_header = true) const {
            std::string index_name = IDXNAME;
            for (size_t i=0; i<results.size(); i++) {
                const auto& res = results[i];
                uint64_t output_k = res.list.size();
                if (output_k > t_kmax) output_k = t_kmax;
                for (size_t j=0; j<output_k; j++) {
                    of << res.qry.number                << "\t"
                       << "Q0"                          << "\t"
                       << index.docmap.name(std::get<0>(res.list[j])) << "\t"
                       << j                                           << "\t"
                       << std::get<1>(res.list[j])                    << "\t"
                       << index_name                          << std::endl;
                }
            }
        };
};

template<class t_index,uint64_t t_kmax = 1000>
class output_trec_id
{
    public:
        std::vector<result>   results;
        void output(std::ostream& of,const t_index& index,bool write_header = true) const {
            std::string index_name = IDXNAME;
            for (size_t i=0; i<results.size(); i++) {
                const auto& res = results[i];
                uint64_t output_k = res.list.size();
                if (output_k > t_kmax) output_k = t_kmax;
                for (size_t j=0; j<output_k; j++) {
                    of << res.qry.number                << "\t"
                       << "Q0"                          << "\t"
                       << std::get<0>(res.list[j])      << "\t"
                       << j                                           << "\t"
                       << std::get<1>(res.list[j])                    << "\t"
                       << index_name                          << std::endl;
                }
            }
        };
};

template<class t_index>
class output_benchmark
{
    public:
        std::vector<result>   results;
        void output(std::ostream& of,const t_index& index,bool write_header = true) const {
            std::string index_name = IDXNAME;
            // write header
            if (write_header) of << "qrynum;index_type;parsed_terms;qry_terms;num_docs;num_results;total_time;csa_time;candidate_time;safek_time;query_strategy;expansion" << std::endl;
            for (size_t i=0; i<results.size(); i++) {
                const auto& res = results[i];
                of << res.qry.number                << ";"
                   << index_name                    << ";"
                   << res.qry.parsed_terms          << ";"
                   << res.qry.terms.size()          << ";"
                   << index.docmap.num_documents()  << ";"
                   << res.list.size()               << ";"
                   << res.total_time.count()        << ";"
                   << res.csa_time.count()          << ";"
                   << res.candidate_time.count()    << ";"
                   << res.process_time.count()      << ";"
                   << sg_query_strat_names[res.query_strategy] << ";"
                   << res.qry.expanded              << std::endl;
            }
        };
};

template<class t_index>
class output_compare
{
    public:
        std::vector<result>   results;
        virtual void output(std::ostream& of,const t_index& index,bool write_header = true) const {
            for (size_t i=0; i<results.size(); i++) {
                const auto& res = results[i];
                for (size_t j=0; j<res.list.size(); j++) {
                    of << res.qry.number                << ";"
                       << index.docmap.name(std::get<0>(res.list[j])) << ";"
                       << j   << std::endl;
                }
            }
        };
};

#endif
