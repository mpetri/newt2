#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include "sdsl/suffix_arrays.hpp"
#include "sdsl/csa_alphabet_strategy.hpp"

using namespace sdsl;

namespace newt_constants
{
const char query_num_seperator		   = ';';
const char query_term_seperator        = ' ';
const std::string KEY_INPUT            = "input";
const std::string KEY_CSA              = "csa";
const std::string KEY_CST              = "cst";
const std::string KEY_RANGES           = "ranges";
const std::string KEY_PLISTS           = "plists";
const std::string KEY_DARRAY           = "darray";
const std::string KEY_DOCMAP           = "docmap";
const std::string KEY_TERMMAP          = "termmap";
const std::string KEY_INPUTDOCMAP      = "indocmap";
const std::string KEY_INPUTTERMMAP     = "intermmap";
const uint64_t document_delimiter = 1;
const uint32_t sa_sample_rate     = 8;
const uint32_t isa_sample_rate    = 64;
const uint64_t pst_phrase_len_before_pruning = 2;
const uint64_t no_expansion_single_weight = 100;
const uint64_t expansion_single_weight = 88;
const uint64_t expansion_phrase_weight = 12;
}

enum query_strat {
    QUERY_TAAT = 0,
    QUERY_WAND = 1,
    QUERY_MAXSCORE = 2,
    QUERY_BMW = 3,
    QUERY_BMM = 4,
    QUERY_DAAT = 5,
    QUERY_UNKNOWN = 6
};

const char* sg_query_strat_names[]= { "TAAT","WAND","MAXSCORE","BMW","BMM", "DAAT", "UNKNOWN" };


class pair_hash
{
    public:
        std::size_t operator()(const std::pair<size_t, size_t>& v) const {
            // simple FNV-1a
            std::size_t hash = 14695981039346656037ULL;
            hash = hash ^ std::hash<size_t>()(v.first);
            hash = hash * 1099511628211ULL;
            hash = hash ^ std::hash<size_t>()(v.second);
            hash = hash * 1099511628211ULL;
            return hash;
        }
};


#endif
