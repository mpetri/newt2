#ifndef QUERY_STRAT_BMM_HPP
#define QUERY_STRAT_BMM_HPP

#include "settings.hpp"
#include "result.hpp"
#include "query.hpp"
#include "score_heap.hpp"
#include "query_profiler.hpp"

class query_strategy_bmm
{
    public:
        template<class t_pl,class rank_func>
        static ranked_result_list_t
        process(std::vector<plist_data<t_pl>*>& postings_lists,const rank_func& ranker,size_t k) {
            score_heap heap(k,0.0);

            size_t initial_num_plists = postings_lists.size();

            // sort lists by score instead of id and create cum_sum
            //std::cout << "sort_lists_by_max_score" << std::endl;
            sort_lists_by_max_score(postings_lists);

            // split into non-essential and essential
            //std::cout << "find_essential_lists" << std::endl;
            auto essential_itr = find_essential_lists<t_pl>(postings_lists.begin(),postings_lists.end(),heap.threshold());

            auto end = postings_lists.end();
            while (essential_itr != end) {
                auto candidate_itr = determine_smallest_id<t_pl>(essential_itr,end);
                //std::cout << "determine_smallest_id = " << candidate_id << std::endl;
                auto num_plists = postings_lists.size();
                if (evaluate_candidate(candidate_itr,postings_lists,ranker,heap,initial_num_plists)) {
                    // heap changed. determine new split
                    if (num_plists != postings_lists.size()) {
                        // one of the lists was removed. reset the iterators
                        end = postings_lists.end();
                        essential_itr = postings_lists.begin();
                    }
                    //std::cout << "find_essential_lists" << std::endl;
                    essential_itr = find_essential_lists<t_pl>(essential_itr,end,heap.threshold());
                }
            }
            // create the result from the heap and return
            return heap.elems();
        }
    public:
        template<class t_pl,class rank_func>
        static bool
        evaluate_candidate(typename std::vector<plist_data<t_pl>*>::iterator& candidate_itr,std::vector<plist_data<t_pl>*>& postings_lists,const rank_func& ranker,score_heap& heap,size_t initial_num_plists) {
#ifdef NEWT_PROFILE_QUERY_PERFORMANCE
            query_profiler::record_evaluation();
#endif

            // strategy: start from the back and partially evaluate till we know we can't get into the heap
            //std::cout << "evaluate_candidate = " << candidate_id << std::endl;
            uint64_t candidate_id = (*candidate_itr)->cur.docid();
            double W_d = ranker.doc_length(candidate_id);
            double doc_score = initial_num_plists * ranker.calc_doc_weight(W_d);
            auto itr = postings_lists.rbegin();
            auto end = postings_lists.rend();

            // try to figure out if we have to score based on block max scores first
            bool list_finished = false;
            bool needs_full_score = true;
            bool heap_changed = false;
            double block_doc_score = doc_score;
            while (itr != end) {
                if (block_doc_score + (*itr)->wand_cum_score < heap.threshold()) {
                    needs_full_score = false;
                    break;
                }
                (*itr)->cur.skip_to_block_with_id(candidate_id);
                if ((*itr)->cur == (*itr)->end) {
                    list_finished = true;
                    itr++;
                    continue;
                }
                if (block_doc_score + (*itr)->cur.block_max() + (*itr)->prev_cum_score < heap.threshold()) {
                    needs_full_score = false;
                    break;
                }
                block_doc_score += (*itr)->cur.block_max();
                itr++;
            }

            if (needs_full_score) {
                // based on the block max scores it looks like we might have a candidate
                itr = postings_lists.rbegin();
                while (itr != end) {
                    if ((*itr)->cur == (*itr)->end) {
                        itr++;
                        continue;
                    }
                    if ((*itr)->cur.docid() < candidate_id) {
                        (*itr)->cur.skip_to_id(candidate_id);
                        // check if we are at the end?
                        if ((*itr)->cur == (*itr)->end) {
                            list_finished = true;
                            itr++;
                            continue;
                        }
                    }
                    if ((*itr)->cur.docid() == candidate_id) {
                        // add to score
                        double contrib = ranker.calculate_impact((*itr)->qt->f_qt,
                                         (*itr)->cur.freq(),(double)(*itr)->qt->f_t,
                                         (*itr)->qt->F_t,W_d,(*itr)->qt->term_weight);
                        doc_score += contrib;
                        // skip to next after evaluating it
                        (*itr)->cur.skip_to_id(candidate_id+1);
                        if ((*itr)->cur == (*itr)->end) {
                            list_finished = true;
                        }
                    }
                    itr++;
                }

                double old_threshold = heap.threshold();
                heap.insert_if_higher_score( {candidate_id,doc_score});
                if (heap.threshold() != old_threshold) {
                    heap_changed = true;
                }
            } else {
                // skip the list with the smallest id to the next larger id
                (*candidate_itr)->cur.skip_to_id(candidate_id+1);
                if ((*candidate_itr)->cur == (*candidate_itr)->end) {
                    list_finished = true;
                }
            }

            if (list_finished) {
                //std::cout << "LIST FINISHED!!" << std::endl;
                // if there is a finished list we clean things up and recompute the cum_score.
                auto del_itr = postings_lists.begin();
                while (del_itr != postings_lists.end()) {
                    if ((*del_itr)->cur == (*del_itr)->end) {
                        //std::cout << "delete list" << std::endl;
                        del_itr = postings_lists.erase(del_itr);
                    } else {
                        del_itr++;
                    }
                }
                double cum_sum = 0;
                for (size_t i=0; i<postings_lists.size(); i++) {
                    postings_lists[i]->prev_cum_score = cum_sum;
                    cum_sum += postings_lists[i]->data->list_max_score();
                    postings_lists[i]->wand_cum_score = cum_sum;
                }
            }
            return (list_finished||heap_changed);
        }
        template<class t_pl>
        static typename std::vector<plist_data<t_pl>*>::iterator
        determine_smallest_id(typename std::vector<plist_data<t_pl>*>::iterator itr,const typename std::vector<plist_data<t_pl>*>::iterator& end) {
            uint64_t smallest_id = (*itr)->cur.docid();
            auto smallest_id_itr = itr;
            while (itr!= end) {
                if ((*itr)->cur.docid() < smallest_id) {
                    smallest_id = (*itr)->cur.docid();
                    smallest_id_itr = itr;
                }
                ++itr;
            }
            return smallest_id_itr;
        }
        template<class t_pl>
        static void
        sort_lists_by_max_score(std::vector<plist_data<t_pl>*>& postings_lists) {
            auto score_cmp = [](const plist_data<t_pl>* a,const plist_data<t_pl>* b) {
                return a->data->list_max_score() < b->data->list_max_score();
            };
            std::sort(postings_lists.begin(),postings_lists.end(),score_cmp);
            double cum_sum = 0;
            for (size_t i=0; i<postings_lists.size(); i++) {
                postings_lists[i]->prev_cum_score = cum_sum;
                cum_sum += postings_lists[i]->data->list_max_score();
                postings_lists[i]->wand_cum_score = cum_sum;
            }
        }
        template<class t_pl>
        static typename std::vector<plist_data<t_pl>*>::iterator
        find_essential_lists(typename std::vector<plist_data<t_pl>*>::iterator itr,const typename std::vector<plist_data<t_pl>*>::iterator& end,double threshold) {
            while (itr!= end) {
                if (threshold < (*itr)->wand_cum_score) break;
                ++itr;
            }
            return itr;
        }
};

#endif
