#ifndef PRUNED_SUFFIX_TREE_HPP
#define PRUNED_SUFFIX_TREE_HPP

#include <utility>  // for std::pair
#include <map>      // for std::map
#include <future>

#include <sdsl/suffix_trees.hpp>
#include "document_arrays.hpp"
#include "settings.hpp"
#include "logger.hpp"

typedef std::pair<size_t, size_t> tPII;
using namespace sdsl;

template<class t_csa>
class pruned_suffix_tree
{
    private:
        pruned_suffix_tree() = delete;
        // types
        typedef int_vector<>::size_type                                        size_type;
        typedef t_csa                                                          csa_type;
        using cst_type = cst_sct3<t_csa>;
    public:
        static std::vector<tPII> determine_ranges(cache_config& config,uint64_t min_stored) {
            using clock = std::chrono::high_resolution_clock;
            // create the cst if it does not exist
            cst_type cst;
            if (!cache_file_exists(newt_constants::KEY_CST+"_"+util::class_to_hash(cst), config)) {
                newt_log::info() << "\t- [CONSTRUCT] compressed suffix tree" << std::endl;
                construct(cst, config.file_map[newt_constants::KEY_INPUT], config, 0);
                store_to_cache(cst,newt_constants::KEY_CST+"_"+util::class_to_hash(cst), config);
            } else {
                newt_log::info() << "\t- [LOAD] compressed suffix tree" << std::endl;
                load_from_cache(cst,newt_constants::KEY_CST+"_"+util::class_to_hash(cst), config);
            }

            // max stored == number of documents = number of doc seps in T[]
            std::vector<uint64_t> tmp_pattern; tmp_pattern.push_back(newt_constants::document_delimiter);
            uint64_t max_stored = sdsl::count(cst,tmp_pattern.begin(),tmp_pattern.end()) + 1;

            // find the intervals we are interested in
            newt_log::info() << "\t- Finding intervals to be precomputed (min="
                             << min_stored << ",max="
                             << max_stored << ")" << std::endl;

            auto traverse_cst_start = clock::now();
            std::vector<tPII> ranges_to_store = find_precomputed_invervals(cst,max_stored,min_stored);
            auto traverse_cst_end = clock::now();

            auto traversal_time_sec = std::chrono::duration_cast<std::chrono::seconds>(traverse_cst_end-traverse_cst_start);
            newt_log::info() << "\t- Found " << ranges_to_store.size() << " intervals. (" << traversal_time_sec.count() << " sec)" << std::endl;
            util::clear(cst);


            // random shuffle ranges to try to balance load between threads later
            std::mt19937 g(4711);
            std::shuffle(ranges_to_store.begin(),ranges_to_store.end(), g);

            return ranges_to_store;
        }
    private:
        static bool skip_node(const cst_type& cst, const typename cst_type::node_type& v,uint64_t min_stored) {
            size_t node_size = cst.size(v);
            if (node_size < min_stored) return true;
            return false;
        }

        static bool skip_subtree(const cst_type& cst, const typename cst_type::node_type& v,uint64_t min_stored) {
            return cst.size(v) < min_stored;
        }

        static std::vector<tPII> find_precomputed_invervals(const cst_type& cst,uint64_t max_stored,uint64_t min_stored) {
            std::vector<tPII> ranges_to_store;
            for (auto it=cst.begin(); it!=cst.end(); ++it) {
                if (it.visit()==1) {
                    auto v = *it;
                    if (skip_node(cst,v,min_stored)) {
                        if (skip_subtree(cst,v,min_stored)) {
                            it.skip_subtree();
                        }
                    } else {
                        if (cst.size(v) <= max_stored) {
                            ranges_to_store.emplace_back(cst.lb(v),cst.rb(v));
                        }
                    }
                }
            }
            return ranges_to_store;
        }

};

#endif
