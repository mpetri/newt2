
#ifndef DOCMAP_HPP
#define DOCMAP_HPP

#include <limits>
#include <string>
#include <iostream>
#include <vector>
#include <exception>
#include <sstream>

#include "sdsl/io.hpp"

#include "settings.hpp"
#include "logger.hpp"

// see http://stackoverflow.com/questions/1567082/how-do-i-iterate-over-cin-line-by-line-in-c/1567703#1567703
class line
{
        std::string data;
    public:
        friend std::istream& operator>>(std::istream& is, line& l) {
            std::getline(is, l.data);
            return is;
        }
        operator std::string() const {
            return data;
        }
};

class document_map
{
    public:
        typedef int_vector<>::size_type                               size_type;
    private:
        sdsl::int_vector<>                                            m_lengths;
        std::vector<std::string>                                        m_names;
        double                    m_avg_length = 0;
        uint32_t                  m_min_length = std::numeric_limits<uint32_t>::max();
        uint32_t                  m_max_length = std::numeric_limits<uint32_t>::lowest();
    private:
        void add_document(uint64_t& len,std::vector<std::string>& names);
    public:
        auto serialize(std::ostream& out, structure_tree_node* v=NULL, std::string name="")  const -> size_type;
        document_map() = default;
        document_map(cache_config& config);
        document_map& operator=(document_map&& dm) {
            m_lengths = std::move(dm.m_lengths);
            m_names = std::move(dm.m_names);
            m_avg_length = dm.m_avg_length;
            m_min_length = dm.m_min_length;
            m_max_length = dm.m_max_length;
            return *this;
        }
        void load(std::istream& in);
        uint32_t length(size_t id) const {
            return m_lengths[id];
        };
        std::string name(size_t id) const {
            return m_names[id];
        }
        size_t num_documents() const {
            return m_names.size();
        };
    public:
        const double& average_length = m_avg_length;
        const uint32_t& minimum_length = m_min_length;
        const uint32_t& maximum_length = m_max_length;
};

document_map::document_map(cache_config& config)
{
    // read the names of the documents into a temporary vector
    // iterate over text and determine document bounds
    newt_log::info() << "\t- reading document names " << std::endl;

    std::ifstream docmap_file(config.file_map[newt_constants::KEY_INPUTDOCMAP]);
    if (! docmap_file.is_open()) {
        throw std::logic_error("document_map: can not open document map file.");
    }

    std::vector<std::string> doc_names;
    std::copy(std::istream_iterator<line>(docmap_file),
              std::istream_iterator<line>(),
              std::back_inserter(doc_names));

    newt_log::info() << "\t- read " << doc_names.size() << " document names from map file." << std::endl;

    // iterate over text and determine document lengths
    newt_log::info() << "\t- parsing collection " << config.file_map[newt_constants::KEY_INPUT].c_str() << std::endl;
    sdsl::int_vector_buffer<> text_buf(config.file_map[newt_constants::KEY_INPUT].c_str(),std::ios_base::in);

    m_lengths.resize(doc_names.size());
    size_type last_doc_delem = 0;
    size_type n = text_buf.size();
    for (size_type i=0; i<n; i++) {
        if (text_buf[i] == newt_constants::document_delimiter) {
            uint64_t doc_size = i - last_doc_delem;
            add_document(doc_size,doc_names);
            last_doc_delem = i;
        }
    }

    newt_log::info() << "\t- added " << m_names.size() << " documents to document map." << std::endl;
    if (doc_names.size() != m_names.size()) {
        throw std::logic_error("number of names in docmap file does not match number of document identifiers in text.");
    }
    if (m_names.size() == 0) {
        throw std::logic_error("no documents found in collection.");
    }

    // calc average
    m_avg_length = (double)n / (double) m_names.size();

    newt_log::info() << "\t- min/avg/max length: " << m_min_length << "/" << m_avg_length << "/" << m_max_length << std::endl;
}

void document_map::add_document(uint64_t& len,std::vector<std::string>& names)
{
    if (len == 0) {
        throw std::logic_error("no documents of length 0 allowed.");
    }

    if (names.size() <= m_names.size()) {
        std::stringstream msg_stream;
        msg_stream      << "number of document names (" << names.size() << ") is smaller "
                        << "than the number of documents (" << m_names.size()+1
                        << ") in the text.";
        throw std::logic_error(msg_stream.str());
    }
    m_lengths[m_names.size()] = len;
    m_names.push_back(names[m_names.size()]);

    if (m_max_length < len) m_max_length = len;
    if (m_min_length > len) m_min_length = len;
}

void document_map::load(std::istream& in)
{
    size_type num_docs;
    sdsl::read_member(num_docs,in);
    sdsl::read_member(m_avg_length,in);
    sdsl::read_member(m_max_length,in);
    sdsl::read_member(m_min_length,in);

    int_vector<> strlens;
    m_lengths.load(in);
    strlens.load(in);

    int_vector<8> string_data;
    string_data.load(in);

    m_names.resize(num_docs);
    size_t string_data_processed = 0;
    for (size_type i=0; i<num_docs; i++) {
        // extract string
        std::string docid(strlens[i],' ');
        for (size_t j=0; j<strlens[i]; j++) docid[j] = string_data[string_data_processed+j];
        string_data_processed += strlens[i];
        m_names[i] = docid;
    }
}

auto document_map::serialize(std::ostream& out, sdsl::structure_tree_node* v, std::string name) const -> size_type
{
    structure_tree_node* child = sdsl::structure_tree::add_child(v, name, util::class_name(*this));
    size_t written_bytes = 0;

    // write the size first == num_docs
    sdsl::write_member(m_names.size(),out,child,"number of documents");
    sdsl::write_member(m_avg_length,out,child,"average document length");
    sdsl::write_member(m_max_length,out,child,"max document length");
    sdsl::write_member(m_min_length,out,child,"min document length");

    int_vector<> strlens(m_names.size());

    size_t total_str_len = 0;
    for (size_type i=0; i<m_names.size(); i++) {
        strlens[i] = m_names[i].size();
        total_str_len += strlens[i];
    }
    util::bit_compress(strlens);

    written_bytes += m_lengths.serialize(out,child,"document lengths");
    written_bytes += strlens.serialize(out,child,"string lengths");

    // write string data
    int_vector<8> string_data(total_str_len);
    size_t j = 0;
    for (size_t i=0; i<m_names.size(); i++) {
        for (size_t l=0; l<m_names[i].size(); l++) string_data[j++] = m_names[i][l];
    }
    written_bytes += string_data.serialize(out,child,"string data");

    sdsl::structure_tree::add_size(child, written_bytes);
    return written_bytes;
}


#endif
