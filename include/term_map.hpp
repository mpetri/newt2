
#ifndef TERMMAP_HPP
#define TERMMAP_HPP

#include <limits>
#include <string>
#include <iostream>
#include <vector>
#include <exception>
#include <sstream>
#include <unordered_map>

#include "sdsl/io.hpp"

#include "settings.hpp"
#include "logger.hpp"

class term_map
{
    public:
        typedef int_vector<>::size_type                            size_type;
    private:
        std::vector<std::string>                                   m_id_to_term;
        std::vector<uint64_t>                                      m_id_to_idf;
        std::unordered_map<std::string,size_t>                     m_term_to_id;
    public: // constructors
        term_map() = default;
        term_map(cache_config& config);
        void load(std::istream& in);
        auto serialize(std::ostream& out, structure_tree_node* v=NULL, std::string name="") const -> size_type;
    public: // access methods
        const std::string& term(const size_t id) const {
            if (id >= m_id_to_term.size()) {
                throw std::logic_error("id larger than all ids in term map.");
            }
            return m_id_to_term[id];
        };
        size_t id(const std::string& term) const {
            auto res = m_term_to_id.find(term);
            if (res == m_term_to_id.end()) {
                return 0;
            }
            return (*res).second;
        };
        std::vector<size_t> map_query(const std::string& line) const {
            std::vector<size_t> res;

            std::vector<std::string> query_terms;
            std::string::size_type p = 0;
            std::string::size_type q;
            while ((q = line.find(newt_constants::query_term_seperator, p)) != std::string::npos) {
                query_terms.emplace_back(line, p, q - p);
                p = q + 1;
            }
            if (p != line.size()) {
                query_terms.emplace_back(line, p);
            }

            for (const auto& token : query_terms) {
                uint64_t term_id = id(token);
                if (term_id != 0) {
                    res.emplace_back(term_id);
                } else {
                    std::cerr <<  "skip parsing token '" << token << "'"
                              << " (does not exist in termmap)" << std::endl;
                }
            }
            return res;
        }
        std::string map_ids(const std::vector<size_t>& ids) const {
            std::string res;
            for (const auto& id : ids) {
                auto str = term(id);
                res = res + newt_constants::query_term_seperator + str;
            }
            return res;
        }
        uint64_t idf(const size_t id) const {
            if (id >= m_id_to_idf.size()) {
                throw std::logic_error("id larger than all ids in term map.");
            }
            return m_id_to_idf[id];
        };
        size_t num_terms() const {
            return m_term_to_id.size();
        }
        uint64_t max_id() const {
            return m_id_to_term.size()-1;
        }
        auto begin() const -> std::unordered_map<std::string,size_t>::const_iterator {
            return m_term_to_id.begin();
        }
        auto end() const -> std::unordered_map<std::string,size_t>::const_iterator {
            return m_term_to_id.end();
        }
};

term_map::term_map(cache_config& config)
{

    // iterate over text and determine document bounds
    newt_log::info() << "\t- parsing term map file" << std::endl;

    std::ifstream termmap_file(config.file_map[newt_constants::KEY_INPUTTERMMAP]);
    if (! termmap_file.is_open()) {
        throw std::logic_error("termmap_file: can not open term map file.");
    }

    std::unordered_map<size_t,uint64_t> tmp_id_idf_map;
    unsigned long long maxid = std::numeric_limits<unsigned long long>::lowest();
    std::string line,term;
    char token_buffer[4096];
    unsigned long long id,idf;
    while (std::getline(termmap_file, line)) {
        if (sscanf(line.c_str(),"%s %llu %llu",token_buffer,&id,&idf) != 3) {
            throw std::logic_error("error parsing term file.");
        }
        size_t tlen = strlen(token_buffer);
        term = std::string(token_buffer,tlen);

        if (id > maxid) maxid = id;

        // check if exists
        if (m_term_to_id.find(term) != m_term_to_id.end()) {
            size_t eid = m_term_to_id[term];
            std::stringstream msg_stream;
            msg_stream      << "termmap: term '" << term << "' has "
                            << "more than one id (" << eid << "," << id << ").";
            throw std::logic_error(msg_stream.str());
        }

        m_term_to_id.insert(std::make_pair(term,id));
        tmp_id_idf_map.insert(std::make_pair(id,idf));
    }
    newt_log::info() << "\t- parsed " << m_term_to_id.size() << " terms." << std::endl;
    newt_log::info() << "\t- creating id->term and id->idf mappings" << std::endl;

    m_id_to_term.resize(maxid+1);
    m_id_to_idf.resize(maxid+1);

    for (const auto &p : m_term_to_id) {
        m_id_to_term[p.second] = p.first;
        m_id_to_idf[p.second] = tmp_id_idf_map[p.second];
    }

    newt_log::info() << "\t- max id: " << maxid  << " num terms: " << m_term_to_id.size() << std::endl;
}


void term_map::load(std::istream& in)
{
    size_type num_terms;
    uint64_t max_id;
    sdsl::read_member(max_id,in);
    sdsl::read_member(num_terms,in);

    int_vector<> ids;
    int_vector<> idfs;
    int_vector<> strlens;
    int_vector<8> string_data;

    ids.load(in);
    idfs.load(in);
    strlens.load(in);
    string_data.load(in);

    m_id_to_term.resize(max_id);
    m_id_to_idf.resize(max_id);

    size_t string_data_processed = 0;
    for (size_t i=0; i<ids.size(); i++) {
        // extract string
        std::string term(strlens[i],' ');
        for (size_t j=0; j<strlens[i]; j++) term[j] = string_data[string_data_processed+j];
        string_data_processed += strlens[i];

        uint64_t id = ids[i];
        m_id_to_term[id] = term;
        m_id_to_idf[id] = idfs[i];

        m_term_to_id.insert(std::make_pair(term,id));
    }
}

auto term_map::serialize(std::ostream& out, sdsl::structure_tree_node* v, std::string name) const -> size_type
{
    structure_tree_node* child = sdsl::structure_tree::add_child(v, name, util::class_name(*this));
    size_t written_bytes = 0;

    // write the size first == num_docs
    sdsl::write_member(m_id_to_term.size(),out,child,"max id");
    sdsl::write_member(m_term_to_id.size(),out,child,"num terms");

    int_vector<> ids(m_term_to_id.size());
    int_vector<> idfs(m_term_to_id.size());
    int_vector<> strlens(m_term_to_id.size());
    std::vector<std::string> term_data(m_term_to_id.size());

    size_t j=0;
    size_t total_str_len = 0;
    for (const auto& p : m_term_to_id) {
        uint64_t id = p.second;
        const std::string& term = p.first;
        ids[j] = id;
        idfs[j] = m_id_to_idf[id];
        strlens[j] = term.size();
        total_str_len += term.size();
        term_data[j] = term;
        j++;
    }
    util::bit_compress(ids);
    util::bit_compress(idfs);
    util::bit_compress(strlens);

    int_vector<8> string_data(total_str_len);
    j = 0;
    for (size_t i=0; i<term_data.size(); i++) {
        for (size_t l=0; l<term_data[i].size(); l++) string_data[j++] = term_data[i][l];
    }

    written_bytes += ids.serialize(out,child,"id");
    written_bytes += idfs.serialize(out,child,"idf");
    written_bytes += strlens.serialize(out,child,"string lengths");
    written_bytes += string_data.serialize(out,child,"string data");

    sdsl::structure_tree::add_size(child, written_bytes);
    return written_bytes;
}

#endif
