
#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <kisslog.hpp>

class rawlogger_t
{
        kisslog::util::CharUtil<char> mCharUtil;
    public:
        rawlogger_t() {};

        template <kisslog::severity::Severity S>
        void log(std::string line) {
            std::cerr << mCharUtil.iso_now() << " : " << kisslog::severity::asPrefix<S,char>() << " : " << line;
        }
};

typedef kisslog::logger<rawlogger_t,kisslog::severity::WARNING> warnlogger_t;
typedef kisslog::logger<rawlogger_t,kisslog::severity::DEBUG> debuglogger_t;
typedef kisslog::logger<rawlogger_t,kisslog::severity::ERR> errorlogger_t;
typedef kisslog::logger<rawlogger_t,kisslog::severity::INFO> infologger_t;

class newt_log
{
    public:
        static infologger_t& theLogger() {
            static rawlogger_t rlog;
            static infologger_t logger(rlog);
            return logger;
        }
        static std::basic_ostream<char>& debug() {
            return theLogger().debug();
        }
        static std::basic_ostream<char>& info() {
            return theLogger().info();
        }
        static std::basic_ostream<char>& notice() {
            return theLogger().notice();
        }
        static std::basic_ostream<char>& warning() {
            return theLogger().warning();
        }
        static std::basic_ostream<char>& err() {
            return theLogger().err();
        }
        static std::basic_ostream<char>& crit() {
            return theLogger().crit();
        }
        static std::basic_ostream<char>& alert() {
            return theLogger().alert();
        }
};


#endif
