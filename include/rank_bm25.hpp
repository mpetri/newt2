#ifndef RANK_BM25_HPP
#define RANK_BM25_HPP

#include "document_map.hpp"

template<uint32_t t_k1=120,uint32_t t_b=75>
class rank_bm25
{
    private:
        double                                               k1 = (double)t_k1/100.0;
        double                                               b = (double)t_b/100.0;
        double                                               m_num_terms; // |C|
        double                                               N;  // |D|
        double                                               W_A;  // |D|
        const document_map&                                  m_dmap;
    public:
        rank_bm25() = delete;

        //! Constructor
        rank_bm25(uint64_t num_terms,const document_map& dm)
            : m_num_terms(num_terms) , N(dm.num_documents()) , W_A(dm.average_length) , m_dmap(dm) {

        }

        double calculate_docscore(const double f_qt,const double f_dt,const double f_t,const double F_t,const double W_d) const {
            double w_qt = log((N - f_t + 0.5) / (f_t+0.5)) * f_qt;
            double K_d = k1*((1-b) + (b*(W_d/W_A)));
            double w_dt = ((k1+1)*f_dt) / (K_d + f_dt);
            return w_dt*w_qt;
        }

        double doc_length(uint64_t id) const {
            return (double) m_dmap.length(id);
        }


        double calculate_impact(const double f_qt,const double f_dt,const double f_t,const double F_t,const double W_d,const double term_weight) const {
            return calculate_docscore(f_qt,f_dt,f_t,F_t,W_d) * term_weight;
        }

        // used only during construction of document map
        static double calc_doc_weight(uint32_t W_d) {
            return (double)0.0f;
        }
};

#endif
