#ifndef RANK_LMDS_HPP
#define RANK_LMDS_HPP

template<uint32_t t_smoothing_param = 2500>
class rank_lmds
{
    private:
        double                                               m_num_terms; // |C|
        double                                               N;  // |D|
        double                                               W_A;  // |D|
        const document_map&                                  m_dmap;
    public:
        rank_lmds() = delete;

        //! Constructor
        rank_lmds(uint64_t num_terms,const document_map& dm)
            : m_num_terms(num_terms) , N(dm.num_documents()) , W_A(dm.average_length) , m_dmap(dm) {

        }

        double calculate_docscore(const double f_qt,const double f_dt,const double f_t,const double F_t,const double W_d) const {
            double normalization = m_num_terms/F_t;
            double doc_score = (f_dt/(double)t_smoothing_param) * normalization;
            return log(doc_score+1);
        }

        double doc_length(uint64_t id) const {
            return (double) m_dmap.length(id);
        }

        double calculate_impact(const double f_qt,const double f_dt,const double f_t,const double F_t,const double W_d,const double term_weight) const {
            return calculate_docscore(f_qt,f_dt,f_t,F_t,W_d) * term_weight;
        }

        // used only during construction of document map
        static double calc_doc_weight(uint32_t size) {
            return log((double)t_smoothing_param /
                       ((double)t_smoothing_param + (double)size)
                      );
        }
};

#endif
