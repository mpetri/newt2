
#ifndef QUERY_PROCESSOR_HPP
#define QUERY_PROCESSOR_HPP


#include <algorithm>
#include "query.hpp"
#include "output_formats.hpp"
#include "result.hpp"
#include "query_strategies.hpp"
#include "query_profiler.hpp"

template<class t_output,
         class t_ranker,
         class t_index
         >
class query_processor
{
    public:
        typedef typename t_index::plist_type                         plist_type;
    public:
        static t_output
        process_queries(const t_index& index,std::vector<query>& queries,uint64_t k,query_strat query_strategy) {
            t_output output;
            for (const auto& qry : queries) {
                output.results.emplace_back(process_query(index,qry,k,query_strategy));
                newt_log::info() << "QUERY [" << qry.number  << "] - "
                                 <<  output.results.back().total_time.count()
                                 << " ms" << std::endl;
            }
            return output;
        };

        static auto
        retrieve_postings_lists(query& qry,const t_index& index) -> std::vector<plist_data<plist_type>> {
            std::vector<plist_data<plist_type>> lists;
            for (auto& qt : qry.terms) {
                if (qt.F_t > 0 && qt.F_t <= index.docmap.num_documents()) {
                    auto& plist = index.lookup_or_create( {qt.sp,qt.ep});
                    qt.f_t = plist.size();
                    lists.emplace_back(plist_data<plist_type>(plist,qt));
                }
            }
            return lists;
        }

        static result
        process_query(const t_index& index,query qry,uint64_t k,query_strat query_strategy) {
            result res;
            uint64_t num_terms = index.csa.size() - index.docmap.num_documents();
            t_ranker ranker(num_terms,index.docmap);

            // (1) determine ranges in SA
            using clock = std::chrono::high_resolution_clock;
            auto csa_start = clock::now();
            for (uint64_t i=0; i<qry.terms.size(); i++) {
                auto& qt = qry.terms[i];
                sdsl::backward_search(index.csa,0ULL,index.csa.size()-1,
                                      qt.tokens.begin(),qt.tokens.end(),qt.sp,qt.ep);
                qt.F_t = qt.ep - qt.sp + 1;
            }
            auto csa_stop = clock::now();

            // (2) retrieve all postings lists
            auto candidates_start = clock::now();
            auto postings_lists = retrieve_postings_lists(qry,index);
            auto candidates_stop = clock::now();

            // we work with pointers to the actual data instead to avoid copying
            std::vector<plist_data<plist_type>*> ptr_lists;
            for (auto& pl : postings_lists) {
                ptr_lists.push_back(&pl);
            }

            // (3) process the lists
            auto list_process_start = clock::now();

#ifdef NEWT_PROFILE_QUERY_PERFORMANCE
            query_profiler::set_cur_qry_and_strat(qry.number,query_strategy);
#endif

            switch (query_strategy) {
                case QUERY_TAAT:
                    res.list = query_strategy_taat::process(ptr_lists,ranker,k);
                    break;
                case QUERY_DAAT:
                    res.list = query_strategy_daat::process(ptr_lists,ranker,k);
                    break;
                case QUERY_WAND:
                    res.list = query_strategy_wand::process(ptr_lists,ranker,k);
                    break;
                case QUERY_BMW:
                    res.list = query_strategy_bmw::process(ptr_lists,ranker,k);
                    break;
                case QUERY_MAXSCORE:
                    res.list = query_strategy_maxscore::process(ptr_lists,ranker,k);
                    break;
                    // case QUERY_BMM:
                    //     res.list = query_strategy_bmm::process(ptr_lists,ranker,k);
                    //     break;
                default:
                    newt_log::crit() << "Unknown query strategy!"  << std::endl;
                    exit(EXIT_FAILURE);
            }
            auto list_process_stop = clock::now();

            // timing stuff
            auto total_elapsed = (csa_stop-csa_start) + (candidates_stop-candidates_start) + (list_process_stop-list_process_start);
            res.total_time = std::chrono::duration_cast<std::chrono::milliseconds>(total_elapsed);
            res.csa_time = std::chrono::duration_cast<std::chrono::milliseconds>(csa_stop-csa_start);
            res.candidate_time = std::chrono::duration_cast<std::chrono::milliseconds>(candidates_stop-candidates_start);
            res.process_time = std::chrono::duration_cast<std::chrono::milliseconds>(list_process_stop-list_process_start);

            // clear the tmp content in the lists
            index.clear_onthefly();

            // ensure the same sorting order of the results
            struct {
                bool operator()(const doc_res_t& a,const doc_res_t& b) {
                    if (a.second == b.second) {
                        return a.first < b.first;
                    }
                    return a.second > b.second;
                }
            } doc_res_sorter;
            std::sort(res.list.begin() , res.list.end() , doc_res_sorter);
            res.qry = std::move(qry);
                      res.query_strategy = query_strategy;
                      return res;
        };

};

#endif
