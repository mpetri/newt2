#ifndef QUERY_STRAT_DAAT_HPP
#define QUERY_STRAT_DAAT_HPP

#include "settings.hpp"
#include "result.hpp"
#include "query.hpp"
#include "score_heap.hpp"
#include "query_profiler.hpp"

class query_strategy_daat
{
    public:
        template<class t_pl,class rank_func>
        static ranked_result_list_t
        process(std::vector<plist_data<t_pl>*>& postings_lists,const rank_func& ranker,size_t k) {
            score_heap heap(k,0.0);

            double query_factor = 0;
            for (const auto& pl : postings_lists) {
                query_factor += pl->qt->term_weight;
            }

            sort_list_by_id(postings_lists);

            // perform daat
            while (!postings_lists.empty()) {
                evaluate_smallest_id(postings_lists,ranker,heap,query_factor);
                sort_list_by_id(postings_lists);
            }

            // create the result from the heap and return
            return heap.elems();
        }
    public:

        template<class t_pl,class rank_func>
        static void
        evaluate_smallest_id(std::vector<plist_data<t_pl>*>& postings_lists,const rank_func& ranker,score_heap& heap,double query_factor) {
#ifdef NEWT_PROFILE_QUERY_PERFORMANCE
            query_profiler::record_evaluation();
#endif

            auto doc_id = postings_lists[0]->cur.docid();
            double W_d = ranker.doc_length(doc_id);
            double doc_score = query_factor * ranker.calc_doc_weight(W_d);
            auto itr = postings_lists.begin();
            auto end = postings_lists.end();
            while (itr != end) {
                if ((*itr)->cur.docid() == doc_id) {
                    double contrib = ranker.calculate_impact((*itr)->qt->f_qt,
                                     (*itr)->cur.freq(),(double)(*itr)->qt->f_t,
                                     (*itr)->qt->F_t,W_d,(*itr)->qt->term_weight);
                    doc_score += contrib;
                    ++((*itr)->cur); // advance
                } else {
                    break;
                }
                itr++;
            }
            // add the doc score
            heap.insert_if_higher_score( {doc_id,doc_score});
        }

        template<class t_pl>
        static void
        sort_list_by_id(std::vector<plist_data<t_pl>*>& postings_lists) {
            // check if we can delete stuff
            auto ditr = postings_lists.begin();
            while (ditr != postings_lists.end()) {
                if ((*ditr)->cur == (*ditr)->end) {
                    ditr = postings_lists.erase(ditr);
                } else {
                    ditr++;
                }
            }
            // sort
            auto ptr_sort = [](const plist_data<t_pl>* a,const plist_data<t_pl>* b) {
                return *a < *b;
            };
            std::sort(postings_lists.begin(),postings_lists.end(),ptr_sort);
        }
};


#endif
