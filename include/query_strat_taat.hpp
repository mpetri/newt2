#ifndef QUERY_STRAT_TAAT_EXHAUSTIVE_HPP
#define QUERY_STRAT_TAAT_EXHAUSTIVE_HPP

#include "settings.hpp"
#include "result.hpp"
#include "query.hpp"
#include "query_profiler.hpp"

// exhaustive term at a time (taat) processing
class query_strategy_taat
{
    public:
        template<class t_pl,class rank_func>
        static ranked_result_list_t
        process(std::vector<plist_data<t_pl>*>& postings_lists,const rank_func& ranker,size_t k) {
            std::unordered_map<uint64_t,double> candidates;

            double query_factor = 0;
            for (const auto& pl : postings_lists) {
                query_factor += pl->qt->term_weight;
            }

            for (size_t j=0; j<postings_lists.size(); j++) {
                auto& pl = postings_lists[j];
                while (pl->cur != pl->end) {
                    auto doc_id = pl->cur.docid();
                    double f_dt = pl->cur.freq();
                    double W_d = ranker.doc_length(doc_id);
                    double score = ranker.calculate_impact(pl->qt->f_qt,f_dt,pl->qt->f_t,pl->qt->F_t,W_d,pl->qt->term_weight);
                    auto citr = candidates.find(doc_id);
                    if (citr == candidates.end()) {
#ifdef NEWT_PROFILE_QUERY_PERFORMANCE
                        query_profiler::record_evaluation();
#endif

                        // not seen before
                        double doc_score = query_factor*ranker.calc_doc_weight(W_d);
                        candidates[doc_id] = doc_score + score;
                    } else {
                        // already has a score
                        citr->second += score;
                    }
                    ++(pl->cur);
                }
            }
            // sort the result and return the real top-k documents
            uint64_t real_k = candidates.size();
            if (real_k > k) real_k = k;

            std::vector<doc_res_t> result_array;
            for (const auto& doc: candidates) {
                result_array.emplace_back(doc.first,doc.second);
            }
            std::partial_sort(result_array.begin(),result_array.begin()+real_k,result_array.end() ,
            [](const doc_res_t& a,const doc_res_t& b) {
                if (a.second == b.second) return a.first < b.first;
                return a.second > b.second;
            }
                             );
            result_array.resize(real_k);
            return result_array;
        }
};

#endif
