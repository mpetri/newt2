#ifndef QUERY_PROFILER_HPP
#define QUERY_PROFILER_HPP

#include "settings.hpp"

struct profile_stats {
    uint64_t query_num = 0;
    query_strat query_strategy = QUERY_UNKNOWN;
    uint64_t evaluations = 0;
    uint64_t block_decodes = 0;
    uint64_t id_skips = 0;
    uint64_t block_skips = 0;
    profile_stats(uint64_t qid,query_strat qs) : query_num(qid) , query_strategy(qs) {}
};

class query_profiler
{
    public:
        std::vector<profile_stats>                                          m_stats;
    private:
        query_profiler() {};
        static query_profiler& the_profiler() {
            static query_profiler qp;
            return qp;
        }
    public:
        static void set_cur_qry_and_strat(uint64_t qid,query_strat query_strategy) {
            auto& qp = the_profiler();
            qp.m_stats.emplace_back(profile_stats(qid,query_strategy));
        }
        static void record_evaluation() {
            auto& qp = the_profiler();
            qp.m_stats.back().evaluations++;
        }
        static void record_block_decode() {
            auto& qp = the_profiler();
            qp.m_stats.back().block_decodes++;
        }
        static void record_skip_id() {
            auto& qp = the_profiler();
            qp.m_stats.back().id_skips++;
        }
        static void record_skip_block() {
            auto& qp = the_profiler();
            qp.m_stats.back().block_skips++;
        }

        static void output_stats(std::ostream& os) {
            // write header
            if (os.good()) {
                os << "query_num;query_strategy;metric;value\n";
                auto& qp = the_profiler();
                std::set<query_strat> used_strats;
                for (const auto& pstats : qp.m_stats) {
                    used_strats.insert(pstats.query_strategy);
                    os << pstats.query_num << ";" << sg_query_strat_names[pstats.query_strategy] << ";" << "evaluations;" << pstats.evaluations << "\n"
                       << pstats.query_num << ";" << sg_query_strat_names[pstats.query_strategy] << ";" << "block_decodes;" << pstats.block_decodes << "\n"
                       << pstats.query_num << ";" << sg_query_strat_names[pstats.query_strategy] << ";" << "id_skips;" << pstats.id_skips << "\n"
                       << pstats.query_num << ";" << sg_query_strat_names[pstats.query_strategy] << ";" << "block_skips;" << pstats.block_skips << "\n";
                }
                // also add the sum over all the queries
                for (const auto qs : used_strats) {
                    size_t evaluated = 0; size_t block_decodes = 0; size_t id_skips = 0; size_t block_skips = 0;
                    for (const auto& pstats : qp.m_stats) {
                        if (pstats.query_strategy == qs) {
                            evaluated += pstats.evaluations;
                            block_decodes += pstats.block_decodes;
                            id_skips += pstats.id_skips;
                            block_skips += pstats.block_skips;
                        }
                    }
                    os << "TOTAL"  << ";" << sg_query_strat_names[qs] << ";" << "evaluations;" << evaluated << "\n"
                       << "TOTAL"  << ";" << sg_query_strat_names[qs] << ";" << "block_decodes;" << block_decodes << "\n"
                       << "TOTAL"  << ";" << sg_query_strat_names[qs] << ";" << "id_skips;" << id_skips << "\n"
                       << "TOTAL"  << ";" << sg_query_strat_names[qs] << ";" << "block_skips;" << block_skips << "\n";
                }
            }
        }
};

#endif
