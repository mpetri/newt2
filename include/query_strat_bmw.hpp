#ifndef QUERY_STRAT_BMW_HPP
#define QUERY_STRAT_BMW_HPP

#include "settings.hpp"
#include "result.hpp"
#include "query.hpp"
#include "score_heap.hpp"
#include "query_profiler.hpp"

#include <iomanip>
#include <functional>
#include <limits>
#include <random>
#include <unordered_set>

//#define DEBUG_BMW 1

enum cstate {
    ID_IS_CANDIDATE,
    BLOCKS_MIGHT_CONTAIN_CANDIDATE,
    BLOCKS_DONT_CONTAIN_CANDIDATE
};

class query_strategy_bmw
{
    public:
        template<class t_pl,class rank_func>
        static ranked_result_list_t
        process(std::vector<plist_data<t_pl>*>& postings_lists,const rank_func& ranker,size_t k) {
            score_heap heap(k,0.0);

            double query_factor = 0;
            for (const auto& pl : postings_lists) {
                query_factor += pl->qt->term_weight;
            }

            // find the initial sort order
            sort_list_by_id(postings_lists);
            auto pivot_list = determine_next_candidate(postings_lists,heap.threshold(),query_factor);

#ifdef DEBUG_BMW
            std::cerr << "DOC_MAXS: ";
            for (const auto& pl : postings_lists) {
                std::cerr << "[" << pl->data->max_doc_weight() << "]";
            }
            std::cerr << std::endl;
#endif

            // perform bmw
            while (pivot_list != postings_lists.end()) {
#ifdef DEBUG_BMW
                std::cout << "pivot = " << (*pivot_list)->cur.docid() << " BR: ";
                for (const auto& pl : postings_lists) {
                    auto cur_block_id = pl->cur.m_cur_block_id;
                    if (cur_block_id != 0) {
                        std::cout << "[" << pl->data->block_rep(cur_block_id-1)+1 << ","
                                  << pl->data->block_rep(cur_block_id) << "]";
                    } else {
                        std::cout << "[0," << pl->data->block_rep(cur_block_id) << "]";
                    }
                }
                std::cout << "  -  BM: ";

                for (const auto& pl : postings_lists) {
                    auto cur_block_id = pl->cur.m_cur_block_id;
                    std::cout << "!" << pl->data->block_max(cur_block_id) << "!";
                }

                std::cout << "  -  ";
                for (const auto& pl : postings_lists) {
                    std::cout << "<" << pl->cur.remaining() << ">";
                }
                std::cout << "  -  ";
                auto itr = postings_lists.begin();
                while (itr != postings_lists.end()) {
                    if (itr == pivot_list) std::cout << "+" << (*itr)->cur.docid() << "+";
                    else std::cout << "|" << (*itr)->cur.docid() << "|";
                    itr++;
                }
                std::cout << std::endl;
#endif
                auto candidate_state = potential_candidate(postings_lists,pivot_list,heap.threshold(),ranker,query_factor);

                switch (candidate_state) {
                    case ID_IS_CANDIDATE:
                        evaluate_candidate(postings_lists,pivot_list,heap,ranker,query_factor);
                        break;
                    case BLOCKS_MIGHT_CONTAIN_CANDIDATE:
                        forward_inside_blocks(postings_lists,pivot_list);
                        break;
                    case BLOCKS_DONT_CONTAIN_CANDIDATE:
                        forward_lists(postings_lists,pivot_list);
                        break;
                }
                pivot_list = determine_next_candidate(postings_lists,heap.threshold(),query_factor);
            }

            // create the result from the heap and return
            return heap.elems();
        }
    public:
        template<class t_pl>
        static void
        forward_inside_blocks(std::vector<plist_data<t_pl>*>& postings_lists,const typename std::vector<plist_data<t_pl>*>::iterator& pivot_list) {
#ifdef DEBUG_BMW
            std::cout << "forward_inside_blocks " << std::endl;
#endif
            // forward the pivot list by one
            ++((*pivot_list)->cur);

            if ((*pivot_list)->cur == (*pivot_list)->end) {
                // list is finished! reorder list by id
                sort_list_by_id(postings_lists);
                return;
            }

            // bubble it down!
            auto itr = pivot_list;
            auto next = pivot_list + 1;
            auto list_end = postings_lists.end();
            while (next != list_end && (*itr)->cur.docid() > (*next)->cur.docid()) {
                std::swap(*itr,*next);
                itr = next;
                next++;
            }
        }
        template<class t_pl>
        static void
        forward_lists(std::vector<plist_data<t_pl>*>& postings_lists,const typename std::vector<plist_data<t_pl>*>::iterator& pivot_list) {
#ifdef DEBUG_BMW
            std::cout << "forward_lists = ";
#endif
            auto smallest_itr = find_shortest_list(postings_lists,pivot_list+1);

#ifdef DEBUG_BMW
            std::cout << " shortest = " << (*smallest_itr)->cur.remaining() << " remaining | ";
#endif

            // determine the next id we might have to eval
            auto list_end = postings_lists.end();
            auto itr = postings_lists.begin();
            auto end = pivot_list + 1;
            uint64_t candidate_id = std::numeric_limits<uint64_t>::max();
            while (itr != end) {
                uint64_t block_candidate = (*itr)->cur.block_rep() + 1;
                candidate_id = std::min(candidate_id,block_candidate);
                itr++;
            }
            if (end != list_end) {
                candidate_id = std::min(candidate_id,(*end)->cur.docid());
            }
            itr++;

#ifdef DEBUG_BMW
            std::cout << "candidate id = " << candidate_id << " | ";

            std::cout << "advance smallest from " << (*smallest_itr)->cur.docid();
#endif

            // advance the smallest list to it
            (*smallest_itr)->cur.skip_to_id(candidate_id);

            if ((*smallest_itr)->cur == (*smallest_itr)->end) {
                // list is finished! reorder list by id
                sort_list_by_id(postings_lists);
                return;
            }

#ifdef DEBUG_BMW
            std::cout << " to " << (*smallest_itr)->cur.docid() << std::endl;
#endif


            // bubble it down!
            auto next = smallest_itr + 1;
            while (next != list_end && (*smallest_itr)->cur.docid() > (*next)->cur.docid()) {
                std::swap(*smallest_itr,*next);
                smallest_itr = next;
                next++;
            }
        }

        template<class t_pl,class rank_func>
        static void
        evaluate_candidate(std::vector<plist_data<t_pl>*>& postings_lists,
                           const typename std::vector<plist_data<t_pl>*>::iterator& pivot_list,
                           score_heap& heap,
                           rank_func& ranker,
                           double query_factor) {
            auto pivot_id = (*pivot_list)->cur.docid();
            if (postings_lists[0]->cur.docid() == pivot_id) {
#ifdef DEBUG_BMW
                std::cout << "evaluate pivot = " << pivot_id;
#endif
#ifdef NEWT_PROFILE_QUERY_PERFORMANCE
                query_profiler::record_evaluation();
#endif
                // we can evaluate
                auto itr = postings_lists.begin();
                auto end = pivot_list+1;
                double W_d = ranker.doc_length(pivot_id);
                double doc_score = query_factor * ranker.calc_doc_weight(W_d);
                while (itr != end) {
                    double contrib = ranker.calculate_impact((*itr)->qt->f_qt,
                                     (*itr)->cur.freq(),(double)(*itr)->qt->f_t,
                                     (*itr)->qt->F_t,W_d,(*itr)->qt->term_weight);
                    doc_score += contrib;
                    ++((*itr)->cur); // move the list ahead after we are done
                    itr++;
                }
#ifdef DEBUG_BMW
                std::cout << " score = " << doc_score << std::endl;
#endif
                heap.insert_if_higher_score( {pivot_id,doc_score});
                // reorder list by id
                sort_list_by_id(postings_lists);
            } else {
#ifdef DEBUG_BMW
                std::cout << "forward stuff before the pivot = " << pivot_id << std::endl;
#endif
                // we have to forward stuff before we can evaluate!
                // find the smallest and do it!
                auto smallest_itr = find_shortest_list(postings_lists,pivot_list);
                (*smallest_itr)->cur.skip_to_id(pivot_id);

                if ((*smallest_itr)->cur == (*smallest_itr)->end) {
                    // list is finished! reorder list by id
                    sort_list_by_id(postings_lists);
                    return;
                }

                // bubble it down!
                auto next = smallest_itr + 1;
                auto list_end = postings_lists.end();
                while (next != list_end && (*smallest_itr)->cur.docid() > (*next)->cur.docid()) {
                    std::swap(*smallest_itr,*next);
                    smallest_itr = next;
                    next++;
                }
            }
        }

        template<class t_pl>
        static typename std::vector<plist_data<t_pl>*>::iterator
        find_shortest_list(std::vector<plist_data<t_pl>*>& postings_lists,const typename std::vector<plist_data<t_pl>*>::iterator& end) {
            auto itr = postings_lists.begin();
            if (itr != end) {
                size_t smallest = std::numeric_limits<size_t>::max();
                auto smallest_itr = itr;
                while (itr != end) {
                    if ((*itr)->cur.remaining() < smallest) {
                        smallest = (*itr)->cur.remaining();
                        smallest_itr = itr;
                    }
                    ++itr;
                }
                return smallest_itr;
            }
            return end;
        }


        template<class t_pl,class rank_func>
        static cstate
        potential_candidate(std::vector<plist_data<t_pl>*>& postings_lists,
                            const typename std::vector<plist_data<t_pl>*>::iterator& pivot_list,
                            double threshold,
                            rank_func& ranker,
                            double query_factor) {
#ifdef DEBUG_BMW
            std::cout << "potential_candidate = ";
#endif
            auto itr = postings_lists.begin();
            auto pivot_id = (*pivot_list)->cur.docid();
            double W_d = ranker.doc_length(pivot_id);
            double doc_weight = ranker.calc_doc_weight(W_d);
            double max_doc_weight_in_blocks = (*pivot_list)->cur.block_max_doc_weight();
            double block_max_score = ((*pivot_list)->qt->term_weight*(*pivot_list)->cur.block_max());
            while (itr != pivot_list) {
                block_max_score += ((*itr)->cur.block_max()*(*itr)->qt->term_weight);
                max_doc_weight_in_blocks = std::max((*itr)->cur.block_max_doc_weight(), max_doc_weight_in_blocks);
                itr++;
            }

            if ((block_max_score + (query_factor*doc_weight)) > threshold) {
#ifdef DEBUG_BMW
                std::cout << "YES -> ID IS CANDIDATE: " << (block_max_score + (query_factor*doc_weight)) << " > " << threshold << std::endl;
#endif
                return ID_IS_CANDIDATE;
            }
            if ((block_max_score + (query_factor*max_doc_weight_in_blocks)) > threshold) {
#ifdef DEBUG_BMW
                std::cout << "NO BUT -> BLOCKS MIGHT CONTAIN CANDIDATE: " << (block_max_score + (query_factor*max_doc_weight_in_blocks)) << " > " << threshold << std::endl;
#endif
                return BLOCKS_MIGHT_CONTAIN_CANDIDATE;
            }
#ifdef DEBUG_BMW
            std::cout << "NO CANDIDATE IN BLOCKS: " << (block_max_score + (query_factor*max_doc_weight_in_blocks)) << " < " << threshold << std::endl;
#endif
            return BLOCKS_DONT_CONTAIN_CANDIDATE;
        }
        template<class t_pl>
        static void
        sort_list_by_id(std::vector<plist_data<t_pl>*>& postings_lists) {
            // check if we can delete stuff
            auto ditr = postings_lists.begin();
            while (ditr != postings_lists.end()) {
                if ((*ditr)->cur == (*ditr)->end) {
                    ditr = postings_lists.erase(ditr);
                } else {
                    ditr++;
                }
            }
            // sort
            auto ptr_sort = [](const plist_data<t_pl>* a,const plist_data<t_pl>* b) {
                return *a < *b;
            };
            std::sort(postings_lists.begin(),postings_lists.end(),ptr_sort);
        }
        template<class t_pl>
        static typename std::vector<plist_data<t_pl>*>::iterator
        determine_next_candidate(std::vector<plist_data<t_pl>*>& postings_lists,double threshold,double query_factor) {
#ifdef DEBUG_BMW
            std::cout << "determine_next_candidate = ";
#endif
            double score = 0;
            double max_doc_score = std::numeric_limits<double>::lowest();
            double total_score = 0;
            auto itr = postings_lists.begin();
            auto end = postings_lists.end();
            while (itr != end) {
                score += ((*itr)->qt->term_weight * (*itr)->data->list_max_score());
                max_doc_score = std::max(max_doc_score,(*itr)->data->max_doc_weight());
                total_score = score + (max_doc_score*query_factor);
#ifdef DEBUG_BMW
                std::cout << "[" << total_score << "," << ((*itr)->qt->term_weight * (*itr)->data->list_max_score()) << "," << (*itr)->data->max_doc_weight() << "] -> ";
#endif
                if (total_score > threshold) {
                    // forward to last list with id equal to pivot!
                    uint64_t pivot_id = (*itr)->cur.docid();
#ifdef DEBUG_BMW
                    std::cout << "FOUND PIVOT ID = " << pivot_id;
#endif
                    auto next = itr+1;
                    while (next != end && (*next)->cur.docid() == pivot_id) {
#ifdef DEBUG_BMW
                        std::cout << " ADVANCE!! ";
#endif
                        itr = next;
                        next++;
                    }
#ifdef DEBUG_BMW
                    std::cout << std::endl;
#endif
                    return itr;
                }
            }
#ifdef DEBUG_BMW
            std::cout << std::endl;
#endif
            return end;
        }
};

#endif
