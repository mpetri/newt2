#ifndef SCORE_HEAP_HPP
#define SCORE_HEAP_HPP

#include <queue>

#include "result.hpp"

struct docscore {
    uint64_t id;
    double score;
    bool operator>(const docscore& rhs) const {
        return score > rhs.score;
    }
};

class score_heap
{
    public:
    private:
        std::priority_queue<docscore,std::vector<docscore>,std::greater<docscore>>  m_heap;
        size_t                                     m_k;
        double                                     m_initial_threshold;
    public:
        score_heap(size_t k,double initial_threshold)
            : m_k(k) , m_initial_threshold(initial_threshold) {
        }
        double threshold() {
            if (m_heap.size() == m_k) return m_heap.top().score;
            return m_initial_threshold;
        }
        size_t size() {
            return m_heap.size();
        }
        void insert_if_higher_score(docscore doc_pair) {
            if (m_heap.size() < m_k) {
                m_heap.push(doc_pair);
            } else {
                if (threshold() < doc_pair.score) {
                    m_heap.pop();
                    m_heap.push(doc_pair);
                }
            }
        }
        ranked_result_list_t elems() {
            ranked_result_list_t result_array;
            while (!m_heap.empty()) {
                auto elem = m_heap.top(); m_heap.pop();
                result_array.emplace(result_array.begin(),elem.id, elem.score);
            }
            return result_array;
        }
};


#endif
