
#ifndef QUERY_PARSER_HPP
#define QUERY_PARSER_HPP

#include <limits>
#include <string>
#include <iostream>
#include <vector>
#include <exception>
#include <sstream>
#include <unordered_set>

#include "sdsl/io.hpp"

#include "settings.hpp"
#include "rank_functions.hpp"
#include "logger.hpp"
#include "query.hpp"
#include "newt_index.hpp"

template<class t_index,
         bool t_expand_query = true,
         bool t_make_unique = true,
         uint64_t t_single_term_weight = newt_constants::expansion_single_weight,
         uint64_t t_phrase_weight = newt_constants::expansion_phrase_weight
         >
class query_parser
{
    public:
        static std::vector<query> parse_query_file(const std::string& query_file, const t_index& index) {
            std::vector<query> queries;

            std::ifstream qry_file(query_file);
            if (! qry_file.is_open()) {
                throw std::logic_error("query_parser: can not open query file.");
            }

            if (t_expand_query) {
                newt_log::info() << "Performing query expansion <"
                                 << t_single_term_weight  << ","
                                 << t_phrase_weight << ">" << std::endl;
            }

            // parse query file
            std::string qline;
            while (std::getline(qry_file, qline)) {
                queries.emplace_back(parse_query_line(qline,index));
            }
            dump_queries(queries,index);
            return queries;
        };
        static query parse_query_line(std::string& line,const t_index& index) {
            query qry;

            // extract number
            std::string::size_type num_sep_pos;
            num_sep_pos = line.find(newt_constants::query_num_seperator, 0);
            if (num_sep_pos == std::string::npos) {
                newt_log::crit() << "query file not in proper format."
                                 << "format: <qrynum>;<term1> <term2> <term3>..."
                                 << std::endl;
                exit(EXIT_FAILURE);
            }
            qry.number = std::atoll(std::string(line,0,num_sep_pos).c_str());

            // extract tokens
            std::vector<std::string> query_terms;
            std::string::size_type p = num_sep_pos+1;
            std::string::size_type q;
            while ((q = line.find(newt_constants::query_term_seperator, p)) != std::string::npos) {
                query_terms.emplace_back(line, p, q - p);
                p = q + 1;
            }
            if (p != line.size()) {
                query_terms.emplace_back(line, p);
            }

            // turn tokens into ids
            for (const auto& token : query_terms) {
                uint64_t term_id = index.termmap.id(token);
                if (term_id != 0) {
                    double single_weight = (double)t_single_term_weight/100.0f;
                    qry.terms.emplace_back(single_weight, 1.0f , term_id);
                } else {
                    newt_log::err() <<  "skip parsing token '" << token << "'"
                                    << " (does not exist in termmap)" << std::endl;
                }
            }
            qry.parsed_terms = qry.terms.size();

            // perform the sequential expansion to include phrases
            if (t_expand_query) {
                //qry = query_analyzer::analyze_query(index,qry,phrase_threshold);
                expand_query_sequential(qry);
                qry.expanded = true;
            }

            // make each query term unique and update f_qt if necessary
            if (t_make_unique) {
                make_query_terms_unique(qry);
            }

            return qry;
        };
        //
        // make each query term unique and update f_qt if necessary
        //
        static void make_query_terms_unique(query& qry) {
            // copy everything into a unique hash table
            std::unordered_set<qterm> queryterm_map;
            for (auto& term : qry.terms) {
                std::unordered_set<qterm>::iterator itr = queryterm_map.find(term);
                if (itr == queryterm_map.end()) {
                    queryterm_map.insert(std::move(term));
                } else {
                    term.f_qt = (*itr).f_qt + 1;
                    queryterm_map.erase(itr);
                    queryterm_map.insert(std::move(term));
                }
            }
            qry.terms.clear();
            // rebuild the query vector
            for (const auto& term : queryterm_map) {
                qry.terms.emplace_back(std::move(term));
            }
        };
        //
        // expands query terms sequentially.
        // This means: Generate all substrings.
        // example: a b c -> "a , a b , a b c , b , b c , c"
        //
        static void expand_query_sequential(query& qry) {
            qry.expanded = true;
            size_t num_terms = qry.terms.size();
            if (num_terms > 1) {
                for (size_t i=0; i<num_terms; i++) {
                    double phrase_weight = (double)t_phrase_weight/100.0f;
                    for (size_t plen=2; i + plen <= num_terms; plen++) {
                        qterm pq = qry.terms[i];
                        pq.term_weight = phrase_weight;
                        for (size_t len=2; len<=plen; ++len) {
                            pq.add_token(qry.terms[i+len-1].tokens[0]);
                        }
                        qry.terms.push_back(pq);
                    }
                }
            }
        };
        //
        // dump queries
        //
        static void dump_queries(const std::vector<query>& queries,const t_index& index) {
            for (size_t i=0; i<queries.size(); i++) {
                const query& qry = queries[i];
                dump_query(qry,index);
            }
        }
        static void dump_query(const query& qry,const t_index& index) {
            std::cerr << "QRY " << qry.number << " ==============================================" << std::endl;
            for (const auto& term : qry.terms) {
                std::cerr << "(ids=[";
                for (const auto& token : term.tokens) {
                    std::cerr << token << " ";
                }
                std::cerr << "],tokens=[";
                for (const auto& token : term.tokens) {
                    std::cerr << index.termmap.term(token) << " ";
                }
                std::cerr << "],f_qt=" << term.f_qt << ",weight=" << term.term_weight << ")\n";
            }
            std::cerr << "QRY =================================================" << std::endl;
        }
};


#endif
