
#ifndef QUERY_HPP
#define QUERY_HPP


//! A qterm can consist of multiple token which is weighted.
class qterm
{
    public:
        double    term_weight = 1.0f;
        double    f_qt = 1;
        double    f_t = 0;
        uint64_t  F_t = 0;
        uint64_t  sp = 0;
        uint64_t  ep = 0;
        std::vector<uint64_t> tokens;
        // modify the query term
    public:
        qterm(double weight,double fqt)
            : term_weight(weight) , f_qt(fqt) {
        }
        qterm(double weight,double fqt,uint64_t token)
            : term_weight(weight) , f_qt(fqt) {
            add_token(token);
        }
        qterm(const qterm& qt) {
            term_weight = qt.term_weight;
            f_qt = qt.f_qt;
            f_t = qt.f_t;
            F_t = qt.F_t;
            sp = qt.sp;
            ep = qt.ep;
            tokens = qt.tokens;
        }
        qterm(const qterm&& qt) {
            term_weight = qt.term_weight;
            f_qt = qt.f_qt;
            f_t = qt.f_t;
            F_t = qt.F_t;
            sp = qt.sp;
            ep = qt.ep;
            tokens = std::move(qt.tokens);
        }
        qterm& operator=(qterm&& qt) {
            term_weight = qt.term_weight;
            f_qt = qt.f_qt;
            f_t = qt.f_t;
            F_t = qt.F_t;
            sp = qt.sp;
            ep = qt.ep;
            tokens = std::move(qt.tokens);
            return *this;
        }
        qterm& operator=(const qterm& qt) {
            term_weight = qt.term_weight;
            f_qt = qt.f_qt;
            f_t = qt.f_t;
            F_t = qt.F_t;
            sp = qt.sp;
            ep = qt.ep;
            tokens = qt.tokens;
            return *this;
        }
        void add_token(uint64_t id) {
            tokens.push_back(id);
        };
        friend bool operator== (qterm& t1, qterm& t2);
};

namespace std
{
template<>
struct hash<qterm> {
    public:
        std::size_t operator()(qterm const& term) const {
            // simple FNV-1a
            std::size_t hash = 14695981039346656037ULL;
            for (size_t i=0; i<term.tokens.size(); i++) {
                hash = hash ^ std::hash<uint64_t>()(term.tokens[i]);
                hash = hash * 1099511628211ULL;
            }
            return hash;
        }
};
}

bool operator==(const qterm& t1,const qterm& t2)
{
    if (t1.tokens.size() != t2.tokens.size()) return false;
    for (size_t i=0; i<t1.tokens.size(); i++) {
        if (t1.tokens[i] != t2.tokens[i]) return false;
    }
    return true;
}

//! Query representation
class query
{
    public:
        uint64_t           number = 0; // For the TREC query ID
        uint64_t           parsed_terms = 0;
        bool               expanded = false;
        std::vector<qterm> terms;
        query() {};
        query(query&& q) {
            number = q.number;
            expanded = q.expanded;
            parsed_terms = q.parsed_terms;
            terms = std::move(q.terms);
        }
        query(const query& q) {
            number = q.number;
            expanded = q.expanded;
            parsed_terms = q.parsed_terms;
            terms = q.terms;
        }
        query& operator=(query&& q) {
            number = q.number;
            expanded = q.expanded;
            parsed_terms = q.parsed_terms;
            terms = std::move(q.terms);
            return *this;
        }
        query& operator=(const query& q) {
            number = q.number;
            expanded = q.expanded;
            parsed_terms = q.parsed_terms;
            terms = q.terms;
            return *this;
        }
};

#endif
